#pragma once

class CTestListCtrl : public CMFCListCtrl
{
	DECLARE_DYNAMIC(CTestListCtrl)

public:
	CTestListCtrl() {};
	virtual ~CTestListCtrl() {};

protected:
	afx_msg BOOL OnToolNeedText(UINT id, NMHDR* pNMHDR, LRESULT* pResult);

	virtual void PreSubclassWindow();

	void CellHitTest(const CPoint& pt, int& nRow, int& nCol) const;
	CString GetToolTipText(int nRow, int nCol);
	DECLARE_MESSAGE_MAP()
};


