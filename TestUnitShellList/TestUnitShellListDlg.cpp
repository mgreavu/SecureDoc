#include "stdafx.h"
#include "TestUnitShellList.h"
#include "TestUnitShellListDlg.h"
#include "AboutDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC(CTestUnitShellListDlg, CColoredDialog)

CTestUnitShellListDlg::CTestUnitShellListDlg(CWnd* pParent /*=NULL*/)
	: CColoredDialog(CTestUnitShellListDlg::IDD, pParent, RGB(255, 255, 255))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestUnitShellListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SHELLLIST, _ctrlList);
	DDX_Control(pDX, IDC_MFCSHELLLIST, _mfcShellList);
	DDX_Control(pDX, IDC_PICTURE, _logo);
}

BEGIN_MESSAGE_MAP(CTestUnitShellListDlg, CColoredDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CTestUnitShellListDlg::OnBnClickedOk)
	ON_COMMAND(ID_BTN_LEFT,&CTestUnitShellListDlg::OnTbnLeft)
	ON_COMMAND(ID_BTN_RIGHT,&CTestUnitShellListDlg::OnTbnRight)
END_MESSAGE_MAP()


BOOL CTestUnitShellListDlg::OnInitDialog()
{
	CColoredDialog::OnInitDialog();

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	CreateToolbar2();

	_ctrlList.InsertColumn(0, L"Name", LVCFMT_LEFT, 100, 0);

	_logo.LoadBmp(IDB_RUBIK);

	return TRUE;
}

void CTestUnitShellListDlg::OnBnClickedOk()
{
	EnumerateFolder(L"D:\\");
}

void CTestUnitShellListDlg::CreateToolbar2()
{
	if (!_mfcToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC)
		|| !_mfcToolBar.LoadToolBar(IDR_TOOLBAR))
	{
		TRACE0("Failed to create toolbar\n");
	}

	_mfcToolBar.SetPaneStyle(_mfcToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_ANY));
	CSize sizeToolBar = _mfcToolBar.CalcFixedLayout(TRUE, TRUE);
    _mfcToolBar.SetWindowPos(NULL, 0, 0, sizeToolBar.cx, sizeToolBar.cy, SWP_NOACTIVATE | SWP_NOZORDER);
}

void CTestUnitShellListDlg::CreateToolbar1()
{
	if (_mfcToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_TOOLBAR))
    {
        _mfcToolBar.SetPaneStyle(_mfcToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_ANY));
        _mfcToolBar.InsertButton(CMFCToolBarButton(ID_BTN_LEFT, -1, _T("Left")));
        _mfcToolBar.InsertButton(CMFCToolBarButton(ID_BTN_RIGHT, -1, _T("Right")));

        CSize sizeToolBar = _mfcToolBar.CalcFixedLayout(TRUE, TRUE);
        _mfcToolBar.SetWindowPos(NULL, 0, 0, sizeToolBar.cx, sizeToolBar.cy, SWP_NOACTIVATE | SWP_NOZORDER);
    }
}

void CTestUnitShellListDlg::OnTbnLeft()
{
	TRACE("\tCTestUnitShellListDlg::OnTbnLeft()\r\n");

	UINT ibtnID = 0, ibtnStyle = 0;
	int  ibtnImage = -1;

	_mfcToolBar.GetButtonInfo(1, ibtnID, ibtnStyle, ibtnImage);
	_mfcToolBar.SetButtonInfo(1, ibtnID, TBBS_DISABLED, ibtnImage);
}

void CTestUnitShellListDlg::OnTbnRight()
{
	TRACE("\tCTestUnitShellListDlg::OnTbnRight()\r\n");
}

void CTestUnitShellListDlg::EnumerateFolder(LPCWSTR path)
{
	HRESULT hr; // COM result, you'd better examine it in your code!
	hr = CoInitialize(NULL); // initialize COM
	// NOTE: usually COM would be initialized just once in your main()

	LPMALLOC pMalloc = NULL; // memory manager, for freeing up PIDLs
	hr = SHGetMalloc(&pMalloc);

	LPSHELLFOLDER psfDesktop = NULL; // namespace root for parsing the path
	hr = SHGetDesktopFolder(&psfDesktop);

	// parse path for absolute PIDL, and connect to target folder
	LPITEMIDLIST pidl = NULL; // general purpose
	hr = psfDesktop->ParseDisplayName(NULL, NULL, (LPWSTR)path, NULL, &pidl, NULL);
	LPSHELLFOLDER psfFolder = NULL;
	hr = psfDesktop->BindToObject(pidl, NULL, IID_IShellFolder, (void**)&psfFolder);
	psfDesktop->Release(); // no longer required
	pMalloc->Free(pidl);

	int nIndex = 0;
	LPENUMIDLIST penumIDL = NULL; // IEnumIDList interface for reading contents
	hr = psfFolder->EnumObjects(NULL, SHCONTF_FOLDERS | SHCONTF_NONFOLDERS, &penumIDL);
	while(1) 
	{
		// retrieve a copy of next local item ID list
		hr = penumIDL->Next(1, &pidl, NULL);
		if(hr == NOERROR) 
		{
			WIN32_FIND_DATA ffd; // let's cheat a bit :)
			hr = SHGetDataFromIDList(psfFolder, pidl, SHGDFIL_FINDDATA, &ffd, sizeof(WIN32_FIND_DATA));

			_ctrlList.InsertItem(nIndex, ffd.cFileName);
			nIndex++;

			pMalloc->Free(pidl);
		}
		// the expected "error" is S_FALSE, when the list is finished
		else 
			break;
	}

	// release all remaining interface pointers
	penumIDL->Release();
	psfFolder->Release();
	pMalloc->Release();

	CoUninitialize(); // shut down COM
}

void CTestUnitShellListDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTestUnitShellListDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTestUnitShellListDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}