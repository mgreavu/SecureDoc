#pragma once
#include "afxcmn.h"
#include "afxshelllistctrl.h"
#include "TestListCtrl.h"
#include "iface/ColoredDialog.h"
#include "iface/XStatic.h"
#include "afxwin.h"

class CTestUnitShellListDlg : public CColoredDialog
{
	DECLARE_DYNAMIC(CTestUnitShellListDlg)

public:
	CTestUnitShellListDlg(CWnd* pParent = NULL);

	enum { IDD = IDD_TESTUNITSHELLLIST_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);

	HICON m_hIcon;
	CTestListCtrl _ctrlList;
	CMFCShellListCtrl _mfcShellList;
	CMFCToolBar _mfcToolBar;
	CXStatic _logo;

	void EnumerateFolder(LPCWSTR path);
	void CreateToolbar1();
	void CreateToolbar2();

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTbnLeft();
	afx_msg void OnTbnRight();
	DECLARE_MESSAGE_MAP()
};
