#include "stdafx.h"
#include "XStatic.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#pragma warning (disable:4244)

CXStatic::CXStatic()
	: _bLoaded(false)
{
}

CXStatic::~CXStatic()
{
}

BEGIN_MESSAGE_MAP(CXStatic, CStatic)
	ON_WM_PAINT()
	ON_WM_SIZE()
END_MESSAGE_MAP()


void CXStatic::OnPaint() 
{
	CPaintDC dc(this);
	if (!_bLoaded)
		return;

	CDC memDC;
	memDC.CreateCompatibleDC(&dc);
	CBitmap *pOldBmp = memDC.SelectObject(&_bm);

	BLENDFUNCTION bf;
	bf.BlendOp = AC_SRC_OVER;
	bf.BlendFlags = 0;
	bf.SourceConstantAlpha = 255;
	bf.AlphaFormat = AC_SRC_ALPHA;
	dc.AlphaBlend(0, 0, _bmInfo.bmWidth, _bmInfo.bmHeight, &memDC, 0, 0, _bmInfo.bmWidth, _bmInfo.bmHeight, bf);
	memDC.SelectObject(pOldBmp);
}

bool CXStatic::LoadBmp(WORD wID)
{
	HBITMAP hBmp = NULL;
	if ((hBmp = (HBITMAP)::LoadImage(::GetModuleHandle(NULL), MAKEINTRESOURCE(wID), IMAGE_BITMAP, 0, 0, (LR_DEFAULTSIZE | LR_CREATEDIBSECTION))) == NULL)
		return false;

	VERIFY(_bm.Attach(hBmp));
	VERIFY(_bm.GetBitmap(&_bmInfo));
	if (_bmInfo.bmBitsPixel != 32)
		return false;
	_bLoaded = true;
/*	Premultiply();	*/
	return true;
}

/*
void CXStatic::Premultiply()
{
	const UINT numPixels = _bmInfo.bmHeight * _bmInfo.bmWidth;	// figure out how many pixels there are in the bitmap

	DIBSECTION ds;
	ZeroMemory(&ds, sizeof(DIBSECTION));

	VERIFY(_bm.GetObject(sizeof(DIBSECTION), &ds) == sizeof(DIBSECTION));
	RGBQUAD* pixels = reinterpret_cast<RGBQUAD*>(ds.dsBm.bmBits);			// get a pointer to the pixels
	VERIFY(pixels != NULL);

	for (UINT i=0; i<numPixels; ++i)
	{
		RGBQUAD* pixel = (RGBQUAD*)(pixels + i);
		TRACE("RGBA = (%d, %d, %d, %d)\r\n", pixel->rgbRed, pixel->rgbGreen, pixel->rgbBlue, pixel->rgbReserved);

		pixel->rgbBlue *= pixel->rgbReserved; pixel->rgbBlue /= 0xFF;
		pixel->rgbGreen *= pixel->rgbReserved; pixel->rgbGreen /= 0xFF;
		pixel->rgbRed *= pixel->rgbReserved; pixel->rgbRed /= 0xFF;
	}
}
*/

void CXStatic::OnSize(UINT nType, int cx, int cy) 
{
	CStatic::OnSize(nType, cx, cy);
	Invalidate();
}
