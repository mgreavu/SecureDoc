#include "stdafx.h"
#include "TestUnitShellList.h"
#include "TestUnitShellListDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

BEGIN_MESSAGE_MAP(CTestUnitShellListApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


CTestUnitShellListApp::CTestUnitShellListApp()
{
}


CTestUnitShellListApp theApp;


BOOL CTestUnitShellListApp::InitInstance()
{
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	CShellManager *pShellManager = new CShellManager;

	CTestUnitShellListDlg dlg;
	m_pMainWnd = &dlg;
	dlg.DoModal();

	if (pShellManager != NULL)
		delete pShellManager;

	return FALSE;
}

