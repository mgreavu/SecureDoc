//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TestUnitShellList.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TESTUNITSHELLLIST_DIALOG    102
#define IDR_MAINFRAME                   128
#define IDR_TOOLBAR                     135
#define IDB_KEYS                        139
#define IDB_RUBIK                       140
#define IDC_SHELLLIST                   1000
#define IDC_MFCSHELLLIST                1001
#define IDC_PICTURE                     1002
#define ID_BTN_LEFT                     32771
#define ID_BTN_RIGHT                    32772
#define ID_BTN_THREE                    32773
#define ID_BTN_FOUR                     32774
#define ID_BTN_FIVE                     32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
