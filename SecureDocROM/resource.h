//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SecureDocROM.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDD_SECUREDOC_DIALOG            102
#define IDD_PROG_DIALOG                 103
#define IDD_CONFIG_IFACE                104
#define IDR_MAINFRAME                   128
#define IDR_CONTEXT_MENU                131
#define IDB_ENCRYPT                     132
#define IDB_DECRYPT                     133
#define IDB_DELETE                      134
#define IDR_TOOLBAR                     136
#define IDD_ENCKEYS_DIALOG              137
#define IDD_ENTER_PASSWORD_DIALOG       138
#define IDS_ABOUTBOX                    139
#define IDS_ENCKEYS_NOT_GEN             140
#define IDS_ENCKEYS_SAVED               141
#define IDS_ENCKEYS_NOT_SAVED           142
#define IDS_ENCKEYS_REMOVE              143
#define IDS_ENTERPWD_FAIL               144
#define IDS_SD_INST_FAIL                145
#define IDS_SD_USERFOLDER_FAIL          146
#define IDS_SDD_NO_ENCKEY               147
#define IDS_SDD_NO_DECKEY               148
#define IDS_SDD_DELETE                  149
#define IDS_SDL_DELETE_ORIG             150
#define IDI_USER_FILES                  151
#define IDS_SDD_EXIT                    152
#define IDI_FOLDER_OPEN                 153
#define IDS_CHGPWD_FAIL                 154
#define IDB_LOGO                        155
#define IDS_SDD_RENAME                  156
#define IDD_CHANGE_PASSWORD_DIALOG      157
#define IDB_OK                          158
#define IDB_CANCEL                      159
#define IDB_DOC_ADD                     160
#define IDB_DOC_CLOSE                   161
#define IDB_DOC_DEL                     162
#define IDB_DOC_SAVE                    163
#define IDB_DOC_SELECT                  164
#define IDS_SETTINGS                    165
#define IDB_SETTINGS                    166
#define IDS_SBAR_FILES                  167
#define IDS_CHGPWD_EMPTY                168
#define IDS_CHGPWD_CONFIRM_FAIL         169
#define IDS_CHGPWD_NEW_IDENT            170
#define IDS_ENTERPWD                    171
#define IDS_CHGPWD_CREDS                172
#define IDS_SDL_HDR_NAME                173
#define IDS_SDL_HDR_MODIF               174
#define IDS_SDL_HDR_TYPE                175
#define IDS_SDL_HDR_SIZE                176
#define IDI_OPT_GENERAL                 177
#define IDC_PC_TREE                     1000
#define IDC_VERT_SPLITTER               1001
#define IDC_LOGO                        1001
#define IDC_DOC_LIST                    1002
#define IDC_PROGBAR                     1003
#define IDC_PROG_TOTAL                  1004
#define IDC_OPERATION                   1005
#define IDC_KEYS_LIST                   1006
#define IDC_NEW_KEY                     1007
#define IDC_REMOVE_KEY                  1008
#define IDC_SAVE_KEYS                   1009
#define IDC_PASSWORD                    1010
#define IDC_PASSWORD_GROUP              1011
#define IDC_SBAR                        1012
#define IDC_GROUP_CHANGE_PASSWORD       1013
#define IDC_CURRENT_PASSW               1014
#define IDC_NEW_PASSW                   1015
#define IDC_CONFIRM_PASSW               1016
#define IDC_LINK                        1017
#define IDC_CHK_ORIG_NAME               1018
#define IDC_CMB_LANG                    1019
#define IDC_HDR_GENERAL                 1020
#define ID_ENCRYPT                      32774
#define ID_DECRYPT                      32775
#define ID_DELETE                       32776
#define ID_BTN_LEFT                     32777
#define ID_BTN_RIGHT                    32778
#define ID_BTN_KEYS                     32779
#define ID_BTN_PASSW                    32780
#define ID_BTN_SETTINGS                 32781
#define ID_BTN_ABOUT                    32782
#define ID_BTN_EXIT                     32783

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        178
#define _APS_NEXT_COMMAND_VALUE         32784
#define _APS_NEXT_CONTROL_VALUE         1021
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
