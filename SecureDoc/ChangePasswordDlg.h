#pragma once
#include "resource.h"
#include "iface/ColoredDialog.h"
#include "iface/GroupBox.h"
#include "afxwin.h"

class CConfig;

class CChangePasswordDlg : public CColoredDialog
{
	DECLARE_DYNAMIC(CChangePasswordDlg)

public:
	CChangePasswordDlg(CWnd* pParent, CConfig* pConfig);
	virtual ~CChangePasswordDlg();

	enum { IDD = IDD_CHANGE_PASSWORD_DIALOG };

protected:
	CConfig* _pConfig;
	CGroupBox _ChgPasswGroup;
	CString _CurrPassw;
	CString _NewPassw;
	CString _ConfirmPassw;
	CMFCButton _btnCancel;
	CMFCButton _btnOk;

	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();

	DECLARE_MESSAGE_MAP()
};
