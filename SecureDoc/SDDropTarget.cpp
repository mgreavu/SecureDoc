#include "stdafx.h"
#include "SecureDoc.h"
#include "SDDropTarget.h"
#include "SDFile.h"
#include "SDListCtrl.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CSDDropTarget::CSDDropTarget(CSDListCtrl* pList) 
	: _pList(pList)
	, _piDropHelper(NULL)
	, _bUseDnDHelper(false)
{
	// Create an instance of the shell DnD helper object.
	if (SUCCEEDED(CoCreateInstance(CLSID_DragDropHelper, NULL, CLSCTX_INPROC_SERVER, IID_IDropTargetHelper, (void**)&_piDropHelper)))
		_bUseDnDHelper = true;
}

CSDDropTarget::~CSDDropTarget()
{
	if (_piDropHelper != NULL)
		_piDropHelper->Release();
}

DROPEFFECT CSDDropTarget::OnDragEnter(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
{
	DROPEFFECT dwEffect = DROPEFFECT_NONE;

	// Look for CF_HDROP data in the data object, and accept the drop if it's there.
	if (pDataObject->GetGlobalData(CF_HDROP) != NULL)
		dwEffect = DROPEFFECT_COPY;

	if (_bUseDnDHelper)
	{
		// The DnD helper needs an IDataObject interface, so get one from the COleDataObject.
		// GetIDataObject will not AddRef() the returned interface, so we do not Release() it.
		// Note that the FALSE param means that.

		IDataObject* piDataObj = pDataObject->GetIDataObject(FALSE);
		_piDropHelper->DragEnter(pWnd->GetSafeHwnd(), piDataObj, &point, dwEffect);
	}

	return dwEffect;
}

DROPEFFECT CSDDropTarget::OnDragOver(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
{
	DROPEFFECT dwEffect = DROPEFFECT_NONE;

	// Look for CF_HDROP data in the data object, and accept the drop if it's there.
	if (pDataObject->GetGlobalData(CF_HDROP) != NULL)
		dwEffect = DROPEFFECT_COPY;

	if (_bUseDnDHelper)
		_piDropHelper->DragOver(&point, dwEffect);

	return dwEffect;
}

BOOL CSDDropTarget::OnDrop(CWnd* pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point)
{
	BOOL bRet = ReadHdropData(pDataObject);
	if (bRet)
	{
		if (_srcFolder.size() && _veFiles.size())
		{
			size_t nPos = wstring::npos;
			if ((nPos = _srcFolder.rfind(L'\\')) !=  wstring::npos)
			{
				_srcFolder = _srcFolder.substr(0, nPos + 1);
				_pList->OnDropCopy(_srcFolder, _veFiles);
			}
		}
	}

	_srcFolder.clear();
	while(!_veFiles.empty())
	{
		delete _veFiles.back();
		_veFiles.pop_back();
	}

	if (_bUseDnDHelper)
	{
		IDataObject* piDataObj = pDataObject->GetIDataObject(FALSE);
		_piDropHelper->Drop(piDataObj, &point, dropEffect);
	}

	return bRet;
}

void CSDDropTarget::OnDragLeave(CWnd* pWnd)
{
	if (_bUseDnDHelper)
		_piDropHelper->DragLeave();
}

// ReadHdropData() reads CF_HDROP data from the passed-in data object, and 
// puts all dropped files/folders into the main window's list control.
BOOL CSDDropTarget::ReadHdropData(COleDataObject* pDataObject)
{
	HGLOBAL hg;
	if ((hg = pDataObject->GetGlobalData(CF_HDROP)) == NULL)
		return FALSE;

	HDROP hdrop;
	if ((hdrop = (HDROP)GlobalLock(hg)) == NULL)
	{
		GlobalUnlock(hg);
		return FALSE;
	}

	SHFILEINFO sfi;
	WCHAR szNextFile[MAX_PATH];
	UINT uNumFiles = DragQueryFile(hdrop, -1, NULL, 0);
	for (UINT uFile = 0; uFile < uNumFiles; ++uFile)
	{
		if (DragQueryFile(hdrop, uFile, szNextFile, MAX_PATH ) > 0)
		{
			wstring fileName = FilenameFromSourcePath(szNextFile);
			LVFINDINFO  rlvFind = {LVFI_STRING, fileName.c_str()};
			if (_pList->FindItem(&rlvFind, -1) != -1)	// If the filename is already in the list, skip it
				continue;
			
			HANDLE hFind = INVALID_HANDLE_VALUE;
			WIN32_FIND_DATA w32fd;
			__int64 size = 0;
			if ((hFind = ::FindFirstFile(szNextFile, &w32fd)) != INVALID_HANDLE_VALUE)
			{
				if (!(w32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
				{
					size = w32fd.nFileSizeHigh;
					size <<= 32;
					size += w32fd.nFileSizeLow;
					::FindClose(hFind);

					SHGetFileInfo(szNextFile, 0, &sfi, sizeof(SHFILEINFO), SHGFI_SYSICONINDEX | SHGFI_ATTRIBUTES | SHGFI_TYPENAME);
					_veFiles.push_back(new SDFile(w32fd.cFileName, L"", sfi.szTypeName, w32fd.ftLastWriteTime, sfi.dwAttributes, size, sfi.iIcon));
				}
			}
		}
	}

	GlobalUnlock(hg);

	_srcFolder = szNextFile;
	return TRUE;
}

wstring CSDDropTarget::FilenameFromSourcePath(const PWCHAR filePath)
{
	wstring sFName = filePath;
	size_t nIndex = wstring::npos;

	if ((nIndex = sFName.rfind(L'\\')) == wstring::npos) 
		return sFName;

	return sFName.substr(nIndex + 1);
}