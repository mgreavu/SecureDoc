#pragma once
#include <string>
#include <vector>

using std::wstring;
using std::vector;

class SDFile;
class CSDListCtrl;

class CSDDropTarget : public COleDropTarget
{
public:
	CSDDropTarget(CSDListCtrl* pList);
	virtual ~CSDDropTarget();

	virtual DROPEFFECT OnDragEnter(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual DROPEFFECT OnDragOver(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual BOOL OnDrop(CWnd* pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);
	virtual void OnDragLeave(CWnd* pWnd);

protected:
	IDropTargetHelper* _piDropHelper;
	vector<SDFile*> _veFiles;
	CSDListCtrl* _pList;
	wstring	_srcFolder;
	bool _bUseDnDHelper;

	BOOL ReadHdropData(COleDataObject* pDataObject);
	wstring FilenameFromSourcePath(const PWCHAR filePath);
};
