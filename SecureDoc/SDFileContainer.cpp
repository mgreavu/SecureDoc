#include "StdAfx.h"
#include "SDFileContainer.h"

void SDFileContainer::DeleteAll()
{
	while(!_veSDFiles.empty())
	{
		delete _veSDFiles.back();
		_veSDFiles.pop_back();
	}
}

void SDFileContainer::SetPath(const wstring& folderPath)
{
	_folderPath = folderPath;
	if (_folderPath.size() > 0)
		if (_folderPath.rfind(L'\\') != (_folderPath.size() - 1))
			_folderPath += L"\\";
}

bool SDFileContainer::Load(unsigned int& totalFiles, unsigned int& encryptedFiles)
{
	HANDLE hSearch = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA w32fd;
	SHFILEINFO sfi;

	wstring SrcPath = _folderPath;
	SrcPath += L"*.*";

	encryptedFiles = 0;
	totalFiles = 0;
	if ((hSearch = ::FindFirstFile(SrcPath.c_str(), &w32fd)) == INVALID_HANDLE_VALUE)
		return false;
	
	while(1)
	{
		if ((_wcsicmp(w32fd.cFileName, L".") != 0) && (_wcsicmp(w32fd.cFileName, L"..") != 0))
			if (!(w32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				__int64 size = w32fd.nFileSizeHigh;
				size <<= 32;
				size += w32fd.nFileSizeLow;

				wstring path = _folderPath;
				path += w32fd.cFileName;
				SHGetFileInfo(path.c_str(), 0, &sfi, sizeof(SHFILEINFO), SHGFI_SYSICONINDEX | SHGFI_SMALLICON | SHGFI_TYPENAME | SHGFI_ATTRIBUTES);

				FILE_HDR fhdr;
				if (GetFileHeader(path, fhdr) && (HIWORD(fhdr.SigAlg) == SDF_SIGNATURE))
				{
					XORHDR(fhdr);
					_veSDFiles.push_back(new SDFile(w32fd.cFileName, fhdr.OriginalName, sfi.szTypeName, w32fd.ftLastWriteTime, sfi.dwAttributes, size, sfi.iIcon));
					++encryptedFiles;
				}
				else
					_veSDFiles.push_back(new SDFile(w32fd.cFileName, L"", sfi.szTypeName, w32fd.ftLastWriteTime, sfi.dwAttributes, size, sfi.iIcon));
				++totalFiles;
			}

		if (!::FindNextFile(hSearch, &w32fd))
			break;
	}
	::FindClose(hSearch);
	return true;
}

const SDFile* SDFileContainer::GetItem(int index) const
{
	if (index >= static_cast<int>(_veSDFiles.size()))
		return NULL;

	return _veSDFiles.at(index);
}

bool SDFileContainer::GetFileHeader(const wstring& strPath, FILE_HDR& fhdr)
{
	HANDLE hFile = INVALID_HANDLE_VALUE;
	if ((hFile = ::CreateFile(strPath.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
		return false;

	bool bReadOk = false;
	unsigned long dwBytes = 0;
	ZeroMemory(&fhdr, sizeof(FILE_HDR));
	if (::ReadFile(hFile, &fhdr, sizeof(FILE_HDR), &dwBytes, NULL))
		bReadOk = true;	

	::CloseHandle(hFile);
	return bReadOk;
}

bool SDFileContainer::FileExists(const wstring& name)
{
	if (!GetSize())
		return false;

	for (int i=0; i<GetSize(); ++i)
	{
		const SDFile* pFile = _veSDFiles.at(i);
		if (pFile)
			if (!_wcsicmp(pFile->GetName().c_str(), name.c_str()))
				return true;
	}
	return false;
}

bool SDFileContainer::GenerateNewName(wstring& name)
{
	wchar_t newName[MAX_PATH + 1];
	size_t pos = wstring::npos;
	pos = name.rfind(L'.');

	for (unsigned int i=2; i<UINT_MAX; ++i)
	{
		ZeroMemory(newName, sizeof(newName));
		if (pos != wstring::npos)
			wsprintf(newName, L"%s (%d).%s", (name.substr(0, pos)).c_str(), i, (name.substr(pos + 1)).c_str());
		else
			wsprintf(newName, L"%s (%d)", name.c_str(), i);
		if (!FileExists(newName))
			break;
	}
	name = newName;
	return ((name.length() > 0) ? true : false);
}