#include "stdafx.h"
#include <Rpc.h>
#include "TaskEncrAes16.h"
#include "Key.h"
#include "base/MemBlock.h"
#include "Exception.h"

#include <vector>
using std::vector;

CTaskEncrAes16::CTaskEncrAes16(unsigned long dwTaskId, const wstring& SourceFilePath, const wstring& DestPath, const CKey* pAes16Key) 
	: CTask(dwTaskId)
	, _SourceFilePath(SourceFilePath)
	, _DestPath(DestPath)
	, _pAesKey(NULL)
	, _hFileToEncrypt(INVALID_HANDLE_VALUE)
	, _hDestFile(INVALID_HANDLE_VALUE)
	, _pCipher(EVP_aes_128_cbc())
	, _pIV(NULL)
{
	if (!pAes16Key)
		throw EncryptionException(0xFFFF, "No encryption key was provided");

	if (pAes16Key->GetLen() != EVP_CIPHER_key_length(_pCipher))
		throw EncryptionException(0xFFFF, "Invalid encryption key length");

	_pAesKey = pAes16Key;

	int ivLen = EVP_CIPHER_iv_length(_pCipher);
	if (ivLen)
	{
		_pIV = new unsigned char[ivLen];
		memset(_pIV, 0x00, ivLen);
		for (int i=0; i<ivLen; ++i)
			*(_pIV + i) = i;
	}

	EVP_CIPHER_CTX_init(&_cipherCtx);

	if (_DestPath.at(_DestPath.size() - 1) != L'\\')
		_DestPath += L'\\';
}

CTaskEncrAes16::~CTaskEncrAes16(void)
{
	Release();

	if (_pIV)
		delete [] _pIV;

	EVP_CIPHER_CTX_cleanup(&_cipherCtx);
}

void CTaskEncrAes16::Release()
{
	if (_hFileToEncrypt != INVALID_HANDLE_VALUE)
	{
		::CloseHandle(_hFileToEncrypt);
		_hFileToEncrypt = INVALID_HANDLE_VALUE;
	}

	if (_hDestFile != INVALID_HANDLE_VALUE)
	{
		::CloseHandle(_hDestFile);
		_hDestFile = INVALID_HANDLE_VALUE;
	}
}

wstring CTaskEncrAes16::FilenameFromSourcePath()
{
	wstring sFName = _SourceFilePath;
	size_t nIndex = wstring::npos;

	if ((nIndex = sFName.rfind(L'\\')) == wstring::npos) 
		return sFName;

	return sFName.substr(nIndex + 1);
}

unsigned __int64 CTaskEncrAes16::GetSourceFileSize()
{
	if (_hFileToEncrypt == INVALID_HANDLE_VALUE)
		throw FileIOException(0xFFFFFFFF, "GetFileSize()::Invalid file handle");

	DWORD dwSizeL = 0, dwSizeH = 0;
	dwSizeL = ::GetFileSize(_hFileToEncrypt, &dwSizeH);
	unsigned __int64 uint64Size = dwSizeH;
	uint64Size <<= 32;
	uint64Size |= dwSizeL;

	return uint64Size;
}

bool CTaskEncrAes16::Resize(HANDLE hFile, unsigned __int64 ullNewSize)
{
	bool bResized = false;
	LONG lLow = (ullNewSize << 32) >> 32;
	LONG lHigh = ullNewSize >> 32;
	::SetFilePointer(hFile, lLow, &lHigh, FILE_BEGIN);
	if (::SetEndOfFile(hFile))
		bResized = true;

	return bResized;
}

void CTaskEncrAes16::Encrypt()
{
	wstring FileNameOnly = FilenameFromSourcePath();

	FILE_HDR fhdr;
	ZeroMemory(&fhdr, sizeof(FILE_HDR));
	fhdr.SigAlg = SDF_SIGNATURE;
	fhdr.SigAlg = (fhdr.SigAlg << 16) + CTaskEncrAes16::ALG_ID;
	wcsncpy_s(fhdr.OriginalName, _countof(fhdr.OriginalName), FileNameOnly.c_str(), __min(MAX_PATH, FileNameOnly.size()));
	XORHDR(fhdr);
	fhdr.OriginalSize = GetSourceFileSize();
	fhdr.TotalChunks = static_cast<unsigned long>(1 + (fhdr.OriginalSize / FILE_UNIT_SIZE));
	fhdr.KeyTag = _pAesKey->GetTag();

	if (!Resize(_hDestFile, fhdr.OriginalSize))		/* estimare, evita realocari de blocuri in fs */
		throw FileIOException(::GetLastError(), "Resize()::Unable to resize the destination file");

	::SetFilePointer(_hDestFile, 0, NULL, FILE_BEGIN);

	unsigned long dwBytesWritten = 0;
	if (!::WriteFile(_hDestFile, (LPBYTE)&fhdr, sizeof(FILE_HDR), &dwBytesWritten, NULL))
		throw FileIOException(::GetLastError(), "WriteFile()::Unable to write the destination file header");

	unsigned __int64 _ullEncrSize = sizeof(FILE_HDR);

	int cipherBlockSize = EVP_CIPHER_block_size(_pCipher);

	MemBlock<byte> pInBuffer(FILE_UNIT_SIZE);
	vector<unsigned char> veOutBuf;

	DWORD dwBytesRead = 0;

	unsigned long ulCurrChunk = 0;
	while (!_pEvtCancel->Wait(_WAIT))
	{
		int inLen = 0, outLen = 0, tmpLen = 0;

		ZeroMemory(pInBuffer, FILE_UNIT_SIZE);
		if (!::ReadFile(_hFileToEncrypt, pInBuffer, FILE_UNIT_SIZE, &dwBytesRead, NULL))
			throw FileIOException(::GetLastError(), "ReadFile()::Unable to read from source file");

		if (!dwBytesRead)
			break;

		inLen = static_cast<int>(dwBytesRead);
		veOutBuf.resize(inLen + cipherBlockSize, 0);

		if (!EVP_EncryptInit_ex(&_cipherCtx, _pCipher, NULL, _pAesKey->GetKey(), _pIV))
			throw EncryptionException(1, "EVP_EncryptInit_ex()::Encryption failure");

		if (!EVP_EncryptUpdate(&_cipherCtx, &(veOutBuf[0]), &outLen, pInBuffer, inLen))
			throw EncryptionException(2, "EVP_EncryptUpdate()::Encryption failure");

		if (!EVP_EncryptFinal_ex(&_cipherCtx, &(veOutBuf[outLen]), &tmpLen))
			throw EncryptionException(3, "EVP_EncryptFinal_ex()::Encryption failure");

		outLen += tmpLen;

		FILE_CHUNK_HDR ZC = {inLen, outLen};
		if (!::WriteFile(_hDestFile, (LPBYTE)&ZC, sizeof(FILE_CHUNK_HDR), &dwBytesWritten, NULL))
			throw FileIOException(::GetLastError(), "WriteFile()::Unable to write FILE_CHUNK_HDR to destination file");
		_ullEncrSize += dwBytesWritten;

		if (!::WriteFile(_hDestFile, &(veOutBuf[0]), outLen, &dwBytesWritten, NULL))
			throw FileIOException(::GetLastError(), "WriteFile()::Unable to write encrypted buffer to destination file");
		_ullEncrSize += dwBytesWritten;

		unsigned long ulPercent = (++ulCurrChunk * 100) / fhdr.TotalChunks;
		if (!(ulPercent % 5))
			NotifyWindow(ulPercent);
	}

	if (fhdr.OriginalSize > _ullEncrSize)
	{
		if (!Resize(_hDestFile, _ullEncrSize))
			throw FileIOException(::GetLastError(), "SetEndOfFile()::Unable to resize the dest file");
	}
}

void CTaskEncrAes16::Execute()
{
	debug("[0x%X] CTaskEncrAes16::Execute(_dwTaskId = %d) --> start time = %d\r\n", ::GetCurrentThreadId(), _dwTaskId, ::GetTickCount());

	UUID uuid;
	ZeroMemory(&uuid, sizeof(UUID));
	::UuidCreate(&uuid);

	PWCHAR pwszUuid = NULL;
	::UuidToString(&uuid, (RPC_WSTR*)&pwszUuid);
	if(pwszUuid == NULL)
		throw FileIOException(::GetLastError(), "UuidToString()::Error generating unique file name");

	wstring destFilePath = _DestPath + wstring(pwszUuid);
	destFilePath += L".sd";
	::RpcStringFree((RPC_WSTR*)&pwszUuid);

	if ((_hFileToEncrypt = ::CreateFile(_SourceFilePath.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
		throw FileIOException(::GetLastError(), "CreateFile()::Error opening source file");

	FILE_HDR fhdr;
	ZeroMemory(&fhdr, sizeof(FILE_HDR));
	unsigned long dwBytes = 0;
	if (!::ReadFile(_hFileToEncrypt, &fhdr, sizeof(FILE_HDR), &dwBytes, NULL))
		throw FileIOException(::GetLastError(), "ReadFile()::Error reading source file header");

	if ((HIWORD(fhdr.SigAlg) == SDF_SIGNATURE) && (LOWORD(fhdr.SigAlg) == CTaskEncrAes16::ALG_ID))
		throw FileIOException(::GetLastError(), "FILE_HDR()::Source file is already AES16 encrypted");

	::SetFilePointer(_hFileToEncrypt, 0, NULL, FILE_BEGIN);

	if ((_hDestFile = ::CreateFile(destFilePath.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
		throw FileIOException(::GetLastError(), "CreateFile()::Error creating destination file");

	Encrypt();

	Release();

	if (_pEvtCancel->Wait(0))
		::DeleteFile(destFilePath.c_str());

	NotifyWindow(100);

	debug("[0x%X] CTaskEncrAes16::Execute(_dwTaskId = %d) --> completion time = %d\r\n", ::GetCurrentThreadId(), _dwTaskId, ::GetTickCount());
}
