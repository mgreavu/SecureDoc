#pragma once
#include "afxcmn.h"
#include "resource.h"
#include "iface\ColoredDialog.h"
#include "iface\GroupBox.h"
#include "iface\XProgressBar.h"
#include "base\Event.h"
#include <string>
#include <vector>
#include <memory>
#include "afxwin.h"

using std::wstring;
using std::vector;
using std::auto_ptr;

class CTask;
class CExecuteEngine;

class CProgressDlg : public CColoredDialog
{
	DECLARE_DYNAMIC(CProgressDlg)

public:
	CProgressDlg(CWnd* pParent, vector<CTask*>& veTasks);
	virtual ~CProgressDlg();

	enum { IDD = IDD_PROG_DIALOG };

protected:	
	CXProgressBar _ProgBar;
	CXProgressBar _ProgBarTotal;
	CGroupBox _ctrlStatus;
	CMFCButton _btnCancel;
	CString _OpStatus;
	CString _OpTitle;
	MEvent _EvtCancel;
	auto_ptr<CExecuteEngine> _pExec;
	vector<CTask*> _veTasks;
	size_t _currTaskIndex;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	void SetStatusText(const wstring& OpTitle, const wstring& OpStatus);
	void FormatStatus();
	const CTask* GetTaskById(unsigned long dwTaskId);
	LRESULT OnNotifyTask(WPARAM wParam, LPARAM lParam);
	LRESULT OnNotifyTaskProgress(WPARAM wParam, LPARAM lParam);	

	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();	

	DECLARE_MESSAGE_MAP()
};
