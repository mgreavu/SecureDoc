#include "StdAfx.h"
#include "Key.h"

CKey::CKey(unsigned __int64 Tag, const byte* Key, byte Len)
	: _Tag(Tag)
	, _Len(Len)
	, _Key(NULL)
{
	_Key = new byte[Len];
	memset(_Key, 0x00, Len);
	memmove(_Key, Key, Len);
	debug("\tCKey --> _Tag = %I64X\r\n", _Tag);
}

CKey::~CKey(void)
{
	if (_Key)
		delete [] _Key;
	debug("\tCKey --> dtor\r\n");
}

CKey::CKey(const CKey& msg) 
	: _Tag(msg._Tag)
	, _Len(msg._Len)
{
	_Key = new byte[_Len];
	memset(_Key, 0x00, _Len);
	memmove(_Key, msg._Key, _Len);
}

CKey& CKey::operator=(const CKey& rhs)
{
	if (this != &rhs)
	{
		_Tag = rhs._Tag;
		_Len = rhs._Len;

		if (_Key)
			delete [] _Key;
		_Key = new byte[_Len];
		memset(_Key, 0x00, _Len);
		memmove(_Key, rhs._Key, _Len);
	}

	return *this;
}
