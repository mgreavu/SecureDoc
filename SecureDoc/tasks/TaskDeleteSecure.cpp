#include "StdAfx.h"
#include "TaskDeleteSecure.h"
#include "base/MemBlock.h"
#include "Exception.h"

CTaskDeleteSecure::CTaskDeleteSecure(unsigned long dwTaskId, const wstring& FilePath) 
	: CTask(dwTaskId)
	, _FilePath(FilePath)
	, _hFileToDelete(INVALID_HANDLE_VALUE)
	, _ulNumOverwrites(2)
{
}

CTaskDeleteSecure::~CTaskDeleteSecure(void)
{
	Release();
}

void CTaskDeleteSecure::Release()
{
	if (_hFileToDelete != INVALID_HANDLE_VALUE)
	{
		::CloseHandle(_hFileToDelete);
		_hFileToDelete = INVALID_HANDLE_VALUE;
	}
}

bool CTaskDeleteSecure::IsFileOnFixedDrive()
{
	wchar_t chDrive[3];
	ZeroMemory(chDrive, sizeof(chDrive));
	wcsncpy_s(chDrive, _countof(chDrive), _FilePath.c_str(), 2);

	if (::GetDriveType(chDrive) == DRIVE_FIXED)
		return true;

	return false;
}

unsigned __int64 CTaskDeleteSecure::GetSourceFileSize()
{
	if (_hFileToDelete == INVALID_HANDLE_VALUE)
		throw FileIOException(0xFFFFFFFF, "GetFileSize()::Invalid file handle");

	DWORD dwSizeL = 0, dwSizeH = 0;
	dwSizeL = ::GetFileSize(_hFileToDelete, &dwSizeH);
	unsigned __int64 uint64Size = dwSizeH;
	uint64Size <<= 32;
	uint64Size |= dwSizeL;

	return uint64Size;
}

void CTaskDeleteSecure::Execute()
{
	debug("[0x%X] CTaskDeleteSecure::Execute(_dwTaskId = %d) --> start time = %d\r\n", ::GetCurrentThreadId(), _dwTaskId, ::GetTickCount());

	::SetFileAttributes(_FilePath.c_str(), FILE_ATTRIBUTE_ARCHIVE);
	if ((_hFileToDelete = ::CreateFile(_FilePath.c_str(), GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
		throw FileIOException(::GetLastError(), "CreateFile()::Error opening source file");

	__int64 ullFileSize = GetSourceFileSize(); 
	if (IsFileOnFixedDrive()) 
		_ulNumOverwrites = 4;

	unsigned long TotalChunks = static_cast<unsigned long>(1 + _ulNumOverwrites*(ullFileSize / FILE_UNIT_SIZE));

	MemBlock<byte> pBuffer(FILE_UNIT_SIZE);
	DWORD dwIndex = 0, dwBytesWritten = 0;

	unsigned long ulCurrChunk = 0;
	while ((++dwIndex <= _ulNumOverwrites) && (!_pEvtCancel->Wait(0)))
	{
		__int64 ullBytesRemaining = ullFileSize;
		memset(pBuffer, LOBYTE(LOWORD(dwIndex*_ulNumOverwrites*0xAE)), FILE_UNIT_SIZE);
		::SetFilePointer(_hFileToDelete, 0, 0, FILE_BEGIN);
		while ((ullBytesRemaining > 0) && (!_pEvtCancel->Wait(_WAIT)))
		{
			if (!::WriteFile(_hFileToDelete, pBuffer, FILE_UNIT_SIZE, &dwBytesWritten, NULL))
				throw FileIOException(::GetLastError(), "WriteFile()::Unable to write to file");

			ullBytesRemaining -= dwBytesWritten;

			unsigned long ulPercent = (++ulCurrChunk * 100) / TotalChunks;
			if (!(ulPercent % 5))
				NotifyWindow(ulPercent);
		}
	}

	::FlushFileBuffers(_hFileToDelete);

	SYSTEMTIME sysTime = {0};
	sysTime.wDay = 1;
	sysTime.wDayOfWeek = 0;
	sysTime.wHour = 0;
	sysTime.wMilliseconds = 0;
	sysTime.wMinute = 0;
	sysTime.wMonth = 1;
	sysTime.wSecond = 0;
	sysTime.wYear = 1980;

	FILETIME fTime = {0};
	::SystemTimeToFileTime(&sysTime, &fTime);
	::SetFileTime(_hFileToDelete, &fTime, &fTime, &fTime);

	Release();
	::DeleteFile(_FilePath.c_str());

	NotifyWindow(100);

	debug("[0x%X] CTaskDeleteSecure::Execute(_dwTaskId = %d) --> completion time = %d\r\n", ::GetCurrentThreadId(), _dwTaskId, ::GetTickCount());
}
