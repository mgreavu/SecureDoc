#include "stdafx.h"
#include "ExecuteEngine.h"
#include "Exception.h"

void _THKERNEL(LPVOID lpvData)
{
	CExecuteEngine *pThread = NULL;
	if ( !(pThread = static_cast<CExecuteEngine*>(lpvData)) )
	{
		return;
	}

    pThread->_locker.lock();
		pThread->_KrnlThrState = CExecuteEngine::ThreadStateWaiting;
		pThread->_bRunning = true;
	pThread->_locker.unlock();

	debug( "[0x%X] _THKERNEL() --> running\r\n", ::GetCurrentThreadId() );
	
	while( 1 )
	{
		pThread->_locker.lock();
			if (!pThread->_bRunning) 
			{
				pThread->_locker.unlock();
				break;
			}
		pThread->_locker.unlock();

		if (!pThread->_ctrlEvt.Wait(INFINITE)) 
			break;

		if (!pThread->TaskQueueProcessor()) 
			break;
		
		pThread->_ctrlEvt.Reset();
	}
	
	pThread->_locker.lock();
		pThread->_KrnlThrState = CExecuteEngine::ThreadStateDown;
/*		pThread->_bRunning = false;	*/
	pThread->_locker.unlock();

	debug( "[0x%X] _THKERNEL() --> completed\r\n", ::GetCurrentThreadId() );
}

bool CExecuteEngine::TaskQueueProcessor()
{
	_locker.lock();
		_KrnlThrState = ThreadStateBusy;
		if (!_bRunning)
		{
			_KrnlThrState = ThreadStateShuttingDown;
			debug("[0x%X] CExecuteEngine::TaskQueueProcessor() --> Shutting down...\r\n", ::GetCurrentThreadId());

			_locker.unlock();
			return false;
		}
	_locker.unlock();

	if (!IsTaskQueueEmpty())
	{
		int nTaskStat = CTask::TaskStatusBeingProcessed;
		while (!IsTaskQueueEmpty())
		{
			if (PopTask())
			{
				if ((!_pEvtCancel->Wait(0)) && (nTaskStat != CTask::TaskStatusAborted))
				{
					_pCurrentTask->SetCancelEvent(_pEvtCancel);
					_pCurrentTask->SetNotificationWindow(_hWndTarget);

					ExecuteTask(_pCurrentTask);
					if ((nTaskStat = _pCurrentTask->GetStatus()) == CTask::TaskStatusAborted)
					{
						_pCurrentTask->Release();
						::SendMessage(_hWndTarget, UWM_NOTIFY_TASK, _pCurrentTask->GetId(), 1);
					}
					else
						::SendMessage(_hWndTarget, UWM_NOTIFY_TASK, _pCurrentTask->GetId(), 0);
				}
			}
		}

		_pCurrentTask = NULL;
	}

	_locker.lock();
		_KrnlThrState = ThreadStateWaiting;
	_locker.unlock();

	::PostMessage(_hWndTarget, UWM_NOTIFY_TASK, 0, 0);

	debug("[0x%X] CExecuteEngine::TaskQueueProcessor() --> _KrnlThrState = %d\r\n", ::GetCurrentThreadId(), _KrnlThrState);

	return true;
}

void CExecuteEngine::Stop()
{
	_locker.lock();
		_bRunning = false;
	_locker.unlock();

	_ctrlEvt.Set();
	debug("[0x%X] CExecuteEngine::Stop() --> signaled\r\n", ::GetCurrentThreadId());

	::Sleep(_dwIdle);
	while(1)
	{
		_locker.lock();
			if (_KrnlThrState == ThreadStateDown)
			{
				debug("[0x%X] CExecuteEngine::Stop() --> _THKERNEL is down\r\n", ::GetCurrentThreadId());
				_locker.unlock();
				return;
			}
		_locker.unlock();

		::Sleep(_dwIdle);
		_ctrlEvt.Set();
	}
}

CExecuteEngine::~CExecuteEngine(void)
{
	if ( _bRunning )
	{
		Stop();
		if ( _thrExecutor.joinable() )
		{
			_thrExecutor.join();
		}
	}

	debug("\tCExecuteEngine::~CExecuteEngine() --> Completed\r\n");
}

CExecuteEngine::CExecuteEngine( HWND hWndTarget, MEvent* pEvtCancel )
	: _hWndTarget( hWndTarget )
	, _pEvtCancel( pEvtCancel )
	, _KrnlThrState( ThreadStateDown )
	, _bRunning( false )
	, _dwIdle( 100 )
	, _dwObjectCondition( NO_ERRORS )
	, _pCurrentTask( NULL )
	, _thrExecutor()
{
}

bool CExecuteEngine::Start()
{
	_locker.lock();
	if ( _bRunning ) 
	{
		_locker.unlock();
		return true;
	}
	_locker.unlock();

	if ( _dwObjectCondition & THREAD_CREATION )
	{
		_dwObjectCondition = _dwObjectCondition ^ THREAD_CREATION;
	}

	_thrExecutor = thread( &_THKERNEL, this ); // move

	return true;
}

void CExecuteEngine::ExecuteTask( CTask* pTask )
{
	wchar_t wszError[MAX_PATH + 1];
	ZeroMemory(wszError, sizeof(wszError));
	size_t i = 0;

	try
	{
		pTask->SetStatus(CTask::TaskStatusBeingProcessed);
		pTask->Execute();										/* throws exceptions */
		pTask->SetStatus(CTask::TaskStatusCompleted);
	}
	catch (std::bad_alloc& e)
	{
		pTask->SetStatus(CTask::TaskStatusAborted);
		mbstowcs_s(&i, wszError, (size_t)MAX_PATH, e.what(), (size_t)MAX_PATH);
		pTask->SetErrorString(wszError);
		debug("\tExecuteTask() --> Exception: %s\r\n", e.what());
	}
	catch (NoMemException& e)
	{
		pTask->SetStatus(CTask::TaskStatusAborted);
		mbstowcs_s(&i, wszError, (size_t)MAX_PATH, e.getDescription(), (size_t)MAX_PATH);
		pTask->SetErrorString(wszError);
		debug("\tExecuteTask() --> Exception: %s\r\n", e.getDescription());
	}
	catch (FileIOException& e)
	{
		pTask->SetStatus(CTask::TaskStatusAborted);
		mbstowcs_s(&i, wszError, (size_t)MAX_PATH, e.getDescription(), (size_t)MAX_PATH);
		pTask->SetErrorString(wszError);
		debug("\tExecuteTask() --> Code: %d, Descr: %s\r\n", e.getCode(), e.getDescription());
	}
	catch (EncryptionException& e)
	{
		pTask->SetStatus(CTask::TaskStatusAborted);
		mbstowcs_s(&i, wszError, (size_t)MAX_PATH, e.getDescription(), (size_t)MAX_PATH);
		pTask->SetErrorString(wszError);
		debug("\tExecuteTask() --> Exception: %s\r\n", e.getDescription());
	}
	catch (DecryptionException& e)
	{
		pTask->SetStatus(CTask::TaskStatusAborted);
		mbstowcs_s(&i, wszError, (size_t)MAX_PATH, e.getDescription(), (size_t)MAX_PATH);
		pTask->SetErrorString(wszError);
		debug("\tExecuteTask() --> Exception: %s\r\n", e.getDescription());
	}
}

bool CExecuteEngine::AddTask( CTask *pTask )
{
	if (!pTask)
		return false;

	if (!PushTask(pTask))
		return false;

	debug("\tAddTask() --> _dwTaskId = %d\r\n", pTask->GetId());

	pTask->SetStatus(CTask::TaskStatusWaitingOnQueue);
	_ctrlEvt.Set();

	return true;
}

unsigned int CExecuteEngine::GetPendingTasks()
{
	unsigned int chEventsWaiting = 0;

	_locker.lock();
		chEventsWaiting = static_cast<unsigned int>(_TaskQueue.size());
    _locker.unlock();

	return chEventsWaiting;
}

bool CExecuteEngine::IsTaskQueueEmpty()
{
	_locker.lock();
		if (_TaskQueue.empty())
		{
			debug("\tCExecuteEngine::IsTaskQueueEmpty() says true\r\n");
			_locker.unlock();

			return true;
		}
		debug("\tCExecuteEngine::IsTaskQueueEmpty() says false\r\n");
	_locker.unlock();

	return false;
}

bool CExecuteEngine::PushTask( CTask* pTask )
{
	if (!pTask) 
		return false;

	_locker.lock();
		_TaskQueue.push(pTask);
	_locker.unlock();

	return true;
}

bool CExecuteEngine::PopTask()
{
	_locker.lock();
		if (_TaskQueue.empty())
		{
			_locker.unlock();
			return false;
		}

		_pCurrentTask = _TaskQueue.front();
		_TaskQueue.pop();
	_locker.unlock();

	return true;
}

CExecuteEngine::ThreadState_t CExecuteEngine::GetThreadState()
{
	ThreadState_t currentState;
	_locker.lock();
		currentState = _KrnlThrState;
	_locker.unlock();

	return currentState;
}
