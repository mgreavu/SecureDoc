#pragma once
#include "Task.h"

class CTaskDeleteSecure : public CTask
{
public:
	CTaskDeleteSecure(unsigned long dwTaskId, const wstring& FilePath);
	~CTaskDeleteSecure(void);

	void Execute();
	void Release();

private:
	static const unsigned int FILE_UNIT_SIZE = 2097152;

	wstring _FilePath;
	HANDLE _hFileToDelete;
	unsigned long _ulNumOverwrites;

protected:
	bool IsFileOnFixedDrive();
	unsigned __int64 GetSourceFileSize();
};
