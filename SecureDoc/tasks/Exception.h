#pragma once

#include <stdexcept>

class BaseException : public std::exception
{
private:
	DWORD dwErr;
	const char* pszDescr;

public:
	BaseException (DWORD dwCode, const char* pszMsg) : std::exception(pszMsg) 
	{
		dwErr = dwCode;
		pszDescr = pszMsg;
	}

	virtual ~BaseException() {};

	const char *getDescription() const throw() { return pszDescr; }
	DWORD getCode() const throw() { return dwErr;}
};

class NoMemException : public BaseException
{
public:
	NoMemException(DWORD dwCode, const char* pszErr) : BaseException(dwCode, pszErr) {};
	~NoMemException() {};
};

class FileIOException : public BaseException
{
public:
	FileIOException(DWORD dwCode, const char* pszErr) : BaseException(dwCode, pszErr) {};
	~FileIOException() {};
};

class EncryptionException : public BaseException
{
public:
	EncryptionException(DWORD dwCode, const char* pszErr) : BaseException(dwCode, pszErr) {};
	~EncryptionException() {};
};

class DecryptionException : public BaseException
{
public:
	DecryptionException(DWORD dwCode, const char* pszErr) : BaseException(dwCode, pszErr) {};
	~DecryptionException() {};
};