#pragma once
#include "Task.h"
#include <base/Event.h>
#include <queue>
#include <mutex>
#include <thread>

using std::queue;
using std::mutex;
using std::thread;

#define NO_ERRORS			       0x00
#define MUTEX_CREATION		       0x01
#define THREAD_CREATION		       0x04
#define MEMORY_FAULT               0x20

class CExecuteEngine 
{
public:
	enum ThreadState_t
	{
		ThreadStateBusy,
		ThreadStateWaiting,
		ThreadStateDown,
		ThreadStateShuttingDown,
		ThreadStateFault
	};

	CExecuteEngine( HWND hWndTarget, MEvent* pEvtCancel );
	~CExecuteEngine(void);

	static const unsigned int UWM_NOTIFY_TASK = WM_APP + 0x301;
	friend void _THKERNEL( LPVOID lpvData );

	bool Start();
	unsigned int  GetPendingTasks();		/* Returns the number of tasks waiting on the queue. */
	bool		  AddTask( CTask *pTask );	/* Places a CTask object on the event stack/queue and notifies the object's thread that a task is waiting to be performed. */
	DWORD		  GetErrorFlags();			/* Returns the object's error flags	*/
	ThreadState_t GetThreadState();			/* Returns the state of a thread */
	DWORD		  GetErrorFlags() const { return _dwObjectCondition; } 

private:
	void Stop();
	bool PushTask( CTask* pTask );
	bool PopTask();
	bool IsTaskQueueEmpty();
	void ExecuteTask( CTask* pTask );
	bool TaskQueueProcessor();

	CExecuteEngine( const CExecuteEngine& );
	CExecuteEngine& operator=( const CExecuteEngine& );

	ThreadState_t	_KrnlThrState;  // current state of thread see thread state data structure
	queue<CTask*>	_TaskQueue;
	MEvent			_ctrlEvt;       // event controller
	mutex			_locker;
	HWND			_hWndTarget;	
	MEvent*			_pEvtCancel;

	bool	_bRunning;		// set to true if thread is running
	DWORD	_dwIdle;		// used for Sleep periods
	DWORD	_dwObjectCondition;
	CTask*	_pCurrentTask;  // task which is currently being processed
	thread  _thrExecutor;
};
