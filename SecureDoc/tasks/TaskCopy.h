#pragma once
#include "Task.h"

class CTaskCopy : public CTask
{
public:
	CTaskCopy(unsigned long dwTaskId, const wstring& SourceFilePath, const wstring& DestPath);
	~CTaskCopy(void);

	void Execute();
	void Release();

private:
	static const unsigned int FILE_UNIT_SIZE = 2097152;

	wstring _SourceFilePath;
	wstring _DestPath;

	HANDLE _hFileToCopy;
	HANDLE _hDestFile;

	bool Resize(HANDLE hFile, unsigned __int64 ullNewSize);
	unsigned __int64 GetSourceFileSize();
	wstring FilenameFromSourcePath();
};
