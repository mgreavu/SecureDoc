#pragma once

class CKey
{
public:
	CKey(unsigned __int64 Tag, const byte* Key, byte Len);
	~CKey(void);

	CKey(const CKey& msg);
	CKey& operator=(const CKey& rhs);

	unsigned __int64 GetTag() const { return _Tag; }
	byte GetLen() const { return _Len; }
	const byte* GetKey() const { return _Key; }

private:
	unsigned __int64 _Tag;
	byte* _Key;
	byte _Len;
};
