#pragma once
#include "Task.h"
#include "FileHdr.h"
#include <openssl/evp.h>

class CKey;

class CTaskEncrAes16 : public CTask
{
public:
	CTaskEncrAes16(unsigned long dwTaskId, const wstring& SourceFilePath, const wstring& DestPath, const CKey* pAes16Key);
	~CTaskEncrAes16(void);

	enum { ALG_ID = 0x16AE };

	void Execute();
	void Release();

protected:
	EVP_CIPHER_CTX _cipherCtx;
	const EVP_CIPHER* _pCipher;
	unsigned char* _pIV;

private:
	static const unsigned int FILE_UNIT_SIZE = 2097152;

	wstring _SourceFilePath;
	wstring _DestPath;

	const CKey* _pAesKey;

	HANDLE _hFileToEncrypt;
	HANDLE _hDestFile;

	void Encrypt();
	bool Resize(HANDLE hFile, unsigned __int64 ullNewSize);
	unsigned __int64 GetSourceFileSize();
	wstring FilenameFromSourcePath();
};
