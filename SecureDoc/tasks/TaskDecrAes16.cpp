#include "stdafx.h"
#include "TaskDecrAes16.h"
#include "Key.h"
#include "base/MemBlock.h"
#include "Exception.h"

#include <vector>
using std::vector;

CTaskDecrAes16::CTaskDecrAes16(unsigned long dwTaskId, const wstring& SourceFilePath, const wstring& DestPath, const CKey* pAes16Key)
	: CTask(dwTaskId)
	, _SourceFilePath(SourceFilePath)
	, _DestPath(DestPath)
	, _pAesKey(NULL)
	, _hFileToDecrypt(INVALID_HANDLE_VALUE)
	, _hDestFile(INVALID_HANDLE_VALUE)
	, _pCipher(EVP_aes_128_cbc())
	, _pIV(NULL)
{
	if (!pAes16Key)
		throw DecryptionException(0xFFFF, "No decryption key was provided");

	if (pAes16Key->GetLen() != EVP_CIPHER_key_length(_pCipher))
		throw DecryptionException(0xFFFF, "Invalid encryption key length");

	_pAesKey = pAes16Key;

	int ivLen = EVP_CIPHER_iv_length(_pCipher);
	if (ivLen)
	{
		_pIV = new unsigned char[ivLen];
		memset(_pIV, 0x00, ivLen);
		for (int i=0; i<ivLen; ++i)
			*(_pIV + i) = i;
	}

	EVP_CIPHER_CTX_init(&_cipherCtx);

	if (_DestPath.at(_DestPath.size() - 1) != L'\\')
		_DestPath += L'\\';
}

CTaskDecrAes16::~CTaskDecrAes16(void)
{
	Release();

	if (_pIV)
		delete [] _pIV;

	EVP_CIPHER_CTX_cleanup(&_cipherCtx);
}

void CTaskDecrAes16::Release()
{
	if (_hFileToDecrypt != INVALID_HANDLE_VALUE)
	{
		::CloseHandle(_hFileToDecrypt);
		_hFileToDecrypt = INVALID_HANDLE_VALUE;
	}

	if (_hDestFile != INVALID_HANDLE_VALUE)
	{
		::CloseHandle(_hDestFile);
		_hDestFile = INVALID_HANDLE_VALUE;
	}
}

bool CTaskDecrAes16::Resize(HANDLE hFile, unsigned __int64 ullNewSize)
{
	bool bResized = false;
	LONG lLow = (ullNewSize << 32) >> 32;
	LONG lHigh = ullNewSize >> 32;
	::SetFilePointer(hFile, lLow, &lHigh, FILE_BEGIN);
	if (::SetEndOfFile(hFile))
		bResized = true;

	return bResized;
}

void CTaskDecrAes16::Decrypt(const FILE_HDR& fhdr)
{
	if (!Resize(_hDestFile, fhdr.OriginalSize))
		throw FileIOException(::GetLastError(), "Resize()::Unable to allocate enough size for the dest file");

	::SetFilePointer(_hDestFile, 0, NULL, FILE_BEGIN);

	int cipherBlockSize = EVP_CIPHER_block_size(_pCipher);

	vector<unsigned char> veOutBuf;
	unsigned long dwBytes = 0, dwBytesWritten = 0;

	unsigned long ulCurrChunk = 0;
	while (!_pEvtCancel->Wait(_WAIT))
	{
		FILE_CHUNK_HDR ZC = {0, 0};
		if (!::ReadFile(_hFileToDecrypt, &ZC, sizeof(FILE_CHUNK_HDR), &dwBytes, NULL))
			throw FileIOException(::GetLastError(), "ReadFile()::Unable to read FILE_CHUNK_HDR from source file");

		if (!dwBytes)	/* EOF */
			break;

		if ((!ZC.dwPlainSize) || (!ZC.dwEncryptedSize))
			throw FileIOException(0xFFFFFFFF, "FILE_CHUNK_HDR()::Invalid file chunk header");

		{
			MemBlock<byte> pEncrData(ZC.dwEncryptedSize);
			if (!::ReadFile(_hFileToDecrypt, pEncrData, ZC.dwEncryptedSize, &dwBytes, NULL))
				throw FileIOException(::GetLastError(), "ReadFile()::Unable to read from source file");

			int byteCount = static_cast<int>(dwBytes);

			int outLen = 0, tmpLen = 0;
			veOutBuf.resize(byteCount + cipherBlockSize, 0);

			if (!EVP_DecryptInit_ex(&_cipherCtx, _pCipher, NULL, _pAesKey->GetKey(), _pIV))
				throw DecryptionException(1, "EVP_DecryptInit_ex()::Decryption failure");
			if (!EVP_DecryptUpdate(&_cipherCtx, &(veOutBuf[0]), &outLen, pEncrData, byteCount))
				throw DecryptionException(2, "EVP_DecryptUpdate()::Decryption failure");
			if (!EVP_DecryptFinal_ex(&_cipherCtx, &(veOutBuf[outLen]), &tmpLen))
				throw DecryptionException(3, "EVP_DecryptFinal_ex()::Decryption failure");

			outLen += tmpLen;

			if (!::WriteFile(_hDestFile, &(veOutBuf[0]), outLen, &dwBytesWritten, NULL))
				throw FileIOException(::GetLastError(), "WriteFile()::Unable to write encrypted buffer to destination file");			
		}

		unsigned long ulPercent = (++ulCurrChunk * 100) / fhdr.TotalChunks;
		if (!(ulPercent % 5))
			NotifyWindow(ulPercent);
	}

	if (!Resize(_hDestFile, fhdr.OriginalSize)) /* elimina %16 bytes de la final, rezultati din AES32 buffer fix */
		throw FileIOException(::GetLastError(), "Resize()::Unable to allocate enough size for the dest file");
}

void CTaskDecrAes16::Execute()
{
	debug("[0x%X] CTaskDecrAes16::Execute(_dwTaskId = %d) --> start time = %d\r\n", ::GetCurrentThreadId(), _dwTaskId, ::GetTickCount());

	if ((_hFileToDecrypt = ::CreateFile(_SourceFilePath.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
		throw FileIOException(::GetLastError(), "CreateFile()::Unable to open the source file");

	FILE_HDR fhdr;
	ZeroMemory(&fhdr, sizeof(FILE_HDR));
	unsigned long dwBytes = 0;
	if (!::ReadFile(_hFileToDecrypt, &fhdr, sizeof(FILE_HDR), &dwBytes, NULL))
		throw FileIOException(::GetLastError(), "FILE_HDR()::Unable to read header from source file");

	if ((HIWORD(fhdr.SigAlg) != SDF_SIGNATURE) || (LOWORD(fhdr.SigAlg) != CTaskDecrAes16::ALG_ID))
		throw FileIOException(::GetLastError(), "FILE_HDR()::Source file is not AES16 encrypted");

	XORHDR(fhdr);

	wstring FilePath = _DestPath + fhdr.OriginalName;
	if ((_hDestFile = ::CreateFile(FilePath.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE) 
		throw FileIOException(::GetLastError(), "CreateFile()::Unable to create the destination file");

	Decrypt(fhdr);

	Release();

	if (_pEvtCancel->Wait(0))
		::DeleteFile(FilePath.c_str());

	NotifyWindow(100);

	debug("[0x%X] CTaskDecrAes16::Execute(_dwTaskId = %d) --> completion time = %d\r\n", ::GetCurrentThreadId(), _dwTaskId, ::GetTickCount());
}
