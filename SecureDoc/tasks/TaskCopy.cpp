#include "StdAfx.h"
#include "TaskCopy.h"
#include "base/MemBlock.h"
#include "Exception.h"

CTaskCopy::CTaskCopy(unsigned long dwTaskId, const wstring& SourceFilePath, const wstring& DestPath)
	: CTask(dwTaskId)
	, _SourceFilePath(SourceFilePath)
	, _DestPath(DestPath)
	, _hFileToCopy(INVALID_HANDLE_VALUE)
	, _hDestFile(INVALID_HANDLE_VALUE)
{
	if (_DestPath.at(_DestPath.size() - 1) != L'\\')
		_DestPath += L'\\';
}

CTaskCopy::~CTaskCopy(void)
{
	Release();
}

void CTaskCopy::Release()
{
	if (_hFileToCopy != INVALID_HANDLE_VALUE)
	{
		::CloseHandle(_hFileToCopy);
		_hFileToCopy = INVALID_HANDLE_VALUE;
	}

	if (_hDestFile != INVALID_HANDLE_VALUE)
	{
		::CloseHandle(_hDestFile);
		_hDestFile = INVALID_HANDLE_VALUE;
	}
}

wstring CTaskCopy::FilenameFromSourcePath()
{
	wstring sFName = _SourceFilePath;
	size_t nIndex = wstring::npos;

	if ((nIndex = sFName.rfind(L'\\')) == wstring::npos) 
		return sFName;

	return sFName.substr(nIndex + 1);
}

unsigned __int64 CTaskCopy::GetSourceFileSize()
{
	if (_hFileToCopy == INVALID_HANDLE_VALUE)
		throw FileIOException(0xFFFFFFFF, "GetFileSize()::Invalid file handle");

	DWORD dwSizeL = 0, dwSizeH = 0;
	dwSizeL = ::GetFileSize(_hFileToCopy, &dwSizeH);
	unsigned __int64 uint64Size = dwSizeH;
	uint64Size <<= 32;
	uint64Size |= dwSizeL;

	return uint64Size;
}

bool CTaskCopy::Resize(HANDLE hFile, unsigned __int64 ullNewSize)
{
	bool bResized = false;
	LONG lLow = (ullNewSize << 32) >> 32;
	LONG lHigh = ullNewSize >> 32;
	::SetFilePointer(hFile, lLow, &lHigh, FILE_BEGIN);
	if (::SetEndOfFile(hFile))
		bResized = true;

	return bResized;
}

void CTaskCopy::Execute()
{
	debug("[0x%X] CTaskCopy::Execute(_dwTaskId = %d) --> start time = %d\r\n", ::GetCurrentThreadId(), _dwTaskId, ::GetTickCount());

	if ((_hFileToCopy = ::CreateFile(_SourceFilePath.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
		throw FileIOException(::GetLastError(), "CreateFile()::Error opening source file");

	wstring FileNameOnly = FilenameFromSourcePath();
	wstring destFilePath = _DestPath + FileNameOnly;

	if ((_hDestFile = ::CreateFile(destFilePath.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
		throw FileIOException(::GetLastError(), "CreateFile()::Error creating destination file");

	unsigned __int64 ullSrcSize = GetSourceFileSize();
	if (!Resize(_hDestFile, ullSrcSize))
		throw FileIOException(::GetLastError(), "Resize()::Unable to resize the destination file");

	::SetFilePointer(_hDestFile, 0, NULL, FILE_BEGIN);

	unsigned long dwBytes = 0, dwBytesWritten = 0;
	MemBlock<byte> pInBuffer(FILE_UNIT_SIZE);
	unsigned long TotalChunks = static_cast<unsigned long>(1 + (ullSrcSize / FILE_UNIT_SIZE));

	unsigned long ulCurrChunk = 0;
	while (!_pEvtCancel->Wait(_WAIT))
	{
		ZeroMemory(pInBuffer, FILE_UNIT_SIZE);
		if (!::ReadFile(_hFileToCopy, pInBuffer, FILE_UNIT_SIZE, &dwBytes, NULL))
			throw FileIOException(::GetLastError(), "ReadFile()::Unable to read from source file");

		if (!dwBytes)
			break;

		if (!::WriteFile(_hDestFile, pInBuffer, dwBytes, &dwBytesWritten, NULL))
			throw FileIOException(::GetLastError(), "WriteFile()::Unable to write to destination file");

		unsigned long ulPercent = (++ulCurrChunk * 100) / TotalChunks;
		if (!(ulPercent % 5))
			NotifyWindow(ulPercent);
	}

	Release();

	if (_pEvtCancel->Wait(0))
		::DeleteFile(destFilePath.c_str());

	NotifyWindow(100);

	debug("[0x%X] CTaskCopy::Execute(_dwTaskId = %d) --> completion time = %d\r\n", ::GetCurrentThreadId(), _dwTaskId, ::GetTickCount());
}