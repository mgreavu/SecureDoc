#include "stdafx.h"
#include "ProgressDlg.h"
#include "Task.h"
#include "ExecuteEngine.h"

IMPLEMENT_DYNAMIC(CProgressDlg, CColoredDialog)

CProgressDlg::CProgressDlg(CWnd* pParent, vector<CTask*>& veTasks)
	: CColoredDialog(CProgressDlg::IDD, pParent, RGB(255, 255, 255))
	, _veTasks(veTasks)
	, _currTaskIndex(0)
	, _OpTitle(L"")
	, _OpStatus(L"")
{
	debug("\tCProgressDlg::CProgressDlg()\r\n");
}

CProgressDlg::~CProgressDlg()
{
	debug("\tCProgressDlg::~CProgressDlg()\r\n");
}

void CProgressDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGBAR, _ProgBar);
	DDX_Control(pDX, IDC_PROG_TOTAL, _ProgBarTotal);
	DDX_Text(pDX, IDC_OPERATION, _OpStatus);
	DDX_Control(pDX, IDC_OPERATION, _ctrlStatus);
	DDX_Control(pDX, IDCANCEL, _btnCancel);
}


BEGIN_MESSAGE_MAP(CProgressDlg, CColoredDialog)
	ON_BN_CLICKED(IDCANCEL, &CProgressDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CProgressDlg::OnBnClickedOk)
	ON_MESSAGE(CTask::UWM_TASK_PROGRESS, &CProgressDlg::OnNotifyTaskProgress)
	ON_MESSAGE(CExecuteEngine::UWM_NOTIFY_TASK, &CProgressDlg::OnNotifyTask)
	ON_WM_CLOSE()
END_MESSAGE_MAP()

BOOL CProgressDlg::OnInitDialog()
{
	CColoredDialog::OnInitDialog();

	_btnCancel.SetImage(IDB_CANCEL, IDB_CANCEL);

	LOGFONT fnt = {0};
	fnt.lfHeight = 16; 
	fnt.lfWeight = 400; 
	fnt.lfItalic = FALSE; 
	fnt.lfQuality = ANTIALIASED_QUALITY; 
	wcscpy_s(fnt.lfFaceName, _countof(fnt.lfFaceName), L"Arial"); 

	_ProgBar.SetFont(fnt);
	_ProgBarTotal.SetFont(fnt);

	_ProgBar.SetRange(0, 100);
	_ProgBar.SetStep(1);
	_ProgBar.SetPos(0);

	size_t nTaskCount = _veTasks.size();
	_ProgBarTotal.SetRange(0, LOWORD(nTaskCount));
	_ProgBarTotal.SetStep(1);
	_ProgBarTotal.SetPos(0);

	wstring strTitle, strFile;
	_veTasks.at(_currTaskIndex)->GetDescription(strTitle, strFile);
	SetStatusText(strTitle, strFile);
	++_currTaskIndex;

	CenterWindow();

	_pExec.reset(new CExecuteEngine(m_hWnd, &_EvtCancel));
	_pExec->Start();
	
	for ( size_t i=0; i<nTaskCount; ++i )
	{
		_pExec->AddTask( _veTasks.at(i) );
	}

	return TRUE;
}

LRESULT CProgressDlg::OnNotifyTaskProgress(WPARAM wParam, LPARAM lParam)
{
	DWORD dwPos = LOWORD(wParam);
	_ProgBar.SetPos(dwPos);
	return 0;
}

LRESULT CProgressDlg::OnNotifyTask(WPARAM wParam, LPARAM lParam)
{
	unsigned long dwTaskId = static_cast<unsigned long>(wParam);

	if ((dwTaskId == 0) && (lParam == 0))	/* toate task-urile au fost executate */
	{
		CDialog::OnCancel();
		return 0;
	}
	if ((dwTaskId != 0) && (lParam == 0)) /* task-ul dwTaskId finalizat cu success */
	{
		_ProgBarTotal.StepIt();

		if (_currTaskIndex < _veTasks.size())
		{
			wstring strTitle, strFile;
			_veTasks.at(_currTaskIndex)->GetDescription(strTitle, strFile);
			SetStatusText(strTitle, strFile);
			++_currTaskIndex;
		}
		return 0;
	}

	CString strTaskMsg = L"An error has occurred during processing. Details as follows:\r\n", strTaskDetails;
	const CTask* pTask = GetTaskById(dwTaskId);
	if (pTask)
	{
		wstring str1, str2;
		pTask->GetDescription(str1, str2);
		strTaskDetails.Format(L"\r\nTaskId = %05d\r\nOperation = %s\r\nFile = %s", dwTaskId, str1.c_str(), str2.c_str());
		strTaskDetails += L"\r\nError = ";
		strTaskDetails += (pTask->GetErrorString()).c_str();
	}
	else
		strTaskDetails.Format(L"\r\nTaskId = %05d", dwTaskId);

	strTaskMsg += strTaskDetails;
	MessageBox(strTaskMsg, L"SecureDoc", MB_OK | MB_ICONSTOP);

	return 1;
}

void CProgressDlg::OnBnClickedOk()
{
	_EvtCancel.Set();
}

void CProgressDlg::OnBnClickedCancel()
{
	_EvtCancel.Set();
}

void CProgressDlg::OnClose()
{
	debug("\tCProgressDlg::OnClose()\r\n");
	_EvtCancel.Set();
}

const CTask* CProgressDlg::GetTaskById(unsigned long dwTaskId)
{
	const CTask* pTask = NULL;

	size_t nTasksNum = _veTasks.size();
	for (size_t i=0; i<nTasksNum; ++i)
		if ((_veTasks.at(i))->GetId() == dwTaskId)
		{
			pTask = _veTasks.at(i);
			break;
		}

	return pTask;
}

void CProgressDlg::SetStatusText(const wstring& OpTitle, const wstring& OpStatus) 
{ 
	_OpTitle = OpTitle.c_str();
	_OpStatus = OpStatus.c_str();

	FormatStatus();

	SetWindowText(_OpTitle);
	((CStatic*)GetDlgItem(IDC_OPERATION))->SetWindowText(_OpStatus);
}

void CProgressDlg::FormatStatus()
{
	HDC hDC = NULL;
	if ((hDC = ::GetDC(m_hWnd)) == NULL)
		return;

	int nPrevMap = 0;
	if (!(nPrevMap = ::SetMapMode(hDC, MM_TEXT)))	/* LP == DP */
	{
		::ReleaseDC(m_hWnd, hDC);
		return;
	}

	CRect RcStat;
	HWND hWndStat = ::GetDlgItem(m_hWnd, IDC_OPERATION);
	::GetClientRect(hWndStat, &RcStat);

	SIZE lStrSize = {0};
	::GetTextExtentPoint(hDC, _OpStatus.GetBuffer(_OpStatus.GetLength()), _OpStatus.GetLength(), &lStrSize);

	CString strOrig = _OpStatus;
	CString strRemove;
	int NumToReplace = static_cast<int>(wcslen(L"..\\.."));
	while(lStrSize.cx >= RcStat.Width() - 30)
	{
		strOrig = _OpStatus;
		strRemove = strOrig.Mid((strOrig.GetLength() - NumToReplace) / 2, ++NumToReplace);
		strOrig.Delete((strOrig.GetLength() - NumToReplace) / 2, NumToReplace);
		::GetTextExtentPoint(hDC, strOrig.GetBuffer(strOrig.GetLength()), strOrig.GetLength(), &lStrSize);
	}

	CString strSec;
	int nPosStartRem = _OpStatus.Find(strRemove);
	int nPosTerm = _OpStatus.Find(L"\\", nPosStartRem + strRemove.GetLength());	// cauta inainte
	if (nPosTerm > 0)
	{
		strSec = _OpStatus.Mid((nPosTerm - nPosStartRem), nPosStartRem);
		_OpStatus.Replace(strSec, L"..\\..");
	}
	else	// cauta inapoi
	{
		strSec = _OpStatus.Mid(0, nPosStartRem);
		int nPosBackRem = strSec.ReverseFind(L'\\');
		if (nPosBackRem > 0)
		{
			strSec = _OpStatus.Mid(nPosBackRem, strRemove.GetLength());
			_OpStatus.Replace(strSec, L"..\\..");
		}
		else
			_OpStatus.Replace(strRemove, L"..\\..");
	}

	::SetMapMode(hDC, nPrevMap);
	::ReleaseDC(m_hWnd, hDC);
}
