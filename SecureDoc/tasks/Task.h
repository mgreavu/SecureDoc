#pragma once
#include <base/Event.h>
#include <string>
#include <mutex>

using std::wstring;
using std::mutex;

class CTask
{
public:
	CTask(unsigned long dwTaskId);
	virtual ~CTask();

	static const unsigned int UWM_TASK_PROGRESS = WM_APP + 100;
	enum TaskStatus_t {TaskStatusNotSubmitted, TaskStatusWaitingOnQueue, TaskStatusBeingProcessed, TaskStatusCompleted, TaskStatusAborted};

	void SetNotificationWindow(HWND hWnd) { _hWndNotify = hWnd; }
	void NotifyWindow(unsigned long dwPosition);
	void SetCancelEvent(MEvent* pEvtCancel);
	void Cancel();

	void SetDescription(const wstring& strTaskDescr1, const wstring& strTaskDescr2)
	{ 
		_strTaskDescr1 = strTaskDescr1;
		_strTaskDescr2 = strTaskDescr2;
	}
	void GetDescription(wstring& strTaskDescr1, wstring& strTaskDescr2) const
	{ 
		strTaskDescr1 = _strTaskDescr1;
		strTaskDescr2 = _strTaskDescr2;
	}

	void SetErrorString(const wstring& strError) { _strError = strError; }
	wstring GetErrorString() const { return _strError; }

	unsigned long GetId() const { return _dwTaskId; }
	void SetStatus(TaskStatus_t state);
	TaskStatus_t GetStatus();

	virtual void Execute() = 0;
	virtual void Release() = 0;

protected:
	unsigned long _dwTaskId;
	mutex _locker;
	TaskStatus_t _TaskStatus;
	HWND _hWndNotify;
	MEvent* _pEvtCancel;
	wstring _strTaskDescr1, _strTaskDescr2;
	wstring _strError;

	static const unsigned int _WAIT = 0;

private:
	CTask(const CTask&);
	CTask& operator=(const CTask&);
};