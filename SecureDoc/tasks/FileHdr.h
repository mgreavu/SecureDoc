#pragma once

#define SDF_SIGNATURE	0x474D

#pragma pack(push, 1)

typedef struct _FILE_HDR
{
	unsigned long SigAlg;
	wchar_t OriginalName[272];
	unsigned long TotalChunks;
	unsigned __int64 OriginalSize;
	unsigned __int64 KeyTag;
} FILE_HDR, *PFILE_HDR;

typedef struct _FILE_CHUNK_HDR
{
	unsigned long dwPlainSize;
	unsigned long dwEncryptedSize;
} FILE_CHUNK_HDR, *PFILE_CHUNK_HDR;

#pragma pack(pop)

void XORHDR(FILE_HDR& fhdr);
