#include "stdafx.h"
#include "Task.h"

using std::lock_guard;

CTask::CTask(unsigned long dwTaskId) 
	: _dwTaskId(dwTaskId)
	, _TaskStatus(TaskStatusNotSubmitted)
	, _pEvtCancel(NULL)
	, _hWndNotify(NULL)
{
	debug("\tCTask::CTask(%d)\r\n", _dwTaskId);
}

CTask::~CTask()
{
	debug("\tCTask::~CTask(%d)\r\n", _dwTaskId);
}

void CTask::SetStatus(TaskStatus_t state) 
{
	lock_guard<mutex> lock(_locker);
	_TaskStatus = state;
	debug("\tCTask::SetStatus() --> _TaskStatus = %d\r\n", _TaskStatus);
}

CTask::TaskStatus_t CTask::GetStatus()
{
	lock_guard<mutex> lock(_locker);
	debug ("\tCTask::GetStatus() --> _TaskStatus = %d\r\n", _TaskStatus);
	return _TaskStatus;
}

void CTask::SetCancelEvent(MEvent* pEvtCancel)
{
	lock_guard<mutex> lock(_locker);
	if (pEvtCancel)	
		_pEvtCancel = pEvtCancel;	
}

void CTask::Cancel()
{
	lock_guard<mutex> lock(_locker);
	if (_pEvtCancel)
		_pEvtCancel->Set();
}

void CTask::NotifyWindow(DWORD dwPosition) 
{
	if (!_hWndNotify)
		return;

	unsigned long dwWparam = _dwTaskId;
	dwWparam <<= 16;
	dwWparam += dwPosition;
	::PostMessage(_hWndNotify, UWM_TASK_PROGRESS, dwWparam, _TaskStatus);
}
