#pragma once
#include "Task.h"
#include "FileHdr.h"
#include <openssl/evp.h>

class CKey;

class CTaskDecrAes16 : public CTask
{
public:
	CTaskDecrAes16(unsigned long dwTaskId, const wstring& SourceFilePath, const wstring& DestPath, const CKey* pAes16Key);
	~CTaskDecrAes16(void);

	enum { ALG_ID = 0x16AE };

	void Execute();
	void Release();

protected:
	EVP_CIPHER_CTX _cipherCtx;
	const EVP_CIPHER* _pCipher;
	unsigned char* _pIV;

private:
	wstring _SourceFilePath;
	wstring _DestPath;

	const CKey* _pAesKey;

	HANDLE _hFileToDecrypt;
	HANDLE _hDestFile;

	void Decrypt(const FILE_HDR& fhdr);
	bool Resize(HANDLE hFile, unsigned __int64 ullNewSize);
};
