#include "StdAfx.h"
#include "KeyContainer.h"
#include "Config.h"
#include "base\MemBlock.h"
#include <wincrypt.h>
#include "tasks\Key.h"

using std::lock_guard;

CKeyContainer::CKeyContainer(CConfig* pConfig)
	: _TagDefaultKey(0)
	, _pConfig(pConfig)
{
	debug("\tCKeyContainer::CKeyContainer()\r\n");
}

CKeyContainer::~CKeyContainer(void)
{
	debug("\tCKeyContainer::~CKeyContainer()\r\n");
}

const CKey* CKeyContainer::GetDefaultKey()
{
	return GetKeyByTag(_TagDefaultKey);
}

const CKey* CKeyContainer::GetKeyByTag(unsigned __int64 Tag)
{
	CKey* pKey = NULL;

	lock_guard<mutex> lock(_locker);

	size_t nTotalKeys = _veKeys.size();
	for (size_t i=0; i<nTotalKeys; ++i)
		if ((_veKeys.at(i).get())->GetTag() == Tag)
		{
			pKey = _veKeys.at(i).get();
			break;
		}

	return pKey;
}

const CKey* CKeyContainer::GetKeyByIndex(size_t Index)
{
	CKey* pKey = NULL;

	lock_guard<mutex> lock(_locker);

	size_t nTotalKeys = _veKeys.size();
	if (Index < nTotalKeys)
		pKey = _veKeys.at(Index).get();

	return pKey;
}

bool CKeyContainer::RemoveKeyByTag(unsigned __int64 Tag)
{
	bool bRemoved = false;

	lock_guard<mutex> lock(_locker);

	vector<CKeyShrPtr>::iterator it = _veKeys.begin();
	for(; it < _veKeys.end(); ++it)
	{
		if ((*it)->GetTag() == Tag)
		{
			if (_TagDefaultKey == Tag)
				_TagDefaultKey = 0;
			_veKeys.erase(it);
			bRemoved = true;
			break;
		}
	}

	return bRemoved;
}

const CKey* CKeyContainer::GenerateKey(byte keyLen)
{
	HCRYPTPROV hCryptProv = NULL;

	if (!::CryptAcquireContext(&hCryptProv, NULL, MS_DEF_PROV, PROV_RSA_FULL, 0))
		if (!::CryptAcquireContext(&hCryptProv, NULL, NULL, PROV_RSA_FULL, CRYPT_NEWKEYSET))
			return NULL;

	CKey* pKey = NULL;
	MemBlock<byte> aesKey(keyLen);

	SYSTEMTIME st;
	::GetSystemTime(&st);
	memmove(aesKey, (byte*)&st, __min(keyLen, sizeof(SYSTEMTIME)));
	if (!::CryptGenRandom(hCryptProv, keyLen, aesKey))
	{
		::CryptReleaseContext(hCryptProv, 0);
		return NULL;
	}

	unsigned __int64 Tag = 0;

	::GetSystemTime(&st);
	memmove((byte*)&Tag, (byte*)&st, __min(sizeof(__int64), sizeof(SYSTEMTIME)));
	if (!::CryptGenRandom(hCryptProv, sizeof(__int64), (byte*)&Tag))
	{
		::CryptReleaseContext(hCryptProv, 0);
		return NULL;
	}

	::CryptReleaseContext(hCryptProv, 0);

	lock_guard<mutex> lock(_locker);

	pKey = new CKey(Tag, aesKey, keyLen);
	_veKeys.push_back(CKeyShrPtr(pKey));

	return pKey;
}

void CKeyContainer::Clear()
{
	lock_guard<mutex> lock(_locker);
	_veKeys.clear();
}

bool CKeyContainer::Load()
{
	lock_guard<mutex> lock(_locker);

	veKeyItem veKeys;
	if (_pConfig->ReadKeys(veKeys) && veKeys.size())
	{
		for(unsigned int i=0; i<veKeys.size(); ++i)
		{
			CKeyItem* pItem = veKeys.at(i);
			_veKeys.push_back(CKeyShrPtr(new CKey(pItem->GetTag(), &((pItem->GetKey())[0]), pItem->GetLen())));
		}

		while(!veKeys.empty())
		{
			delete veKeys.back();
			veKeys.pop_back();
		}
	}

	return true;
}

bool CKeyContainer::Save()
{
	lock_guard<mutex> lock(_locker);

	if (!_pConfig->Begin())
		return false;

	if (!_pConfig->Empty())
	{
		_pConfig->Rollback();
		return false;
	}

	const CKey* pKey = NULL;
	for (unsigned int i=0; i<_veKeys.size(); ++i)
	{
		if ((pKey = GetKeyByIndex(i)) != NULL)
		{
			if (!_pConfig->WriteKey(pKey->GetTag(), pKey->GetLen(), pKey->GetKey()))
			{
				_pConfig->Rollback();
				return false;
			}
		}
	}

	_pConfig->Commit();
	return true;
}