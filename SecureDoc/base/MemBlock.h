#pragma once

/* doesn't handle constructors or anything, meant for int, char, etc */

template<class T> class MemBlock
{
public:
	MemBlock(size_t len)
	{
		Block = new T[len];
		memset(Block, 0x00, len * sizeof(T));
	}

	~MemBlock()
	{ 
		if(Block != NULL) 
			delete [] Block;
	}

	operator T *()
	{
		return Block;
	}

protected:
	T *Block;
};