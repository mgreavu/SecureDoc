#pragma once
#include <windows.h>

class MEvent
{
public:
	MEvent(void) 
		: _hEvent(NULL)
		, _bManualReset(TRUE)
	{
		_hEvent = ::CreateEvent(NULL, _bManualReset, FALSE, NULL);
	}

	~MEvent(void)
	{
		if (_hEvent)
			::CloseHandle(_hEvent);
	}

	void Set() { ::SetEvent(_hEvent); }
	void Reset() { ::ResetEvent(_hEvent); }

	bool Wait(DWORD dwWaitTime)
	{
		if(::WaitForSingleObject(_hEvent, dwWaitTime) != WAIT_OBJECT_0)
			return false;

		return true;
	}

private:
	HANDLE _hEvent;
	BOOL _bManualReset;

	MEvent(const MEvent&);
	MEvent& operator=(const MEvent&);
};
