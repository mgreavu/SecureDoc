#pragma once

class CTrans
{
public:
	static CString Load(UINT nResId)
	{
		CString msg;
		return (msg.LoadString(nResId) ? msg : L"TRANS NOT FOUND");
	}

	static CString Load(UINT nResId, CString subst)
	{
		CString msg;
		if (!msg.LoadString(nResId))
			return L"TRANS NOT FOUND";
		msg.Format(nResId, (LPCTSTR)subst);
		return msg;
	}

	static CString Load(UINT nResId, long subst1, long subst2)
	{
		CString msg;
		if (!msg.LoadString(nResId))
			return L"TRANS NOT FOUND";
		msg.Format(nResId, subst1, subst2);
		return msg;
	}

private:
	CTrans() {};
};