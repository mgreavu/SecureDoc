#include "stdafx.h"
#include "SecureDoc.h"
#include "SettingsDlg.h"
#include "Config.h"
#include "ConfigIface.h"

IMPLEMENT_DYNAMIC(CSettingsDlg, CMFCPropertySheet)

CSettingsDlg::CSettingsDlg(CWnd* pParentWnd, CConfig* pConfig)
	: CMFCPropertySheet(IDS_SETTINGS, pParentWnd)
	, _pConfig(pConfig)
	, _pPageIface(new CConfigIface(_pConfig))
{
	if ((m_psh.dwFlags & PSH_HASHELP) == PSH_HASHELP)
		m_psh.dwFlags &= ~PSH_HASHELP;
	SetLook(CMFCPropertySheet::PropSheetLook_OutlookBar);
	SetIconsList(IDB_SETTINGS, 32);

	AddPage(_pPageIface);
}

CSettingsDlg::~CSettingsDlg()
{
	if (_pPageIface)
		delete _pPageIface;

	debug("\tCSettingsDlg()::~CSettingsDlg()\r\n");
}


BEGIN_MESSAGE_MAP(CSettingsDlg, CMFCPropertySheet)
END_MESSAGE_MAP()
