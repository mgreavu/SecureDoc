#include "stdafx.h"
#include "ChangePasswordDlg.h"
#include "Config.h"
#include "base/Trans.h"

IMPLEMENT_DYNAMIC(CChangePasswordDlg, CColoredDialog)

CChangePasswordDlg::CChangePasswordDlg(CWnd* pParent, CConfig* pConfig)
	: CColoredDialog(CChangePasswordDlg::IDD, pParent, RGB(255, 255, 255))
	, _pConfig(pConfig)
	, _CurrPassw(_T(""))
	, _NewPassw(_T(""))
	, _ConfirmPassw(_T(""))
{
}

CChangePasswordDlg::~CChangePasswordDlg()
{
	debug("\tCChangePasswordDlg()::~CChangePasswordDlg()\r\n");
}

void CChangePasswordDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GROUP_CHANGE_PASSWORD, _ChgPasswGroup);
	DDX_Text(pDX, IDC_CURRENT_PASSW, _CurrPassw);
	DDV_MaxChars(pDX, _CurrPassw, 64);
	DDX_Text(pDX, IDC_NEW_PASSW, _NewPassw);
	DDV_MaxChars(pDX, _NewPassw, 64);
	DDX_Text(pDX, IDC_CONFIRM_PASSW, _ConfirmPassw);
	DDV_MaxChars(pDX, _ConfirmPassw, 64);
	DDX_Control(pDX, IDCANCEL, _btnCancel);
	DDX_Control(pDX, IDOK, _btnOk);
}

BEGIN_MESSAGE_MAP(CChangePasswordDlg, CColoredDialog)
	ON_BN_CLICKED(IDOK, &CChangePasswordDlg::OnBnClickedOk)
END_MESSAGE_MAP()


void CChangePasswordDlg::OnBnClickedOk()
{
	UpdateData();

	size_t i = 0;
	char ansiPassw[MAX_PATH + 1];
	ZeroMemory(ansiPassw, sizeof(ansiPassw));
	wcstombs_s(&i, ansiPassw, (size_t)MAX_PATH, _CurrPassw.GetBuffer(_CurrPassw.GetLength()), (size_t)MAX_PATH);

	if (!_pConfig->VerifyPassword(ansiPassw))
	{
		MessageBox(CTrans::Load(IDS_ENTERPWD_FAIL), L"SecureDoc", MB_OK | MB_ICONSTOP);
		((CEdit*)GetDlgItem(IDC_CURRENT_PASSW))->SetFocus();
		((CEdit*)GetDlgItem(IDC_CURRENT_PASSW))->SetSel(0, -1);
		return;
	}

	if (_NewPassw.IsEmpty())
	{
		MessageBox(CTrans::Load(IDS_CHGPWD_EMPTY), L"SecureDoc", MB_OK | MB_ICONEXCLAMATION);
		((CEdit*)GetDlgItem(IDC_NEW_PASSW))->SetFocus();
		return;
	}
	if (_NewPassw != _ConfirmPassw)
	{
		MessageBox(CTrans::Load(IDS_CHGPWD_CONFIRM_FAIL), L"SecureDoc", MB_OK | MB_ICONEXCLAMATION);
		((CEdit*)GetDlgItem(IDC_CONFIRM_PASSW))->SetFocus();
		((CEdit*)GetDlgItem(IDC_CONFIRM_PASSW))->SetSel(0, -1);
		return;
	}
	if (_NewPassw == _CurrPassw)
	{
		MessageBox(CTrans::Load(IDS_CHGPWD_NEW_IDENT), L"SecureDoc", MB_OK | MB_ICONEXCLAMATION);
		((CEdit*)GetDlgItem(IDC_NEW_PASSW))->SetFocus();
		((CEdit*)GetDlgItem(IDC_NEW_PASSW))->SetSel(0, -1);
		return;
	}

	i = 0;
	ZeroMemory(ansiPassw, sizeof(ansiPassw));
	wcstombs_s(&i, ansiPassw, (size_t)MAX_PATH, _NewPassw.GetBuffer(_NewPassw.GetLength()), (size_t)MAX_PATH);
	if (!_pConfig->SetPassword(ansiPassw))
	{
		MessageBox(CTrans::Load(IDS_CHGPWD_FAIL), L"SecureDoc", MB_OK | MB_ICONSTOP);
		((CEdit*)GetDlgItem(IDC_NEW_PASSW))->SetFocus();
		((CEdit*)GetDlgItem(IDC_NEW_PASSW))->SetSel(0, -1);
		return;
	}

	OnOK();
}

BOOL CChangePasswordDlg::OnInitDialog()
{
	CColoredDialog::OnInitDialog();

	_ChgPasswGroup.SetWndTitle(CTrans::Load(IDS_CHGPWD_CREDS).GetBuffer(32));
	_btnCancel.SetImage(IDB_CANCEL, IDB_CANCEL);
	_btnOk.SetImage(IDB_OK, IDB_OK);
	return TRUE;
}
