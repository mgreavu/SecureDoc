#pragma once
#include <deque>
#include <string>
#include "tasks/FileHdr.h"
using std::wstring;
using std::deque;

class CKeyContainer;
class CTask;

class CSDShellTreeCtrl : public CMFCShellTreeCtrl
{
	DECLARE_DYNAMIC(CSDShellTreeCtrl)

public:
	CSDShellTreeCtrl(CKeyContainer* pKeyContainer);
	virtual ~CSDShellTreeCtrl();

	void Back();
	void Forward();

protected:
	deque<HTREEITEM> _dqueVisited;
	int _dquePos;
	bool _bNav;
	CKeyContainer* _pKeyContainer;
	vector<CTask*> _veTasks;
	unsigned int _TaskIdCounter;
	bool GetFileHeader(const wstring& strPath, FILE_HDR& fhdr);
	bool TraverseFolder(const wstring& SourcePath, unsigned long dwReqType, bool bDelOrig);
	bool DoOps();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnSelChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnMenuEncrypt();
	afx_msg void OnMenuDecrypt();
	DECLARE_MESSAGE_MAP()
};
