#include "stdafx.h"
#include "EnterPasswordDlg.h"
#include "Config.h"
#include "base/Trans.h"

IMPLEMENT_DYNAMIC(CEnterPasswordDlg, CColoredDialog)

CEnterPasswordDlg::CEnterPasswordDlg(CWnd* pParent, CConfig* pConfig)
	: CColoredDialog(CEnterPasswordDlg::IDD, pParent, RGB(255, 255, 255))
	, _pConfig(pConfig)
	, _Password(L"")
{
}

CEnterPasswordDlg::~CEnterPasswordDlg()
{
	debug("\tCEnterPasswordDlg::~CEnterPasswordDlg()\r\n");
}

void CEnterPasswordDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PASSWORD, _Password);
	DDV_MaxChars(pDX, _Password, 64);
	DDX_Control(pDX, IDC_PASSWORD_GROUP, _PasswGroup);
	DDX_Control(pDX, IDCANCEL, _btnCancel);
	DDX_Control(pDX, IDOK, _btnOk);
}


BEGIN_MESSAGE_MAP(CEnterPasswordDlg, CColoredDialog)
	ON_BN_CLICKED(IDOK, &CEnterPasswordDlg::OnBnClickedOk)
END_MESSAGE_MAP()


void CEnterPasswordDlg::OnBnClickedOk()
{
	UpdateData();

	char ansiPassw[MAX_PATH + 1];
	ZeroMemory(ansiPassw, sizeof(ansiPassw));
	size_t i = 0;
	wcstombs_s(&i, ansiPassw, (size_t)MAX_PATH, _Password.GetBuffer(_Password.GetLength()), (size_t)MAX_PATH);

	if (!_pConfig->VerifyPassword(ansiPassw))
	{
		MessageBox(CTrans::Load(IDS_ENTERPWD_FAIL), L"SecureDoc", MB_OK | MB_ICONSTOP);
		((CEdit*)GetDlgItem(IDC_PASSWORD))->SetFocus();
		((CEdit*)GetDlgItem(IDC_PASSWORD))->SetSel(0, -1);
		return;
	}

	OnOK();
}

BOOL CEnterPasswordDlg::OnInitDialog()
{
	CColoredDialog::OnInitDialog();

	_PasswGroup.SetWndTitle(CTrans::Load(IDS_ENTERPWD).GetBuffer(32));
	_btnCancel.SetImage(IDB_CANCEL, IDB_CANCEL);
	_btnOk.SetImage(IDB_OK, IDB_OK);
	return TRUE;  
}
