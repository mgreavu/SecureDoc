#pragma once
#include "resource.h"
#include "iface\ColoredDialog.h"
#include "KeyContainer.h"
#include "afxwin.h"

class CEncKeysDlg : public CColoredDialog
{
	DECLARE_DYNAMIC(CEncKeysDlg)

public:
	CEncKeysDlg(CWnd* pParent, CKeyContainer* pKeyContainer);
	virtual ~CEncKeysDlg();

	enum { IDD = IDD_ENCKEYS_DIALOG };

protected:
	CListCtrl _listKeys;
	int _nCurrSel;
	CKeyContainer* _pKeyContainer;
	CMFCButton _btnNew;
	CMFCButton _btnDefault;
	CMFCButton _btnDelete;
	CMFCButton _btnSave;
	CMFCButton _btnClose;

	void AddListHeaders();
	void PopulateList();
	void AddRow(int nListIndex, const CKey* pKey);
	virtual void DoDataExchange(CDataExchange* pDX);
	void OnClick(NMHDR *pNMHDR, LRESULT *pResult);
	void OnRightClick(NMHDR *pNMHDR, LRESULT *pResult);

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedNewKey();
	afx_msg void OnBnClickedRemoveKey();
	afx_msg void OnBnClickedSaveKeys();
	DECLARE_MESSAGE_MAP()
};
