#pragma once
#include "afxwin.h"
#include "iface/ColoredDialog.h"
#include "iface/AlphaBlendStatic.h"
#include "iface/Hyperlink.h"

class CAboutDlg : public CColoredDialog
{
	DECLARE_DYNAMIC(CAboutDlg)

public:
	CAboutDlg(CWnd* pParent = NULL);

	enum { IDD = IDD_ABOUTBOX };

protected:
	CAlphaBlendStatic _logo;
	CHyperlink _link;
	CMFCButton _btnOk;

	bool _bWorkaroundDone;
	void WorkaroundMFCBug();

	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
};
