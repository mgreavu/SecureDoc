#include "StdAfx.h"
#include "Config.h"
#include <openssl/md5.h>
#include <time.h>
#include <assert.h>

using std::lock_guard;

CConfig::CConfig(const string& dbName)
	: _dbName(dbName)
	, _db(NULL)
	, _stmtInsert(NULL)
	, _stmtSelect(NULL)
	, _bDispEncr(false)
	, _lang("en")
{
	assert(sqlite3_libversion_number() == SQLITE_VERSION_NUMBER);
	assert(sqlite3_threadsafe() > 0);

	_dbName += "config.sqlite";
	if (sqlite3_open_v2(_dbName.c_str(), &_db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX, NULL) != SQLITE_OK)
		return;

	if (sqlite3_exec(_db, "pragma auto_vacuum = 2", NULL, NULL, NULL) != SQLITE_OK)
		return;

	if (sqlite3_exec(_db, "create table if not exists tbl_keys(id integer primary key, tag integer, size integer, content blob)", NULL, NULL, NULL) != SQLITE_OK)
		return;

	if (!TableExists("tbl_config"))
	{
		if (sqlite3_exec(_db, "create table tbl_config(key text, value text)", NULL, NULL, NULL) != SQLITE_OK)
			return;
		if (sqlite3_exec(_db, "insert into tbl_config(key, value) values('passw', '81dc9bdb52d04dc20036dbd8313ed055')", NULL, NULL, NULL) != SQLITE_OK)
			return;
		if (sqlite3_exec(_db, "insert into tbl_config(key, value) values('lang', 'en')", NULL, NULL, NULL) != SQLITE_OK)
			return;
		if (sqlite3_exec(_db, "insert into tbl_config(key, value) values('dispencr', '0')", NULL, NULL, NULL) != SQLITE_OK)
			return;
	}

	string stmt = "insert into tbl_keys(tag, size, content) values(?, ?, ?)";
	if (sqlite3_prepare_v2(_db, stmt.c_str(), static_cast<int>(stmt.length()), &_stmtInsert, 0) != SQLITE_OK)
		return;

	stmt = "select tag, size, content from tbl_keys";
	if (sqlite3_prepare_v2(_db, stmt.c_str(), static_cast<int>(stmt.length()), &_stmtSelect, 0) != SQLITE_OK)
		return;
}

CConfig::~CConfig(void)
{
	if (_stmtInsert)
		sqlite3_finalize(_stmtInsert);

	if (_stmtSelect)
		sqlite3_finalize(_stmtSelect);

	if(_db)
		sqlite3_close(_db);

	debug("\tCConfig::~CConfig()\r\n");
}

bool CConfig::Begin()
{
	_locker.lock();
	if (sqlite3_exec(_db, "begin", NULL, NULL, NULL) != SQLITE_OK)
	{
		_locker.unlock();
		return false;
	}
	return true;
}

bool CConfig::Commit()
{
	if (sqlite3_exec(_db, "end", NULL, NULL, NULL) != SQLITE_OK)
	{
		_locker.unlock();
		return false;
	}
	_locker.unlock();
	return true;
}

bool CConfig::Rollback()
{
	if (sqlite3_exec(_db, "rollback", NULL, NULL, NULL) != SQLITE_OK)
	{
		_locker.unlock();
		return false;
	}
	_locker.unlock();
	return true;
}

bool CConfig::WriteKey(unsigned __int64 Tag, byte keyLen, const byte* key)
{
	sqlite3_bind_int64(_stmtInsert, 1, Tag);
	sqlite3_bind_int(_stmtInsert, 2, keyLen);
	sqlite3_bind_blob(_stmtInsert, 3, key, keyLen, SQLITE_STATIC);

	int sqlErr = sqlite3_step(_stmtInsert);
	debug("\tCConfig::WriteKey(%I64X) --> sqlErr = %d\r\n", Tag, sqlErr);
	sqlite3_reset(_stmtInsert);

	return ((sqlErr == SQLITE_DONE) ? true : false);
}

bool CConfig::ReadKeys(veKeyItem& keyItems)
{
	lock_guard<mutex> lock(_locker);
	
	unsigned __int64 tag = 0;
	unsigned int keyLen = 0;
	int totalBytesRead = 0;

	vector<byte> key;
	const byte* pContent = NULL;

	while (sqlite3_step(_stmtSelect) == SQLITE_ROW)
	{
		tag = sqlite3_column_int64(_stmtSelect, 0);
		keyLen = sqlite3_column_int(_stmtSelect, 1);

		pContent = (byte*)sqlite3_column_blob(_stmtSelect, 2);
		int byteCount = sqlite3_column_bytes(_stmtSelect, 2);
		for (int i=0; i<byteCount; ++i)
			key.push_back(*(pContent + i));

		keyItems.push_back(new CKeyItem(tag, keyLen, key));
		key.clear();
	}

	sqlite3_reset(_stmtSelect);
	return true;
}

bool CConfig::SetPassword(const string& appPassw)
{
	if (!appPassw.length())
		return false;

	lock_guard<mutex> lock(_locker);

	unsigned char md5sum[MD5_DIGEST_LENGTH];
	ZeroMemory(md5sum, sizeof(md5sum));
	MD5((unsigned char*)appPassw.c_str(), appPassw.length(), md5sum);

	char output[2*MD5_DIGEST_LENGTH + 1];
	ZeroMemory(output, sizeof(output));
	for (int j=0; j<MD5_DIGEST_LENGTH; ++j)
		sprintf_s(output + j * 2, 3, "%02x", md5sum[j]);

	string stmt = "update tbl_config set value = '";
	stmt += output;
	stmt += "' where key = 'passw'";

	return ((sqlite3_exec(_db, stmt.c_str(), NULL, NULL, NULL) == SQLITE_OK) ? true : false);
}

bool CConfig::VerifyPassword(const string& appPassw)
{
	if (!appPassw.length())
		return false;

	lock_guard<mutex> lock(_locker);

	sqlite3_stmt* stmtSelect = NULL;
	string stmt = "select value from tbl_config where key = 'passw'";
	if (sqlite3_prepare_v2(_db, stmt.c_str(), static_cast<int>(stmt.length()), &stmtSelect, 0) != SQLITE_OK)
		return false;

	bool bOk = false;
	const unsigned char* pContent = NULL;
	if (sqlite3_step(stmtSelect) == SQLITE_ROW)
	{
		if ((pContent = sqlite3_column_text(stmtSelect, 0)) != NULL)
		{
			unsigned char md5sum[MD5_DIGEST_LENGTH];
			ZeroMemory(md5sum, sizeof(md5sum));
			MD5((unsigned char*)appPassw.c_str(), appPassw.length(), md5sum);

			char output[2*MD5_DIGEST_LENGTH + 1];
			ZeroMemory(output, sizeof(output));
			for(int j=0; j<MD5_DIGEST_LENGTH; ++j)
				sprintf_s(output + j * 2, 3, "%02x", md5sum[j]);

			if (!strncmp((char*)pContent, output, 2*MD5_DIGEST_LENGTH))
				bOk = true;
		}
	}
	sqlite3_finalize(stmtSelect);
	return bOk;
}

bool CConfig::Read()
{
	lock_guard<mutex> lock(_locker);

	sqlite3_stmt* stmtSelect = NULL;
	string stmt = "select value from tbl_config where key = 'dispencr'";
	if (sqlite3_prepare_v2(_db, stmt.c_str(), static_cast<int>(stmt.length()), &stmtSelect, 0) != SQLITE_OK)
		return false;

	bool bOk = false;
	const unsigned char* pVal = NULL;
	if (sqlite3_step(stmtSelect) == SQLITE_ROW)
	{
		if ((pVal = sqlite3_column_text(stmtSelect, 0)) != NULL)
		{
			_bDispEncr = false;
			if (!strncmp((char*)pVal, "1", 1))
				_bDispEncr = true;
			bOk = true;
		}
	}
	sqlite3_finalize(stmtSelect);

	stmt = "select value from tbl_config where key = 'lang'";
	if (sqlite3_prepare_v2(_db, stmt.c_str(), static_cast<int>(stmt.length()), &stmtSelect, 0) != SQLITE_OK)
		return false;

	bOk = false;
	pVal = NULL;
	if (sqlite3_step(stmtSelect) == SQLITE_ROW)
	{
		if ((pVal = sqlite3_column_text(stmtSelect, 0)) != NULL)
		{
			_lang = (char*)pVal;
			bOk = true;
		}
	}
	sqlite3_finalize(stmtSelect);

	return bOk;
}

bool CConfig::Write()
{
	lock_guard<mutex> lock(_locker);

	bool bOk = true;
	string stmt = "update tbl_config set value = '";
	stmt += ((_bDispEncr) ? "1" : "0");
	stmt += "' where key = 'dispencr'";
	if (sqlite3_exec(_db, stmt.c_str(), NULL, NULL, NULL) != SQLITE_OK)
		bOk = false;

	stmt = "update tbl_config set value = '";
	stmt += _lang;
	stmt += "' where key = 'lang'";
	if (sqlite3_exec(_db, stmt.c_str(), NULL, NULL, NULL) != SQLITE_OK)
		bOk = false;

	return bOk;
}

bool CConfig::Empty()
{
	lock_guard<mutex> lock(_locker);
	return ((sqlite3_exec(_db, "delete from tbl_keys", NULL, NULL, NULL) == SQLITE_OK) ? true : false);
}

bool CConfig::Compact()
{
	lock_guard<mutex> lock(_locker);
	return ((sqlite3_exec(_db, "vacuum", NULL, NULL, NULL) == SQLITE_OK) ? true : false);
}

bool CConfig::TableExists(const string& name)
{
	string sql = "select name from sqlite_master where type in ('table','view') and name = ?";
	sqlite3_stmt* stmt = NULL;
	if (sqlite3_prepare(_db, sql.c_str(), static_cast<int>(sql.length()), &stmt, 0) != SQLITE_OK)
		return false;

	sqlite3_bind_text(stmt, 1, name.c_str(), static_cast<int>(name.length()), SQLITE_TRANSIENT);

	int result = sqlite3_step(stmt);
	sqlite3_finalize(stmt);
	if(result == SQLITE_ROW)
		return true;

	sql = "select name from sqlite_temp_master where type in ('table','view') and name=?";
	if (sqlite3_prepare(_db, sql.c_str(), static_cast<int>(sql.length()), &stmt, 0) != SQLITE_OK)
		return false;

	sqlite3_bind_text(stmt, 1, name.c_str(), static_cast<int>(name.length()), SQLITE_TRANSIENT);
	result = sqlite3_step(stmt);
	sqlite3_finalize(stmt);

	return (result == SQLITE_ROW);
}