#include "stdafx.h"
#include "base\Trans.h"
#include "SecureDoc.h"
#include "SecureDocDlg.h"
#include "EnterPasswordDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

BEGIN_MESSAGE_MAP(CSecureDocApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


CSecureDocApp theApp;

BOOL CSecureDocApp::InitInstance()
{
	if ((_hMutexInst = ::CreateMutex(NULL, TRUE, L"{32F49201-4354-4682-8CF1-606B02DF48FC}")) == NULL)
	{
		debug("[0x%X] InitInstance() --> PID = [0x%X], CreateMutex() has failed with %d\r\n", ::GetCurrentThreadId(), ::GetCurrentProcessId(), ::GetLastError());
		return FALSE;
	}

	if (::GetLastError() == ERROR_ALREADY_EXISTS)
	{
		::MessageBox(NULL, CTrans::Load(IDS_SD_INST_FAIL), L"SecureDoc", MB_OK | MB_ICONEXCLAMATION);
		debug("[0x%X] InitInstance() --> PID = [0x%X], another instance detected\r\n", ::GetCurrentThreadId(), ::GetCurrentProcessId());
		return FALSE;
	}

	_dwInitialOwner = ::GetCurrentProcessId();

	// InitCommonControlsEx() is required on Windows XP if an application manifest specifies use of ComCtl32.dll version 6 or later 
	// to enable visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	if (!AfxOleInit())
	{
		AfxMessageBox(L"AfxOleInit Error !");
		return FALSE;
	}

	CWinApp::InitInstance();

	debug("[0x%X] InitInstance() --> PID = [0x%X]\r\n", ::GetCurrentThreadId(), _dwInitialOwner);

	WCHAR destPath[MAX_PATH + 1];
	ZeroMemory(destPath, sizeof(destPath));
	if (FAILED(::SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, destPath)))
	{
		::MessageBox(NULL, CTrans::Load(IDS_SD_USERFOLDER_FAIL), L"SecureDoc", MB_OK | MB_ICONSTOP);
		return FALSE;
	}

	wcscat_s(destPath, MAX_PATH, L"\\Security32");
	::CreateDirectory(destPath, NULL);
	wcscat_s(destPath, MAX_PATH, L"\\SecureDoc");
	::CreateDirectory(destPath, NULL);
	wcscat_s(destPath, MAX_PATH, L"\\");

	char ansiPath[MAX_PATH + 1];
	ZeroMemory(ansiPath, sizeof(ansiPath));
	size_t i = 0;
	wcstombs_s(&i, ansiPath, (size_t)MAX_PATH, destPath, (size_t)MAX_PATH);

	_pConfig.reset(new CConfig(ansiPath));
	_pConfig->Read();

	if (_pConfig->GetLang() == "ro")
	{
		HINSTANCE hRes = NULL;
		if ((hRes = LoadLibrary(L"lang\\SecureDocROM.dll")) != NULL) 
			AfxSetResourceHandle(hRes);
	}

	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
	CMFCButton::EnableWindowsTheming();

	CEnterPasswordDlg* pDlgPwd = new CEnterPasswordDlg(NULL, _pConfig.get());
	if (pDlgPwd->DoModal() != IDOK)
	{
		delete pDlgPwd;
		return FALSE;
	}
	delete pDlgPwd;

	// Create the shell manager, in case the dialog contains any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	auto_ptr<CSecureDocDlg> pDlgMain(new CSecureDocDlg(NULL, _pConfig.get()));
	m_pMainWnd = pDlgMain.get();
	pDlgMain->DoModal();

	if (pShellManager != NULL)	
		delete pShellManager;	

	return FALSE;
}

int CSecureDocApp::ExitInstance()
{
	debug("[0x%X] ExitInstance() --> PID = [0x%X]\r\n", ::GetCurrentThreadId(), ::GetCurrentProcessId());

	if (_hMutexInst)
	{
		if (_dwInitialOwner == ::GetCurrentProcessId())
			::ReleaseMutex(_hMutexInst);

		::CloseHandle(_hMutexInst);
	}

	return CWinApp::ExitInstance();
}