#pragma once

class CConfig;
class CConfigIface;

class CSettingsDlg : public CMFCPropertySheet
{
	DECLARE_DYNAMIC(CSettingsDlg)

public:
	CSettingsDlg(CWnd* pParentWnd, CConfig* pConfig);
	virtual ~CSettingsDlg();

protected:
	CConfig* _pConfig;
	CConfigIface* _pPageIface;

	DECLARE_MESSAGE_MAP()
};
