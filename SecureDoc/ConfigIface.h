#pragma once
#include "afxwin.h"
#include "iface/GradientStatic.h"

class CConfig;

class CConfigIface : public CMFCPropertyPage
{
	DECLARE_DYNAMIC(CConfigIface)

public:
	CConfigIface(CConfig* pConfig);
	virtual ~CConfigIface();

	enum { IDD = IDD_CONFIG_IFACE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog();
	virtual BOOL OnApply();

	afx_msg void OnBnClickedChkOrigName();
	afx_msg void OnCbnSelchangeCmbLang();
	DECLARE_MESSAGE_MAP()

private:
	bool _bDirty;
	CConfig* _pConfig;
	BOOL _chkOrigNames;
	CComboBox _cmbLang;	
	CGradientStatic _hdr;
};
