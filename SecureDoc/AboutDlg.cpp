#include "stdafx.h"
#include "SecureDoc.h"
#include "AboutDlg.h"
#include "afxdialogex.h"

// this class is only used to allow accessing the protected CMFCButton::m_pToolTip - don't use it for anything else
class CMFCButtonTooltipAccessHelper : public CMFCButton
{
public:
	using CMFCButton::m_pToolTip;
};


IMPLEMENT_DYNAMIC(CAboutDlg, CColoredDialog)

CAboutDlg::CAboutDlg(CWnd* pParent) 
	: CColoredDialog(CAboutDlg::IDD, pParent, RGB(255, 255, 255))
{
	_bWorkaroundDone = false;
}

void CAboutDlg::WorkaroundMFCBug()
{
	if (NULL == GetSafeHwnd())
		return;

	if (false != _bWorkaroundDone)
		return;

	CWnd* pWndChild = GetWindow(GW_CHILD);
	while (NULL != pWndChild)
	{
		CMFCButtonTooltipAccessHelper* pButton = static_cast<CMFCButtonTooltipAccessHelper*>(DYNAMIC_DOWNCAST(CMFCButton, pWndChild));
		if (NULL != pButton && NULL != pButton->m_pToolTip)
		{
			CTooltipManager::DeleteToolTip(pButton->m_pToolTip);
			delete pButton->m_pToolTip;
			pButton->m_pToolTip = NULL;
		}
		pWndChild = pWndChild->GetNextWindow();
	}

	_bWorkaroundDone = true;
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
#pragma message("WARNING: Check if this workaround is still needed with newer versions of MFC !")
	WorkaroundMFCBug();

	CColoredDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOGO, _logo);
	DDX_Control(pDX, IDC_LINK, _link);
	DDX_Control(pDX, IDOK, _btnOk);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CColoredDialog)
END_MESSAGE_MAP()

BOOL CAboutDlg::OnInitDialog()
{
	CColoredDialog::OnInitDialog();

	_logo.LoadBmp(IDB_LOGO);
	_link.SetHyperlinkColors(RGB(255, 255, 255), RGB(255, 255, 0), RGB(255,255,255));
	_btnOk.SetImage(IDB_OK, IDB_OK);
	return TRUE;
}