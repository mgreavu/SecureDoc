﻿#include "stdafx.h"
#include "SecureDoc.h"
#include "ConfigIface.h"
#include "afxdialogex.h"
#include "Config.h"

IMPLEMENT_DYNAMIC(CConfigIface, CMFCPropertyPage)

CConfigIface::CConfigIface(CConfig* pConfig)
	: CMFCPropertyPage(CConfigIface::IDD)
	, _bDirty(false)
	, _pConfig(pConfig)
	, _chkOrigNames(FALSE)
{
}

CConfigIface::~CConfigIface()
{
	debug("\tCConfigIface()::~CConfigIface()\r\n");
}

void CConfigIface::DoDataExchange(CDataExchange* pDX)
{
	CMFCPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_ORIG_NAME, _chkOrigNames);
	DDX_Control(pDX, IDC_CMB_LANG, _cmbLang);
	DDX_Control(pDX, IDC_HDR_GENERAL, _hdr);
}


BEGIN_MESSAGE_MAP(CConfigIface, CMFCPropertyPage)
	ON_BN_CLICKED(IDC_CHK_ORIG_NAME, &CConfigIface::OnBnClickedChkOrigName)
	ON_CBN_SELCHANGE(IDC_CMB_LANG, &CConfigIface::OnCbnSelchangeCmbLang)
END_MESSAGE_MAP()


BOOL CConfigIface::OnInitDialog()
{
	CMFCPropertyPage::OnInitDialog();

	_hdr.SetGradient(RGB(32, 32, 64), RGB(112, 112, 156), false);
	_hdr.SetAlignement(0);
	_hdr.SetTextColor(RGB(255, 255, 255));
	_hdr.SetIcon(IDI_OPT_GENERAL);

	LOGFONT fnt;
	ZeroMemory(&fnt, sizeof(fnt));
	fnt.lfHeight = 15; 
	fnt.lfWeight = FW_NORMAL; 
	fnt.lfItalic = FALSE; 
	fnt.lfQuality = ANTIALIASED_QUALITY; 
	fnt.lfPitchAndFamily = DEFAULT_PITCH | FF_MODERN;
	fnt.lfQuality = PROOF_QUALITY;
	_tcsncpy_s(fnt.lfFaceName, _countof(fnt.lfFaceName), _T("Arial"), _tcslen(_T("Arial")));
	_hdr.SetFont(fnt);
	_hdr.SetText(L"General");

	_pConfig->GetDispEncr() ? _chkOrigNames = TRUE : _chkOrigNames = FALSE;
	_cmbLang.AddString(L"English");
	_cmbLang.AddString(L"Română");
	(_pConfig->GetLang() == "ro") ? _cmbLang.SelectString(-1, L"Română") : _cmbLang.SelectString(-1, L"English");

	UpdateData(FALSE);
	return TRUE;
}

BOOL CConfigIface::OnApply()
{
	if (!_bDirty)
		return TRUE;
	
	UpdateData();
	_pConfig->SetDispEncr(((_chkOrigNames) ? true : false));
	(_cmbLang.GetCurSel() == 1) ? _pConfig->SetLang("ro") : _pConfig->SetLang("en");

	_pConfig->Write();
	_bDirty = false;
	return TRUE;
}

void CConfigIface::OnBnClickedChkOrigName()
{
	SetModified();
	_bDirty = true;
}

void CConfigIface::OnCbnSelchangeCmbLang()
{
	SetModified();
	_bDirty = true;
}
