#pragma once
#include "iface\SplitterControl.h"
#include "SDShellTreeCtrl.h"
#include "afxwin.h"

class CConfig;
class CKeyContainer;
class CSDListCtrl;

// distance from the splitter margins to the left and right
// at which you want to place other windows
// you want this zero in this app so that the parent window
// doesn't have to erase it's background and so reduce
// flickering during resizing

#define SPLITTER_MARGIN	0

class CSecureDocDlg : public CDialogEx
{
public:
	CSecureDocDlg(CWnd* pParent, CConfig* pConfig);

	enum { IDD = IDD_SECUREDOC_DIALOG };
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	HICON m_hIcon;
	CSize _sizeToolBar;
	CSize _sizeStatusBar;
	CSplitterControl _vertSplitter;
	float _fSplitterDiv;
	CMFCToolBar _mfcToolBar;
	auto_ptr<CSDListCtrl> _pctrlFileList;
	auto_ptr<CSDShellTreeCtrl> _pshellTree;
	CStatusBarCtrl _statusBar;
	int _SBarWidths[2];
	CConfig* _pConfig;
	auto_ptr<CKeyContainer> _pKeyContainer;

	static const UINT UWM_TBNENCKEYS = WM_APP + 1735;
	static const UINT UWM_TBNPASSWD = WM_APP + 1736;
	static const UINT UWM_TBNSETTINGS = WM_APP + 1737;
	static const UINT UWM_TBNABOUT = WM_APP + 1738;
	static const UINT UWM_TBNEXIT = WM_APP + 1739;

	bool GetScreenRect(LPRECT lpRect);
	LRESULT OnTbnKeys(WPARAM wParam, LPARAM lParam);
	LRESULT OnTbnPasswd(WPARAM wParam, LPARAM lParam);
	LRESULT OnTbnSettings(WPARAM wParam, LPARAM lParam);
	LRESULT OnTbnAbout(WPARAM wParam, LPARAM lParam);
	LRESULT OnTbnExit(WPARAM wParam, LPARAM lParam);
	LRESULT OnListFilesChanged(WPARAM wParam, LPARAM lParam);
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);

	virtual BOOL OnInitDialog();
	virtual void OnOK() {}
	virtual void OnCancel();

	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTreeSelChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTbnNavLeft();
	afx_msg void OnTbnNavRight();
	afx_msg void OnTbnKeys();
	afx_msg void OnTbnPasswd();
	afx_msg void OnTbnSettings();
	afx_msg void OnTbnAbout();
	afx_msg void OnTbnExit();
	DECLARE_MESSAGE_MAP()
};
