#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "Config.h"
#include <memory>
using std::auto_ptr;

class CSecureDocApp : public CWinApp
{
public:
	CSecureDocApp()
		: _hMutexInst(NULL)
		, _dwInitialOwner(0)
	{}

	HANDLE _hMutexInst;
	DWORD _dwInitialOwner;

	virtual BOOL InitInstance();
	virtual int ExitInstance();

protected:
	auto_ptr<CConfig> _pConfig;

	DECLARE_MESSAGE_MAP()
};

extern CSecureDocApp theApp;