#include "stdafx.h"
#include "base\Trans.h"
#include "SecureDoc.h"
#include "SDListCtrl.h"
#include "SDDropTarget.h"
#include "SDFileContainer.h"
#include "KeyContainer.h"
#include "tasks\TaskCopy.h"
#include "tasks\TaskEncrAes16.h"
#include "tasks\TaskDecrAes16.h"
#include "tasks\TaskDeleteSecure.h"
#include "tasks\ProgressDlg.h"
#include "Config.h"

IMPLEMENT_DYNAMIC(CSDListCtrl, CToolTipListCtrl)

CSDListCtrl::CSDListCtrl(CKeyContainer* pKeyContainer, CConfig* pConfig)
	: _pKeyContainer(pKeyContainer)
	, _pConfig(pConfig)
	, _pSDFileContainer(new SDFileContainer())
	, _TaskIdCounter(0)
	, _totalFiles(0)
	, _encryptedFiles(0)
	, _lastWidth(0)
{
	debug("\tCSDListCtrl::CSDListCtrl()\r\n");
}

CSDListCtrl::~CSDListCtrl()
{
	_imgList.Detach();
	debug("\tCSDListCtrl::~CSDListCtrl()\r\n");
}

BEGIN_MESSAGE_MAP(CSDListCtrl, CToolTipListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT_EX(NM_RCLICK, &CSDListCtrl::OnRightClick)
	ON_NOTIFY_REFLECT_EX(NM_DBLCLK, &CSDListCtrl::OnDblClick)
	ON_NOTIFY_REFLECT_EX(LVN_ENDLABELEDIT, &CSDListCtrl::OnEndLabelEdit)
	ON_COMMAND(ID_ENCRYPT, &CSDListCtrl::OnMenuEncrypt)
	ON_COMMAND(ID_DECRYPT, &CSDListCtrl::OnMenuDecrypt)
	ON_COMMAND(ID_DELETE, &CSDListCtrl::OnMenuDelete)
END_MESSAGE_MAP()


void CSDListCtrl::Reload()
{
	int nItem = GetNextItem(-1, LVNI_SELECTED);
	Load(_pSDFileContainer->GetFolder());
	if (nItem != -1)
	{
		SetItemState(nItem, LVNI_SELECTED, LVNI_SELECTED);
		EnsureVisible(nItem, TRUE);
	}
}

void CSDListCtrl::Load(const wstring& folderPath)
{
	SendMessage(WM_SETREDRAW, (WPARAM)FALSE, NULL);

	DeleteAllItems();
	_pSDFileContainer->DeleteAll();
	_pSDFileContainer->SetPath(folderPath);

	if (folderPath.size())
	{
		_pSDFileContainer->Load(_totalFiles, _encryptedFiles);

		const SDFile* pFile = NULL;
		for (int i=0; i<_pSDFileContainer->GetSize(); ++i)
		{
			if ((pFile = _pSDFileContainer->GetItem(i)) == NULL)
				continue;

			AddItem(i, pFile);
		}
		Sort(m_iSortedColumn, m_bAscending);
	}
	else
	{
		_encryptedFiles = 0;
		_totalFiles = 0;
	}

	SendMessage(WM_SETREDRAW, (WPARAM)TRUE, NULL);
	RedrawWindow(NULL, NULL, RDW_ERASE | RDW_FRAME | RDW_INVALIDATE | RDW_ALLCHILDREN);
	::PostMessage(GetParent()->m_hWnd, UWM_FILESCHANGED, _totalFiles, _encryptedFiles);
}

BOOL CSDListCtrl::OnDblClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	int nItem = -1;
	int nSubItem = -1;
	if (pNMIA)
	{
		nItem = pNMIA->iItem;
		nSubItem = pNMIA->iSubItem;
	}

	*pResult = 0;

	if (nItem == -1)
		return FALSE;

	wstring destFolder = _pSDFileContainer->GetFolder();
	if (!destFolder.size())
		return FALSE;

	const SDFile* pFile = NULL;
	if ((pFile = (const SDFile*)GetItemData(nItem)) != NULL)
	{
		wstring srcPath = destFolder + pFile->GetName();
		if (!pFile->GetDispName().size())
			::ShellExecute(NULL, L"open", srcPath.c_str(), NULL, NULL, SW_SHOWNORMAL);
	}
	return FALSE;
}

BOOL CSDListCtrl::OnEndLabelEdit(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVDISPINFO* pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
	LV_ITEM* plvItem = &pDispInfo->item;
	int nItem = -1;
	int nSubItem = -1;
	if (plvItem)
	{
		nItem = plvItem->iItem;
		nSubItem = plvItem->iSubItem;
	}

	*pResult = 0;

	if ((nItem == -1) || (nSubItem != 0))
		return FALSE;

	if (plvItem->pszText == NULL)
		return FALSE;

	wstring destFolder = _pSDFileContainer->GetFolder();
	if (!destFolder.size())
		return FALSE;

	SDFile* pFile = NULL;
	if ((pFile = (SDFile*)GetItemData(nItem)) == NULL)
		return FALSE;
	
	wstring newName = plvItem->pszText;
	wstring srcPath = destFolder + pFile->GetName();
	wstring dstPath = destFolder + newName;
	
	if (!srcPath.compare(dstPath))
		return FALSE;

	if (_pSDFileContainer->FileExists(newName))
	{
		_pSDFileContainer->GenerateNewName(newName);
		if (MessageBox(CTrans::Load(IDS_SDD_RENAME, newName.c_str()), L"SecureDoc", MB_YESNO | MB_ICONQUESTION) == IDNO)
			return FALSE;
		
		dstPath = destFolder + newName;
	}

	if (!::MoveFileEx(srcPath.c_str(), dstPath.c_str(), MOVEFILE_WRITE_THROUGH))
		return FALSE;
	
	pFile->SetName(newName);
	SetItemText(plvItem->iItem, 0, newName.c_str());

	LVITEM Item;
	ZeroMemory(&Item, sizeof(LVITEM));
	Item.iItem = nItem;
	Item.mask = LVIF_IMAGE;
	if (GetItem(&Item))
	{
		SHFILEINFO sfi;
		SHGetFileInfo(dstPath.c_str(), 0, &sfi, sizeof(SHFILEINFO), SHGFI_SYSICONINDEX | SHGFI_SMALLICON | SHGFI_TYPENAME | SHGFI_ATTRIBUTES);
		if (SetItemText(plvItem->iItem, 2, (sfi.szTypeName)))
			pFile->SetType(sfi.szTypeName);
		Item.iImage = sfi.iIcon;
		if (SetItem(&Item))
			pFile->SetIcon(sfi.iIcon);
	}
	return FALSE;
}

void CSDListCtrl::OnMenuEncrypt()
{
	if (!_pKeyContainer->GetDefaultKey())
	{
		MessageBox(CTrans::Load(IDS_SDD_NO_ENCKEY), L"SecureDoc", MB_OK | MB_ICONWARNING);
		return;
	}

	vector<wstring> vePaths;
	size_t nCount = 0;
	if (!(nCount = GetSelectedItems(vePaths)))
		return;

	int nres = 0;
	if ((nres = MessageBox(CTrans::Load(IDS_SDL_DELETE_ORIG), L"SecureDoc", MB_YESNOCANCEL | MB_ICONQUESTION)) == IDCANCEL)
		return;

	FILE_HDR fhdr;
	for (size_t i=0; i<nCount; ++i)
	{
		if (!_pSDFileContainer->GetFileHeader(vePaths.at(i), fhdr))
			continue;
		if ((HIWORD(fhdr.SigAlg) == SDF_SIGNATURE) && (LOWORD(fhdr.SigAlg) == CTaskEncrAes16::ALG_ID))
			continue;

		CTask* pTask = new CTaskEncrAes16(++_TaskIdCounter, vePaths.at(i), _pSDFileContainer->GetFolder(), _pKeyContainer->GetDefaultKey());
		pTask->SetDescription(L"Encrypt", vePaths.at(i));
		_veTasks.push_back(pTask);

		if (nres == IDYES)
		{
			CTask* pTask = new CTaskDeleteSecure(++_TaskIdCounter, vePaths.at(i));
			pTask->SetDescription(L"Secure delete", vePaths.at(i));
			_veTasks.push_back(pTask);
		}
	}

	if (DoOps())
		Reload();
}

void CSDListCtrl::OnMenuDecrypt()
{
	if (!_pKeyContainer->GetSize())
	{
		MessageBox(CTrans::Load(IDS_SDD_NO_DECKEY), L"SecureDoc", MB_OK | MB_ICONWARNING);
		return;
	}

	vector<wstring> vePaths;
	size_t nCount = 0;
	if (!(nCount = GetSelectedItems(vePaths)))
		return;

	int nres = 0;
	if ((nres = MessageBox(CTrans::Load(IDS_SDL_DELETE_ORIG), L"SecureDoc", MB_YESNOCANCEL | MB_ICONQUESTION)) == IDCANCEL)
		return;

	FILE_HDR fhdr;
	for (size_t i=0; i<nCount; ++i)
	{
		if (!_pSDFileContainer->GetFileHeader(vePaths.at(i), fhdr))
			continue;
		if ((HIWORD(fhdr.SigAlg) == SDF_SIGNATURE) && (LOWORD(fhdr.SigAlg) == CTaskDecrAes16::ALG_ID))
		{
			const CKey* pKey = _pKeyContainer->GetKeyByTag(fhdr.KeyTag);
			if (!pKey)
				continue;
			CTask* pTask = new CTaskDecrAes16(++_TaskIdCounter, vePaths.at(i), _pSDFileContainer->GetFolder(), pKey);
			pTask->SetDescription(L"Decrypt", vePaths.at(i));
			_veTasks.push_back(pTask);

			if (nres == IDYES)
			{
				CTask* pTask = new CTaskDeleteSecure(++_TaskIdCounter, vePaths.at(i));
				pTask->SetDescription(L"Secure delete", vePaths.at(i));
				_veTasks.push_back(pTask);
			}
		}
	}

	if (DoOps())
		Reload();
}

void CSDListCtrl::OnMenuDelete()
{
	if (MessageBox(CTrans::Load(IDS_SDD_DELETE), L"SecureDoc", MB_YESNO | MB_ICONQUESTION) == IDNO)
		return;

	vector<wstring> vePaths;
	size_t nCount = 0;
	if (!(nCount = GetSelectedItems(vePaths)))
		return;

	for (size_t i=0; i<nCount; ++i)
	{
		CTask* pTask = new CTaskDeleteSecure(++_TaskIdCounter, vePaths.at(i));
		pTask->SetDescription(L"Secure delete", vePaths.at(i));
		_veTasks.push_back(pTask);
	}

	if (DoOps())
		Reload();
}

void CSDListCtrl::OnDropCopy(const wstring& srcFolder, const vector<SDFile*>& veFiles)
{
	wstring destFolder = _pSDFileContainer->GetFolder();
	if (!srcFolder.size() || !destFolder.size())
		return;

	const SDFile* pFile = NULL;
	for (int i=0; i<static_cast<int>(veFiles.size()); ++i)
	{
		if ((pFile = veFiles.at(i)) == NULL)
			continue;

		CTask* pTask = new CTaskCopy(++_TaskIdCounter, srcFolder + pFile->GetName(), destFolder);
		pTask->SetDescription(L"Copy", srcFolder + pFile->GetName());
		_veTasks.push_back(pTask);
	}

	if (DoOps())
		Reload();
}

bool CSDListCtrl::DoOps()
{
	if (!_veTasks.size())
		return false;
	CProgressDlg dlgProgr(this, _veTasks);
	dlgProgr.DoModal();

	while(!_veTasks.empty())
	{
		delete _veTasks.back();
		_veTasks.pop_back();
	}
	_veTasks.clear();
	return true;
}

BOOL CSDListCtrl::OnRightClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	int nItem = -1;
	int nSubItem = -1;
	if (pNMIA)
	{
		nItem = pNMIA->iItem;
		nSubItem = pNMIA->iSubItem;
	}

	*pResult = 0;

	if (nItem == -1)
		return FALSE;

	POINT mp = {0, 0};
	GetCursorPos(&mp);
	CMenu popMenu;
	popMenu.LoadMenu(IDR_CONTEXT_MENU);
	CMenu* pPopup = popMenu.GetSubMenu(0);
	if (!pPopup)
		return FALSE;

	CBitmap bmpMenu[3];
	bmpMenu[0].LoadBitmap(IDB_ENCRYPT);
	bmpMenu[1].LoadBitmap(IDB_DECRYPT);
	bmpMenu[2].LoadBitmap(IDB_DELETE);
	pPopup->SetMenuItemBitmaps(ID_ENCRYPT, MF_BYCOMMAND, &bmpMenu[0], &bmpMenu[0]);
	pPopup->SetMenuItemBitmaps(ID_DECRYPT, MF_BYCOMMAND, &bmpMenu[1], &bmpMenu[1]);
	pPopup->SetMenuItemBitmaps(ID_DELETE, MF_BYCOMMAND, &bmpMenu[2], &bmpMenu[2]);

	vector<wstring> vePaths;
	size_t nCount = GetSelectedItems(vePaths);

	FILE_HDR fhdr;
	BYTE fileType = 0;
	for (size_t i=0; i<nCount; ++i)
	{
		if (_pSDFileContainer->GetFileHeader(vePaths.at(i), fhdr))
			((HIWORD(fhdr.SigAlg) == SDF_SIGNATURE) && (LOWORD(fhdr.SigAlg) == CTaskEncrAes16::ALG_ID)) ? fileType |= 0x01 : fileType |= 0x02;
	}

	if (fileType == 0x00)
	{
		pPopup->EnableMenuItem(ID_ENCRYPT, MF_GRAYED);
		pPopup->EnableMenuItem(ID_DECRYPT, MF_GRAYED);
		pPopup->EnableMenuItem(ID_DELETE, MF_GRAYED);
	}
	else if (fileType == 0x03)
	{
		pPopup->RemoveMenu(ID_ENCRYPT, MF_BYCOMMAND);
		pPopup->RemoveMenu(ID_DECRYPT, MF_BYCOMMAND);
	}
	else if (fileType == 0x01)
		pPopup->RemoveMenu(ID_ENCRYPT, MF_BYCOMMAND);
	else if (fileType == 0x02)
		pPopup->RemoveMenu(ID_DECRYPT, MF_BYCOMMAND);

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, mp.x, mp.y, this, NULL);
	return FALSE;
}

size_t CSDListCtrl::GetSelectedItems(vector<wstring>& vePaths)
{
	if (!_pSDFileContainer->GetFolder().size())
		return 0;

	POSITION pos = NULL;
	if ((pos = GetFirstSelectedItemPosition()) == NULL)
		return 0;

	int nSelItem = -1;
	const SDFile* pFile = NULL;
	while (pos)
	{
		nSelItem = GetNextSelectedItem(pos);
		if ((pFile = (const SDFile*)GetItemData(nSelItem)) != NULL)
		{
			wstring srcPath = _pSDFileContainer->GetFolder() + pFile->GetName();
			vePaths.push_back(srcPath);
		}
	}

	return vePaths.size();
}

CString CSDListCtrl::GetToolTipText(int nRow, int nCol)
{
	if (nCol == 0)
	{
		const SDFile* pFile = (const SDFile*)GetItemData(nRow);
		if (pFile)
			return ((pFile->GetDispName().size()) ? pFile->GetDispName().c_str() : pFile->GetName().c_str());
	}
	return GetItemText(nRow, nCol);
}

void CSDListCtrl::AddItem(int nIndex, const SDFile* pFile)
{
	LVITEM lvItem;
	ZeroMemory(&lvItem, sizeof(LVITEM));
	lvItem.mask = LVIF_IMAGE;
	lvItem.iItem = nIndex;
	lvItem.iSubItem = 0;
	lvItem.iImage = pFile->GetIcon();

	if (pFile->GetAttr() & SFGAO_GHOSTED)
	{
		lvItem.mask |= LVIF_STATE;
		lvItem.state = lvItem.stateMask = LVIS_CUT;
	}

	InsertItem(&lvItem);
	SetItemData(nIndex, (DWORD_PTR)pFile);

	if (_pConfig->GetDispEncr() && pFile->GetDispName().size())
		SetItemText(nIndex, 0, pFile->GetDispName().c_str());
	else
		SetItemText(nIndex, 0, pFile->GetName().c_str());

	CString sfmt;
	SYSTEMTIME st;
	::FileTimeToSystemTime(&(pFile->GetFileTime()), &st);
	sfmt.Format(L"%02d/%02d/%d %02d:%02d", st.wDay, st.wMonth, st.wYear, st.wHour, st.wMinute);
	SetItemText(nIndex, 1, sfmt);

	SetItemText(nIndex, 2, pFile->GetType().c_str());

	((pFile->GetSize()/1024) < 1024) ? sfmt.Format(L"%.2f KB", (float)pFile->GetSize()/1024) : sfmt.Format(L"%.2f MB", (float)pFile->GetSize()/1048576);
	SetItemText(nIndex, 3, sfmt);
}

int CSDListCtrl::OnCompareItems(LPARAM lParam1, LPARAM lParam2, int iColumn)
{
	if ((lParam1 == NULL) || (lParam2 == NULL))
		return 0;

	if (iColumn == 0)
	{
		wstring item1, item2;
		SDFile* pFile1 = (SDFile*)lParam1;
		SDFile* pFile2 = (SDFile*)lParam2;
		if (_pConfig->GetDispEncr())
		{
			(pFile1->GetDispName().size()) ? item1 = pFile1->GetDispName() : item1 = pFile1->GetName();
			(pFile2->GetDispName().size()) ? item2 = pFile2->GetDispName() : item2 = pFile2->GetName();
		}
		else
		{
			item1 = pFile1->GetName();
			item2 = pFile2->GetName();
		}
		return _wcsicmp(item1.c_str(), item2.c_str());
	}
	if (iColumn == 1)
	{
		FILETIME ft1 = ((SDFile*)lParam1)->GetFileTime();
		FILETIME ft2 = ((SDFile*)lParam2)->GetFileTime();
		return ::CompareFileTime(&ft1, &ft2);
	}
	if (iColumn == 2)
	{
		wstring item1 = ((SDFile*)lParam1)->GetType();
		wstring item2 = ((SDFile*)lParam2)->GetType();
		return _wcsicmp(item1.c_str(), item2.c_str());
	}
	if (iColumn == 3)
	{
		__int64 item1 = ((SDFile*)lParam1)->GetSize();
		__int64 item2 = ((SDFile*)lParam2)->GetSize();
		return ((item1 < item2) ? -1 : 1);
	}
	return 0;
}

int CSDListCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMFCListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	ModifyStyle(LVS_SINGLESEL, 0, 0);

	DWORD dwStyle = GetStyle();
	if (!(dwStyle & LVS_EDITLABELS))
		ModifyStyle(0, LVS_EDITLABELS);

	SetExtendedStyle(LVS_EX_FULLROWSELECT);

	SHFILEINFO sfi;
    HIMAGELIST hSysSmallImageList = (HIMAGELIST)SHGetFileInfo(L".txt", 0, &sfi, sizeof(SHFILEINFO), SHGFI_SYSICONINDEX | SHGFI_SMALLICON | SHGFI_USEFILEATTRIBUTES);
	_imgList.Attach(hSysSmallImageList);
	SetImageList(&_imgList, LVSIL_SMALL);

	_rcInit.top = 0;
	_rcInit.left = 0;
	_rcInit.right = lpCreateStruct->cx;
	_rcInit.bottom = lpCreateStruct->cy;

	_colRatio[0] = 0.50f;
	_colRatio[1] = 0.20f;
	_colRatio[2] = 0.15f;
	_colRatio[3] = 0.15f;

	ENSURE(InsertColumn(0, CTrans::Load(IDS_SDL_HDR_NAME), LVCFMT_LEFT, static_cast<int>(_colRatio[0] * _rcInit.Width()), 0) >= 0);
	ENSURE(InsertColumn(1, CTrans::Load(IDS_SDL_HDR_MODIF), LVCFMT_LEFT, static_cast<int>(_colRatio[1] * _rcInit.Width()), 1) >= 0);
	ENSURE(InsertColumn(2, CTrans::Load(IDS_SDL_HDR_TYPE), LVCFMT_LEFT, static_cast<int>(_colRatio[2] * _rcInit.Width()), 2) >= 0);
	ENSURE(InsertColumn(3, CTrans::Load(IDS_SDL_HDR_SIZE), LVCFMT_LEFT, static_cast<int>(_colRatio[3] * _rcInit.Width()), 3) >= 0);

	m_iSortedColumn = 2;
	m_bAscending = 1;

	_pDropTarget.reset(new CSDDropTarget(this));
	_pDropTarget->Register(this);
	return 0;
}

void CSDListCtrl::OnSize(UINT nType, int cx, int cy)
{
	CToolTipListCtrl::OnSize(nType, cx, cy);

	if ((!cx) || (cx == _lastWidth))
	{
		return;
	}

	_lastWidth = cx;
	SetColumnWidth(3, LVSCW_AUTOSIZE_USEHEADER);
}

void CSDListCtrl::ResizeCols(int newWidth)
{
	SetColumnWidth(0, static_cast<int>(_colRatio[0] * ((newWidth > 0) ? newWidth : _lastWidth)));
	SetColumnWidth(3, LVSCW_AUTOSIZE_USEHEADER);
}
