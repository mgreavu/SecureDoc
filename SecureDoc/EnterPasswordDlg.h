#pragma once
#include "resource.h"
#include "iface/ColoredDialog.h"
#include "iface/GroupBox.h"
#include "afxwin.h"

class CConfig;

class CEnterPasswordDlg : public CColoredDialog
{
	DECLARE_DYNAMIC(CEnterPasswordDlg)

public:
	CEnterPasswordDlg(CWnd* pParent, CConfig* pConfig);
	virtual ~CEnterPasswordDlg();

	enum { IDD = IDD_ENTER_PASSWORD_DIALOG };

protected:
	CConfig* _pConfig;
	CString _Password;
	CGroupBox _PasswGroup;
	CMFCButton _btnCancel;
	CMFCButton _btnOk;

	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();

	DECLARE_MESSAGE_MAP()
};
