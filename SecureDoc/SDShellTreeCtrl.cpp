#include "stdafx.h"
#include "SecureDoc.h"
#include "SDShellTreeCtrl.h"
#include "KeyContainer.h"
#include "base/Trans.h"
#include "tasks/TaskEncrAes16.h"
#include "tasks/TaskDecrAes16.h"
#include "tasks/TaskDeleteSecure.h"
#include "tasks/ProgressDlg.h"

IMPLEMENT_DYNAMIC(CSDShellTreeCtrl, CMFCShellTreeCtrl)

CSDShellTreeCtrl::CSDShellTreeCtrl(CKeyContainer* pKeyContainer)
	: _dquePos(-1)
	, _bNav(false)
	, _pKeyContainer(pKeyContainer)
	, _TaskIdCounter(0)
{
}

CSDShellTreeCtrl::~CSDShellTreeCtrl()
{
	debug("\tCSDShellTreeCtrl::~CSDShellTreeCtrl()\r\n");
}

BEGIN_MESSAGE_MAP(CSDShellTreeCtrl, CMFCShellTreeCtrl)
	ON_WM_CREATE()
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_ENCRYPT, &CSDShellTreeCtrl::OnMenuEncrypt)
	ON_COMMAND(ID_DECRYPT, &CSDShellTreeCtrl::OnMenuDecrypt)
	ON_NOTIFY_REFLECT_EX(TVN_SELCHANGED, &CSDShellTreeCtrl::OnSelChanged)
END_MESSAGE_MAP()


void CSDShellTreeCtrl::OnContextMenu(CWnd* pWnd, CPoint point)
{
	if (point.x == -1 && point.y == -1)
		return;

	HTREEITEM hItem = NULL;

	CPoint ptClient = point;
	ScreenToClient(&ptClient);
	UINT nFlags = 0;
	if ((hItem = HitTest(ptClient, &nFlags)) == NULL)
		return;

	bool bOk = false;
	CString strPath;
	if (GetItemPath(strPath, hItem) && (!strPath.IsEmpty()))
	{
		HANDLE hSearch = INVALID_HANDLE_VALUE;
		WIN32_FIND_DATA w32fd;
		ZeroMemory(&w32fd, sizeof(WIN32_FIND_DATA));
		if ((hSearch = ::FindFirstFile(strPath.GetBuffer(strPath.GetLength()), &w32fd)) != INVALID_HANDLE_VALUE)
		{
			::FindClose(hSearch);
			if (w32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				bOk = true;
		}	
	}

	if (!bOk)
		return;
	
	CMenu popMenu;
	popMenu.LoadMenu(IDR_CONTEXT_MENU);
	CMenu* pPopup = popMenu.GetSubMenu(0);
	if (!pPopup)
		return;

	CBitmap bmpMenu[3];
	bmpMenu[0].LoadBitmap(IDB_ENCRYPT);
	bmpMenu[1].LoadBitmap(IDB_DECRYPT);
	bmpMenu[2].LoadBitmap(IDB_DELETE);
	pPopup->SetMenuItemBitmaps(ID_ENCRYPT, MF_BYCOMMAND, &bmpMenu[0], &bmpMenu[0]);
	pPopup->SetMenuItemBitmaps(ID_DECRYPT, MF_BYCOMMAND, &bmpMenu[1], &bmpMenu[1]);
	pPopup->SetMenuItemBitmaps(ID_DELETE, MF_BYCOMMAND, &bmpMenu[2], &bmpMenu[2]);

	pPopup->RemoveMenu(ID_DELETE, MF_BYCOMMAND);

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
}

void CSDShellTreeCtrl::OnMenuEncrypt()
{
	CString strPath;
	if (GetItemPath(strPath, NULL) && (!strPath.IsEmpty()))
	{
		if (!_pKeyContainer->GetDefaultKey())
		{
			MessageBox(CTrans::Load(IDS_SDD_NO_ENCKEY), L"SecureDoc", MB_OK | MB_ICONWARNING);
			return;
		}
		bool bDelOrig = false;
		int nres = 0;
		if ((nres = MessageBox(CTrans::Load(IDS_SDL_DELETE_ORIG), L"SecureDoc", MB_YESNOCANCEL | MB_ICONQUESTION)) == IDCANCEL)
			return;
		if (nres == IDYES) 
			bDelOrig = true;

		TraverseFolder(strPath.GetBuffer(strPath.GetLength()), 0, bDelOrig);
		DoOps();
	}
}

void CSDShellTreeCtrl::OnMenuDecrypt()
{
	CString strPath;
	if (GetItemPath(strPath, NULL) && (!strPath.IsEmpty()))
	{
		if (!_pKeyContainer->GetSize())
		{
			MessageBox(CTrans::Load(IDS_SDD_NO_DECKEY), L"SecureDoc", MB_OK | MB_ICONWARNING);
			return;
		}
		bool bDelOrig = false;
		int nres = 0;
		if ((nres = MessageBox(CTrans::Load(IDS_SDL_DELETE_ORIG), L"SecureDoc", MB_YESNOCANCEL | MB_ICONQUESTION)) == IDCANCEL)
			return;
		if (nres == IDYES) 
			bDelOrig = true;

		TraverseFolder(strPath.GetBuffer(strPath.GetLength()), 1, bDelOrig);
		DoOps();
	}
}

bool CSDShellTreeCtrl::TraverseFolder(const wstring& SourcePath, unsigned long dwReqType, bool bDelOrig)
{
	wstring srcPath = SourcePath;
	if (srcPath.at(srcPath.size() - 1) != L'\\')
		srcPath += L'\\';
	srcPath += L"*.*";

	HANDLE hSearch = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA w32fd;
	if ((hSearch = ::FindFirstFile(srcPath.c_str(), &w32fd)) == INVALID_HANDLE_VALUE)
		return false;

	bool bOk = true;
	while (1) 
	{
		if ((_wcsicmp(w32fd.cFileName, L".") != 0) && (_wcsicmp(w32fd.cFileName, L"..") != 0)) 
		{
			srcPath = SourcePath;
			if (srcPath.at(srcPath.size() - 1) != L'\\')
				srcPath += L'\\';
			srcPath += w32fd.cFileName;

			if (w32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) 
			{
				if (!TraverseFolder(srcPath, dwReqType, bDelOrig))
				{
					bOk = false;
					break;
				}
			}
			else
			{
				FILE_HDR fhdr;
				if (GetFileHeader(srcPath, fhdr))
				{
					CTask* pTask = NULL;

					if (dwReqType == 0)
					{
						if ((HIWORD(fhdr.SigAlg) != SDF_SIGNATURE) || (LOWORD(fhdr.SigAlg) != CTaskEncrAes16::ALG_ID))
						{
							pTask = new CTaskEncrAes16(++_TaskIdCounter, srcPath, SourcePath, _pKeyContainer->GetDefaultKey());
							pTask->SetDescription(L"Encrypt", srcPath);
							_veTasks.push_back(pTask);

							if (bDelOrig)
							{
								pTask = new CTaskDeleteSecure(++_TaskIdCounter, srcPath);
								pTask->SetDescription(L"Secure delete", srcPath);
								_veTasks.push_back(pTask);
							}
						}
					}
					else if (dwReqType == 1)
					{
						if ((HIWORD(fhdr.SigAlg) == SDF_SIGNATURE) && (LOWORD(fhdr.SigAlg) == CTaskDecrAes16::ALG_ID))
						{
							const CKey* pKey = _pKeyContainer->GetKeyByTag(fhdr.KeyTag);
							if (pKey)
							{
								pTask = new CTaskDecrAes16(++_TaskIdCounter, srcPath, SourcePath, pKey);
								pTask->SetDescription(L"Decrypt", srcPath);
								_veTasks.push_back(pTask);

								if (bDelOrig)
								{
									pTask = new CTaskDeleteSecure(++_TaskIdCounter, srcPath);
									pTask->SetDescription(L"Secure delete", srcPath);
									_veTasks.push_back(pTask);
								}
							}
						}
					}
				}
			}
		}
		if (!::FindNextFile(hSearch, &w32fd)) 
			break;
	}

	::FindClose(hSearch);
	return bOk;
}

bool CSDShellTreeCtrl::DoOps()
{
	if (!_veTasks.size())
		return false;
	CProgressDlg dlgProgr(this, _veTasks);
	dlgProgr.DoModal();

	while(!_veTasks.empty())
	{
		delete _veTasks.back();
		_veTasks.pop_back();
	}
	_veTasks.clear();

	HTREEITEM hItem = NULL;	/* reselect the item to force TVN_SELCHANGED -> list reload */
	if ((hItem = GetSelectedItem()) != NULL)
	{
		SelectItem(NULL);
		SelectItem(hItem);
	}
	return true;
}

bool CSDShellTreeCtrl::GetFileHeader(const wstring& strPath, FILE_HDR& fhdr)
{
	HANDLE hFile = INVALID_HANDLE_VALUE;
	if ((hFile = ::CreateFile(strPath.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
		return false;

	ZeroMemory(&fhdr, sizeof(FILE_HDR));

	bool bReadOk = false;
	unsigned long dwBytes = 0;
	if (::ReadFile(hFile, &fhdr, sizeof(FILE_HDR), &dwBytes, NULL))
		bReadOk = true;	

	::CloseHandle(hFile);
	return bReadOk;
}

BOOL CSDShellTreeCtrl::OnSelChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	if (pNMTreeView)
	{
		if (!_bNav)
		{
			if (_dqueVisited.size() == 10)
				_dqueVisited.pop_front();
			_dqueVisited.push_back(pNMTreeView->itemNew.hItem);
			_dquePos = static_cast<int>(_dqueVisited.size() - 1);
		}
		else
			_bNav = false;
	}
	*pResult = 0;
	return FALSE;
}

void CSDShellTreeCtrl::Back()
{
	if (_dquePos > 0)
	{
		_bNav = true;
		SelectItem(_dqueVisited.at(--_dquePos));
		EnsureVisible(_dqueVisited.at(_dquePos));
	}
}

void CSDShellTreeCtrl::Forward()
{
	if (_dquePos < static_cast<int>(_dqueVisited.size()) - 1)
	{
		_bNav = true;
		SelectItem(_dqueVisited.at(++_dquePos));
		EnsureVisible(_dqueVisited.at(_dquePos));
	}
}

int CSDShellTreeCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMFCShellTreeCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableShellContextMenu(FALSE);
	DWORD dwStyle = GetStyle();
	if (dwStyle & TVS_EDITLABELS)
		ModifyStyle(TVS_EDITLABELS, 0);
	return 0;
}