#pragma once
#include <string>

using std::wstring;

class SDFile
{
public:
	SDFile(const wstring& name, const wstring& dispname, const wstring &type, const FILETIME& lastWriteTime, DWORD attr, __int64 size, int iIcon = -1)
		: _name(name)
		, _dispname(dispname)
		, _type(type)
		, _lastWriteTime(lastWriteTime)
		, _attr(attr)
		, _size(size)
		, _iIcon(iIcon)
	{}

	~SDFile(void) {}

	wstring GetName() const { return _name; }
	wstring GetDispName() const { return _dispname; }
	wstring GetType() const { return _type; }
	FILETIME GetFileTime() const { return _lastWriteTime; }
	DWORD GetAttr() const { return _attr; }
	__int64 GetSize() const { return _size; }
	int GetIcon() const { return _iIcon; }

	void SetName(const wstring& name) { _name = name; }
	void SetType(const wstring& type) { _type = type; }
	void SetIcon(int iIcon) { _iIcon = iIcon; }

private:
	wstring _name;
	wstring _dispname;
	wstring _type;
	FILETIME _lastWriteTime;
	DWORD _attr;
	__int64 _size;
	int _iIcon;
};
