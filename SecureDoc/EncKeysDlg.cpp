#include "stdafx.h"
#include "base\Trans.h"
#include "EncKeysDlg.h"
#include "tasks\Key.h"
#include <openssl\md5.h>

IMPLEMENT_DYNAMIC(CEncKeysDlg, CColoredDialog)

CEncKeysDlg::CEncKeysDlg(CWnd* pParent, CKeyContainer* pKeyContainer) 
	: CColoredDialog(CEncKeysDlg::IDD, pParent, RGB(255, 255, 255))
	, _pKeyContainer(pKeyContainer)
	, _nCurrSel(-1)
{
}

CEncKeysDlg::~CEncKeysDlg()
{
	debug("\tCEncKeysDlg::~CEncKeysDlg()\r\n");
}

void CEncKeysDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_NEW_KEY, _btnNew);
	DDX_Control(pDX, IDOK, _btnDefault);
	DDX_Control(pDX, IDC_REMOVE_KEY, _btnDelete);
	DDX_Control(pDX, IDC_SAVE_KEYS, _btnSave);
	DDX_Control(pDX, IDCANCEL, _btnClose);
}

BEGIN_MESSAGE_MAP(CEncKeysDlg, CColoredDialog)
	ON_NOTIFY(NM_CLICK, IDC_KEYS_LIST, &CEncKeysDlg::OnClick)
	ON_NOTIFY(NM_RCLICK, IDC_KEYS_LIST, &CEncKeysDlg::OnRightClick)
	ON_BN_CLICKED(IDOK, &CEncKeysDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_NEW_KEY, &CEncKeysDlg::OnBnClickedNewKey)
	ON_BN_CLICKED(IDC_REMOVE_KEY, &CEncKeysDlg::OnBnClickedRemoveKey)
	ON_BN_CLICKED(IDC_SAVE_KEYS, &CEncKeysDlg::OnBnClickedSaveKeys)
END_MESSAGE_MAP()

void CEncKeysDlg::OnClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	int nItem = -1;
	if (pNMIA)
		nItem = pNMIA->iItem;

	if (nItem == -1)
	{
		if (_nCurrSel > -1)
		{
			((CButton*)GetDlgItem(IDOK))->EnableWindow(FALSE);
			((CButton*)GetDlgItem(IDC_REMOVE_KEY))->EnableWindow(FALSE);
		}
	}
	else
	{
		CString sTag = _listKeys.GetItemText(nItem, 1);
		unsigned __int64 Tag = 0;
		swscanf_s(sTag, L"%I64X", &Tag);

		if (_pKeyContainer->GetDefaultKeyTag() != Tag)
			((CButton*)GetDlgItem(IDOK))->EnableWindow(TRUE);
		else
			((CButton*)GetDlgItem(IDOK))->EnableWindow(FALSE);

		if (_nCurrSel == -1)
			((CButton*)GetDlgItem(IDC_REMOVE_KEY))->EnableWindow(TRUE);	
	}

	_nCurrSel = nItem;
}

void CEncKeysDlg::OnRightClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	int nItem = -1;
	if (pNMIA)
		nItem = pNMIA->iItem;

	if (nItem == -1)
	{
		if (_nCurrSel > -1)
		{
			((CButton*)GetDlgItem(IDOK))->EnableWindow(FALSE);
			((CButton*)GetDlgItem(IDC_REMOVE_KEY))->EnableWindow(FALSE);
		}
	}
	else
	{
		CString sTag = _listKeys.GetItemText(nItem, 1);
		unsigned __int64 Tag = 0;
		swscanf_s(sTag, L"%I64X", &Tag);

		if (_pKeyContainer->GetDefaultKeyTag() != Tag)
			((CButton*)GetDlgItem(IDOK))->EnableWindow(TRUE);
		else
			((CButton*)GetDlgItem(IDOK))->EnableWindow(FALSE);

		if (_nCurrSel == -1)
			((CButton*)GetDlgItem(IDC_REMOVE_KEY))->EnableWindow(TRUE);
	}

	_nCurrSel = nItem;
}

BOOL CEncKeysDlg::OnInitDialog()
{
	CColoredDialog::OnInitDialog();

	CRect clientRect, listRect;
	GetClientRect(&clientRect);
	listRect = clientRect;

	listRect.DeflateRect(10, 10, 115, 10);
	DWORD dwStyle = LVS_REPORT | LVS_SINGLESEL | LVS_SHOWSELALWAYS | WS_CHILD | WS_VISIBLE | WS_GROUP | WS_TABSTOP;
	VERIFY(_listKeys.CreateEx(WS_EX_CLIENTEDGE, dwStyle, listRect, this, IDC_KEYS_LIST));

	_listKeys.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	_listKeys.EnableToolTips(FALSE);

	_btnNew.SetImage(IDB_DOC_ADD);
	_btnDefault.SetImage(IDB_DOC_SELECT);
	_btnDelete.SetImage(IDB_DOC_DEL);
	_btnSave.SetImage(IDB_DOC_SAVE);
	_btnClose.SetImage(IDB_DOC_CLOSE);

	AddListHeaders();
	PopulateList();

	return TRUE;
}

void CEncKeysDlg::AddListHeaders()
{
	CRect listRect;
	_listKeys.GetClientRect(&listRect);
	int totalWidth = listRect.Width();
	int accWidth = 0;
	int colwidths[4] = {3, 13, 4, 18};

	debug("\tAddListHeaders() --> totalWidth = %d\r\n", totalWidth);

	CStringArray arrHeaders;
	CString sHdr;

	sHdr = L"Id";
	arrHeaders.Add(sHdr);

	sHdr = L"Tag";
	arrHeaders.Add(sHdr);

	sHdr = L"Len";
	arrHeaders.Add(sHdr);

	sHdr = L"Hash";
	arrHeaders.Add(sHdr);

	LV_COLUMN lvcolumn;
	for (int i=0; i<arrHeaders.GetSize(); ++i)
	{
		memset(&lvcolumn, 0x00, sizeof(LV_COLUMN));
		lvcolumn.mask = LVCF_FMT | LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH;

		sHdr = arrHeaders.GetAt(i);
		lvcolumn.pszText = sHdr.GetBuffer(sHdr.GetLength());
		lvcolumn.iSubItem = i;

		if (i<3)
			lvcolumn.cx = (totalWidth * colwidths[i]) / 38;
		else
			lvcolumn.cx = totalWidth - accWidth + 5;

		accWidth += lvcolumn.cx;

		_listKeys.InsertColumn(i, &lvcolumn);
	}

	return;
}

void CEncKeysDlg::PopulateList()
{
	_listKeys.DeleteAllItems();

	const CKey* pKey = NULL;
	for (size_t i=0; i<_pKeyContainer->GetSize(); ++i)
	{
		pKey = _pKeyContainer->GetKeyByIndex(i);
		AddRow(static_cast<int>(i), pKey);
	}
}

void CEncKeysDlg::AddRow(int nListIndex, const CKey* pKey)
{
	if (_listKeys.InsertItem(nListIndex, L"") == LB_ERR)
		return;

	CString str;
	str.Format(L"%d", nListIndex);
	_listKeys.SetItemText(nListIndex, 0, str);

	str.Empty();
	str.Format(L"%016I64X", pKey->GetTag());
	_listKeys.SetItemText(nListIndex, 1, str);

	str.Empty();
	str.Format(L"%d", pKey->GetLen());
	_listKeys.SetItemText(nListIndex, 2, str);

	unsigned char md5sum[MD5_DIGEST_LENGTH];
	ZeroMemory(md5sum, sizeof(md5sum));
	MD5(pKey->GetKey(), pKey->GetLen(), md5sum);

	CHAR output[2*MD5_DIGEST_LENGTH + 1];
	ZeroMemory(output, sizeof(output));

	for(int j=0; j<MD5_DIGEST_LENGTH; ++j)
		sprintf_s(output + j * 2, 3, "%02x", md5sum[j]);

	size_t i = 0;
	wchar_t wszTag[MAX_PATH + 1];
	ZeroMemory(wszTag, sizeof(wszTag));
	mbstowcs_s(&i, wszTag, (size_t)MAX_PATH, output, (size_t)MAX_PATH);

	_listKeys.SetItemText(nListIndex, 3, wszTag);
}

void CEncKeysDlg::OnBnClickedOk()
{
	if (_nCurrSel < 0)
		return;

	CString sTag = _listKeys.GetItemText(_nCurrSel, 1);
	unsigned __int64 Tag = 0;
	swscanf_s(sTag, L"%I64X", &Tag);

	_pKeyContainer->SetDefaultKey(Tag);
	((CButton*)GetDlgItem(IDOK))->EnableWindow(FALSE);
}

void CEncKeysDlg::OnBnClickedNewKey()
{
	const CKey* pKey = NULL;
	if ((pKey = _pKeyContainer->GenerateKey(16)) == NULL)
	{
		MessageBox(CTrans::Load(IDS_ENCKEYS_NOT_GEN), L"SecureDoc", MB_OK | MB_ICONEXCLAMATION);
		return;
	}
	
	AddRow(_listKeys.GetItemCount(), pKey);
}

void CEncKeysDlg::OnBnClickedRemoveKey()
{
	if (_nCurrSel < 0)
		return;

	CString sTag = _listKeys.GetItemText(_nCurrSel, 1);
	if (MessageBox(CTrans::Load(IDS_ENCKEYS_REMOVE, sTag), L"SecureDoc", MB_YESNO | MB_ICONQUESTION) == IDNO)
		return;

	unsigned __int64 Tag = 0;
	swscanf_s(sTag, L"%I64X", &Tag);

	if (_pKeyContainer->RemoveKeyByTag(Tag))
	{
		if (_listKeys.DeleteItem(_nCurrSel))
		{
			((CButton*)GetDlgItem(IDOK))->EnableWindow(FALSE);
			((CButton*)GetDlgItem(IDC_REMOVE_KEY))->EnableWindow(FALSE);
			_nCurrSel = -1;
		}
	}
}

void CEncKeysDlg::OnBnClickedSaveKeys()
{
	if (_pKeyContainer->Save())
		MessageBox(CTrans::Load(IDS_ENCKEYS_SAVED), L"SecureDoc", MB_OK | MB_ICONINFORMATION);
	else
		MessageBox(CTrans::Load(IDS_ENCKEYS_NOT_SAVED), L"SecureDoc", MB_OK | MB_ICONEXCLAMATION);
}
