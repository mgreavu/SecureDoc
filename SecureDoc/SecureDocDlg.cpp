#include "stdafx.h"
#include "SecureDoc.h"
#include "SecureDocDlg.h"
#include "base/Trans.h"
#include "Config.h"
#include "tasks/Key.h"
#include "KeyContainer.h"
#include "SDListCtrl.h"
#include "AboutDlg.h"
#include "EncKeysDlg.h"
#include "ChangePasswordDlg.h"
#include "SettingsDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CSecureDocDlg::CSecureDocDlg(CWnd* pParent, CConfig* pConfig)
	: CDialogEx(CSecureDocDlg::IDD, pParent)
	, _fSplitterDiv(0.3f)
	, _pConfig(pConfig)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	_SBarWidths[0] = 100; _SBarWidths[1] = -1;
	_pKeyContainer.reset(new CKeyContainer(_pConfig));
	_pctrlFileList.reset(new CSDListCtrl(_pKeyContainer.get(), _pConfig));
	_pshellTree.reset(new CSDShellTreeCtrl(_pKeyContainer.get()));
}

void CSecureDocDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSecureDocDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_NOTIFY(TVN_SELCHANGED, IDC_PC_TREE, &CSecureDocDlg::OnTreeSelChanged)
	ON_MESSAGE(CSDListCtrl::UWM_FILESCHANGED, &CSecureDocDlg::OnListFilesChanged)
	ON_COMMAND(ID_BTN_LEFT, &CSecureDocDlg::OnTbnNavLeft)
	ON_COMMAND(ID_BTN_RIGHT, &CSecureDocDlg::OnTbnNavRight)
	ON_COMMAND(ID_BTN_KEYS, &CSecureDocDlg::OnTbnKeys)
	ON_MESSAGE(UWM_TBNENCKEYS, &CSecureDocDlg::OnTbnKeys)
	ON_COMMAND(ID_BTN_PASSW, &CSecureDocDlg::OnTbnPasswd)
	ON_MESSAGE(UWM_TBNPASSWD, &CSecureDocDlg::OnTbnPasswd)
	ON_COMMAND(ID_BTN_SETTINGS, &CSecureDocDlg::OnTbnSettings)
	ON_MESSAGE(UWM_TBNSETTINGS, &CSecureDocDlg::OnTbnSettings)
	ON_COMMAND(ID_BTN_ABOUT, &CSecureDocDlg::OnTbnAbout)
	ON_MESSAGE(UWM_TBNABOUT, &CSecureDocDlg::OnTbnAbout)
	ON_COMMAND(ID_BTN_EXIT, &CSecureDocDlg::OnTbnExit)
	ON_MESSAGE(UWM_TBNEXIT, &CSecureDocDlg::OnTbnExit)
END_MESSAGE_MAP()

BOOL CSecureDocDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	CRect rcInit;
	GetClientRect(&rcInit);

	if (_mfcToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC)
		&& _mfcToolBar.LoadToolBar(IDR_TOOLBAR))
	{
		_mfcToolBar.SetPaneStyle(_mfcToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_ANY));
		_sizeToolBar = _mfcToolBar.CalcFixedLayout(TRUE, TRUE);
		_mfcToolBar.SetWindowPos(NULL, 0, 0, _sizeToolBar.cx, _sizeToolBar.cy, SWP_NOACTIVATE | SWP_NOZORDER);
	}
	rcInit.top += _sizeToolBar.cy;

	_statusBar.Create(WS_CHILD | WS_VISIBLE | CCS_BOTTOM | SBARS_SIZEGRIP, CRect(0,0,0,0), this, IDC_SBAR);
	_SBarWidths[0] = static_cast<int>(rcInit.right * _fSplitterDiv);
	_statusBar.SetParts(2, _SBarWidths);
	_statusBar.SetIcon(0, (HICON)::LoadImage(::GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_USER_FILES), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR));
	_statusBar.SetIcon(1, (HICON)::LoadImage(::GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_FOLDER_OPEN), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR));
	_statusBar.SetText(L"Start", 0, 0);

	CRect rc;
	::GetClientRect(::GetDlgItem(m_hWnd, IDC_SBAR), &rc);
	_sizeStatusBar.cy = rc.Height();
	_sizeStatusBar.cx = rc.Width();

	rcInit.bottom -= _sizeStatusBar.cy;

	rc.top = rcInit.top;
	rc.left = rcInit.left;
	rc.right = static_cast<LONG>(rcInit.right * _fSplitterDiv);
	rc.bottom = rcInit.bottom;

	DWORD dwStyle = LVS_REPORT | LVS_SINGLESEL | LVS_SHOWSELALWAYS | WS_CHILD | WS_VISIBLE | WS_GROUP | WS_TABSTOP;
	VERIFY(_pshellTree->CreateEx(WS_EX_CLIENTEDGE, dwStyle, rc, this, IDC_PC_TREE));
	_pshellTree->SetFlags(SHCONTF_FOLDERS | SHCONTF_INCLUDEHIDDEN | SHCONTF_INCLUDESUPERHIDDEN);	

	_pshellTree->SetBkColor(RGB(248, 248, 248));

	rc.top = rcInit.top;
	rc.left = static_cast<LONG>(rcInit.right * _fSplitterDiv + SPLITTER_MARGIN);
	rc.right = rc.left;
	rc.bottom = rcInit.bottom;

	_vertSplitter.Create(WS_CHILD | WS_VISIBLE, rc, this, IDC_VERT_SPLITTER);
	_vertSplitter.SetStyle(SPS_VERTICAL);
	_vertSplitter.SetRange(static_cast<int>(_fSplitterDiv*rcInit.Width()), static_cast<int>(0.5*rcInit.Width()));

	rc.top = rcInit.top;
	rc.left = static_cast<LONG>(rcInit.right * _fSplitterDiv + SPLITTER_MARGIN);
	rc.right = rcInit.right;
	rc.bottom = rcInit.bottom;

	dwStyle = LVS_REPORT | LVS_SINGLESEL | LVS_SHOWSELALWAYS | WS_CHILD | WS_VISIBLE | WS_GROUP | WS_TABSTOP;
	VERIFY(_pctrlFileList->CreateEx(WS_EX_CLIENTEDGE, dwStyle, rc, this, IDC_DOC_LIST));

	_pctrlFileList->SetBkColor(RGB(255, 196, 136));
	_pctrlFileList->SetTextBkColor(CLR_NONE);

	_pshellTree->SelectItem(_pshellTree->GetRootItem());

	if (_pKeyContainer->Load())
	{
		if (_pKeyContainer->GetSize())
		{
			_pKeyContainer->SetDefaultKey((_pKeyContainer->GetKeyByIndex(0))->GetTag());
		}
	}

	GetClientRect(&rcInit);
	if (GetScreenRect(rc))
	{
		rcInit.left = 0;
		rcInit.top = 0;	
		rcInit.right = static_cast<LONG>(0.6f * rc.Width());
		rcInit.bottom = static_cast<LONG>(0.7f * rc.Height());
		MoveWindow(&rcInit, FALSE);
	}

	_pctrlFileList->ResizeCols( static_cast<int>((1.0f - _fSplitterDiv) * rcInit.Width()) );

	return TRUE;
}

void CSecureDocDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	if (!_vertSplitter.m_hWnd)
		return;

	int Wt = static_cast<int>(_fSplitterDiv*cx);	// width tree
	int Ls = Wt + SPLITTER_MARGIN;					// left splitter
	int Ws = 6;										// width splitter
	int Ll = Ls + Ws + SPLITTER_MARGIN;				// left file list
	int Wl = cx - Ll;								// width file list

	HDWP hMoveSet = NULL;
	if ((hMoveSet = ::BeginDeferWindowPos(4)) != NULL)
	{
		hMoveSet = ::DeferWindowPos(hMoveSet, _vertSplitter.m_hWnd, NULL, Ls, _sizeToolBar.cy, Ws, cy - _sizeToolBar.cy - _sizeStatusBar.cy, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE);
		hMoveSet = ::DeferWindowPos(hMoveSet, _pshellTree->m_hWnd, NULL, 0, _sizeToolBar.cy, Wt, cy - _sizeToolBar.cy - _sizeStatusBar.cy, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE);
		hMoveSet = ::DeferWindowPos(hMoveSet, _pctrlFileList->m_hWnd, NULL, Ll, _sizeToolBar.cy, Wl, cy - _sizeToolBar.cy - _sizeStatusBar.cy, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE);
		hMoveSet = ::DeferWindowPos(hMoveSet, _statusBar.m_hWnd, NULL, 0, cy - _sizeStatusBar.cy, cx, _sizeStatusBar.cy, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE);
		::EndDeferWindowPos(hMoveSet);
	}

	_vertSplitter.SetRange(static_cast<int>(0.3*cx), static_cast<int>(0.5*cx));
	_SBarWidths[0] = Wt + Ws/2;
	_statusBar.SetParts(2, _SBarWidths);
}

LRESULT CSecureDocDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if (message == WM_NOTIFY)
	{
		if (wParam == IDC_VERT_SPLITTER)
		{
			SPC_NMHDR* pHdr = reinterpret_cast<SPC_NMHDR*>(lParam);

			CSplitterControl::ChangeWidth(_pshellTree.get(), pHdr->delta, CW_LEFTALIGN);
			CSplitterControl::ChangeWidth(_pctrlFileList.get(), -pHdr->delta, CW_RIGHTALIGN);

			if (pHdr->delta > 0)
			{
    			_pctrlFileList->Invalidate();
			}

			_SBarWidths[0] += pHdr->delta;
			_statusBar.SetParts(2, _SBarWidths);

			CRect rcDlg;
			GetClientRect(rcDlg);
			CRect rcSplitter;
			_vertSplitter.GetWindowRect(rcSplitter);
			((CWnd*)FromHandle(m_hWnd))->ScreenToClient(rcSplitter);
			_fSplitterDiv = static_cast<float>(rcSplitter.left) / static_cast<float>(rcDlg.Width()); // raport pozitional LeftSplitter / DlgWidth
		}
	}

	return CDialog::DefWindowProc(message, wParam, lParam);
}

bool CSecureDocDlg::GetScreenRect(LPRECT lpRect)
{
	if (!lpRect) 
		return false;

	HDC hPrimaryDisplay = ::CreateDC(L"DISPLAY", NULL, NULL, NULL);
	if (!hPrimaryDisplay) 
		return false;

	lpRect->left = 0;
	lpRect->top = 0;
	lpRect->right = ::GetDeviceCaps(hPrimaryDisplay, HORZRES);
	lpRect->bottom = ::GetDeviceCaps(hPrimaryDisplay, VERTRES);

	::DeleteDC(hPrimaryDisplay);
	return true;
}

void CSecureDocDlg::OnTreeSelChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	*pResult = 0;

	HTREEITEM hItem = NULL;
	if ((hItem = pNMTreeView->itemNew.hItem) == NULL)
		return;

	CString strPath;
	_pshellTree->GetItemPath(strPath, hItem);
	_pctrlFileList->Load(strPath.GetBuffer(strPath.GetLength()));
}

LRESULT CSecureDocDlg::OnListFilesChanged(WPARAM wParam, LPARAM lParam)
{
	_statusBar.SetText(CTrans::Load(IDS_SBAR_FILES, static_cast<long>(wParam), static_cast<long>(lParam)), 1, 0);
	return 0;
}

void CSecureDocDlg::OnTbnNavLeft()
{
	_pshellTree->Back();
}

void CSecureDocDlg::OnTbnNavRight()
{
	_pshellTree->Forward();
}

void CSecureDocDlg::OnTbnKeys()
{
	PostMessage(UWM_TBNENCKEYS, 0, 0);
}

LRESULT CSecureDocDlg::OnTbnKeys(WPARAM wParam, LPARAM lParam)
{
	CEncKeysDlg dlgKeys(this, _pKeyContainer.get());
	dlgKeys.DoModal();
	return 0;
}

void CSecureDocDlg::OnTbnPasswd()
{
	PostMessage(UWM_TBNPASSWD, 0, 0);
}

LRESULT CSecureDocDlg::OnTbnPasswd(WPARAM wParam, LPARAM lParam)
{
	CChangePasswordDlg dlgChgPwd(this, _pConfig);
	dlgChgPwd.DoModal();
	return 0;
}

void CSecureDocDlg::OnTbnSettings()
{
	PostMessage(UWM_TBNSETTINGS, 0, 0);
}

LRESULT CSecureDocDlg::OnTbnSettings(WPARAM wParam, LPARAM lParam)
{
	CSettingsDlg dlgSettings(this, _pConfig);
	dlgSettings.DoModal();
	return 0;
}

void CSecureDocDlg::OnTbnAbout()
{
	PostMessage(UWM_TBNABOUT, 0, 0);
}

LRESULT CSecureDocDlg::OnTbnAbout(WPARAM wParam, LPARAM lParam)
{
	CAboutDlg dlgAbout(this);
	dlgAbout.DoModal();
	return 0;
}

LRESULT CSecureDocDlg::OnTbnExit(WPARAM wParam, LPARAM lParam)
{
	OnCancel();
	return 0;
}

void CSecureDocDlg::OnTbnExit()
{
	PostMessage(UWM_TBNEXIT, 0, 0);
}

void CSecureDocDlg::OnCancel()
{
	if (MessageBox(CTrans::Load(IDS_SDD_EXIT), L"SecureDoc", MB_YESNO | MB_ICONQUESTION) == IDNO)
		return;
	_pshellTree->DeleteAllItems();
	CDialogEx::OnCancel();
}

void CSecureDocDlg::OnClose()
{
	OnCancel();
}

void CSecureDocDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

BOOL CSecureDocDlg::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
}

void CSecureDocDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this);

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

HCURSOR CSecureDocDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
