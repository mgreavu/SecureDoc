#pragma once
#include <string>
#include <vector>
#include <mutex>
#include <sqlite3.h>

using std::string;
using std::vector;
using std::mutex;

class CKeyItem
{
public:
	CKeyItem(unsigned __int64 Tag, byte keyLen, vector<byte>& key)
		: _Tag(Tag)
		, _keyLen(keyLen)
		, _key(key)
	{
	}

	~CKeyItem() { /* debug("\tCKeyItem::~CKeyItem() --> _Tag = %I64X\r\n", _Tag); */ }

	unsigned __int64 GetTag() const { return _Tag; }
	byte GetLen() const { return _keyLen; }
	vector<byte> GetKey() const { return _key; }

protected:
	unsigned __int64 _Tag;
	byte _keyLen;
	vector<byte> _key;
};

typedef vector<CKeyItem*> veKeyItem;

class CConfig
{
public:
	CConfig(const string& dbName);
	~CConfig(void);
	
	bool Begin();
	bool Commit();
	bool Rollback();

	bool WriteKey(unsigned __int64 Tag, byte keyLen, const byte* key);
	bool ReadKeys(veKeyItem& keyItems);

	bool SetPassword(const string& appPassw);
	bool VerifyPassword(const string& appPassw);

	bool Read();
	bool Write();

	bool GetDispEncr() const { return _bDispEncr; };
	void SetDispEncr(bool bDispEncr) { _bDispEncr = bDispEncr; }
	string GetLang() const { return _lang; };
	void SetLang(const string& lang) { _lang = lang; }

	bool Empty();
	bool Compact();

private:
	mutex _locker;
	string _dbName;

	sqlite3* _db;
	sqlite3_stmt* _stmtInsert;
	sqlite3_stmt* _stmtSelect;

	bool _bDispEncr;
	string _lang;
	
	bool TableExists(const string& name);

	CConfig(const CConfig&);
	CConfig& operator=(const CConfig&);
};
