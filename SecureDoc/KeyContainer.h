#pragma once
#include <vector>
#include <memory>
#include <mutex>

using std::vector;
using std::shared_ptr;
using std::mutex;

class CConfig;
class CKey;

class CKeyContainer
{
public:
	typedef shared_ptr<CKey> CKeyShrPtr;

	CKeyContainer(CConfig* pConfig);
	~CKeyContainer(void);

	void SetDefaultKey(unsigned __int64 Tag) { _TagDefaultKey = Tag; }
	unsigned __int64 GetDefaultKeyTag() const { return _TagDefaultKey; }

	const CKey* GenerateKey(byte keyLen);
	const CKey* GetDefaultKey();
	const CKey* GetKeyByIndex(size_t Index);
	const CKey* GetKeyByTag(unsigned __int64 Tag);
	bool RemoveKeyByTag(unsigned __int64 Tag);	

	size_t GetSize() { return _veKeys.size(); }
	void Clear();
	bool Load();
	bool Save();

private:
	vector<CKeyShrPtr> _veKeys;
	unsigned __int64 _TagDefaultKey;
	CConfig* _pConfig;
	mutex _locker;
};
