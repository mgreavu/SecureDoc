#pragma once
#include "SDFile.h"
#include "tasks\FileHdr.h"
#include <vector>
#include <string>

using std::wstring;
using std::vector;

class SDFileContainer
{
public:
	SDFileContainer() {}
	~SDFileContainer(void)
	{
		DeleteAll();
	}

	bool Load(unsigned int& totalFiles, unsigned int& encryptedFiles);
	void DeleteAll();
	void SetPath(const wstring& folderPath);

	wstring GetFolder() const { return _folderPath; }
	int GetSize() const { return static_cast<int>(_veSDFiles.size()); }
	bool GetFileHeader(const wstring& strPath, FILE_HDR& fhdr);
	const SDFile* GetItem(int index) const;
	bool FileExists(const wstring& name);
	bool GenerateNewName(wstring& name);

private:
	wstring _folderPath;
	vector<SDFile*> _veSDFiles;
};

