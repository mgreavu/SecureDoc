#pragma once

class CAlphaBlendStatic : public CStatic
{
public:
	CAlphaBlendStatic();
	virtual ~CAlphaBlendStatic();

public:
	bool LoadBmp(WORD wID);

protected:
	CBitmap	_bm;
	BITMAP _bmInfo;
	bool _bLoaded;
/*	void Premultiply();	*/

	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
};
