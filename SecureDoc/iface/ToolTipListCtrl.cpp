#include "stdafx.h"
#include "ToolTipListCtrl.h"

IMPLEMENT_DYNAMIC(CToolTipListCtrl, CMFCListCtrl)

BEGIN_MESSAGE_MAP(CToolTipListCtrl, CMFCListCtrl)
	ON_NOTIFY_EX(TTN_NEEDTEXTA, 0, &CToolTipListCtrl::OnToolNeedText)
	ON_NOTIFY_EX(TTN_NEEDTEXTW, 0, &CToolTipListCtrl::OnToolNeedText)
	ON_WM_WINDOWPOSCHANGING()
END_MESSAGE_MAP()

void CToolTipListCtrl::PreSubclassWindow()
{
	CMFCListCtrl::PreSubclassWindow();

	SetExtendedStyle(LVS_EX_DOUBLEBUFFER | GetExtendedStyle());
}

void CToolTipListCtrl::CellHitTest(const CPoint& pt, int& nRow, int& nCol) const
{
	nRow = -1;
	nCol = -1;

	LVHITTESTINFO lvhti = {0};
	lvhti.pt = pt;
	nRow = ListView_SubItemHitTest(m_hWnd, &lvhti);	// SubItemHitTest is non-const
	nCol = lvhti.iSubItem;
	if (!(lvhti.flags & LVHT_ONITEM))
		nRow = -1;
}

BOOL CToolTipListCtrl::OnToolNeedText(UINT id, NMHDR* pNMHDR, LRESULT* pResult)
{
	CPoint pt(GetMessagePos());
	ScreenToClient(&pt);

	int nRow, nCol;
	CellHitTest(pt, nRow, nCol);
	if (nRow == -1 || nCol == -1)
		return FALSE;

	_tooltip = GetToolTipText(nRow, nCol);

	TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
	TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;

	if (pNMHDR->code == TTN_NEEDTEXTA)
		_wcstombsz(pTTTA->szText, static_cast<LPCTSTR>(_tooltip), sizeof(pTTTA->szText));
	else
		pTTTW->lpszText = _tooltip.GetBuffer(_tooltip.GetLength());

	return TRUE;
}

CString CToolTipListCtrl::GetToolTipText(int nRow, int nCol)
{
	return GetItemText(nRow, nCol);
}

int CToolTipListCtrl::GetAllItemsHeight()
{
	RECT viewRect;
	GetViewRect(&viewRect);
	return viewRect.bottom - viewRect.top;
}

int CToolTipListCtrl::GetRowWidth()
{
	if (GetItemCount() == 0)
		return 0;
	RECT rect;
	GetItemRect(0, &rect, LVIR_BOUNDS);
	return rect.right - rect.left;
}

int CToolTipListCtrl::GetHeaderHeight()
{
	RECT headerRect;
	GetHeaderCtrl().GetWindowRect(&headerRect);
	return headerRect.bottom - headerRect.top;
}

bool CToolTipListCtrl::IsScrollBarVisible(DWORD scrollBar)
{
	return (GetWindowLong(m_hWnd, GWL_STYLE) & scrollBar) != 0;
}

void CToolTipListCtrl::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{
	CMFCListCtrl::OnWindowPosChanging(lpwndpos);

	if ((lpwndpos->flags & SWP_NOSIZE) != 0)	// override only if control is resized
		return;

	if (!lpwndpos->cx)
		return;

	RECT rect;
	GetWindowRect(&rect);

	int deltaX = lpwndpos->cx - (rect.right - rect.left);	// calculate control width change

	if (deltaX < 0) 
	{
		int lastColumnIndex = m_wndHeader.GetItemCount() - 1;
		int columnWidth = GetColumnWidth(lastColumnIndex);
		SetColumnWidth(lastColumnIndex, columnWidth + deltaX);
	}
	if (IsScrollBarVisible(WS_VSCROLL)) 
	{
		// vertical scroll bar may become hidden either if height of control is increased or if width is increased (in case both scrollbars are visible)
		int deltaY = lpwndpos->cy - (rect.bottom - rect.top);
		if (deltaX <= 0 && deltaY <= 0)
			return;
		// height required for all items to be visible
		int allItemsHeight = GetAllItemsHeight();
		// row (i.e item) width
		int rowWidth = GetRowWidth();
		// calculate new client height and width after resize
		RECT clientRect;
		GetClientRect(&clientRect);
		int newClientHeight = clientRect.bottom - GetHeaderHeight() + deltaY;

		if (IsScrollBarVisible(WS_HSCROLL)) 
		{
			int newClientWidth = clientRect.right + deltaX;
			int hScrollBarHeight = GetSystemMetrics(SM_CYHSCROLL);
			int vScrollBarWidth = GetSystemMetrics(SM_CXVSCROLL);
			// if both scrollbars will be hidden then correct new height of client area
			if ((newClientHeight + hScrollBarHeight >= allItemsHeight) && (newClientWidth + vScrollBarWidth >= rowWidth))
				newClientHeight += hScrollBarHeight;
		}
		// ensure the first item is moved to the top
		if (newClientHeight >= allItemsHeight)
			EnsureVisible(0, FALSE);
	}
}
