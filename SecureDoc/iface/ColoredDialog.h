#pragma once
#include "afxwin.h"

class CColoredDialog : public CDialogEx
{
public:
	CColoredDialog::CColoredDialog (int nIDD, CWnd* pParent, const COLORREF& clrForeGnd)
		: CDialogEx(nIDD, pParent)
		, _clrForeGnd(clrForeGnd)
	{}
	CColoredDialog::~CColoredDialog(void) {}

protected:
	COLORREF _clrForeGnd;
	CBitmap _bkgBitmap;
	void CreateBackgroundBitmap();

	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	DECLARE_MESSAGE_MAP()
};
