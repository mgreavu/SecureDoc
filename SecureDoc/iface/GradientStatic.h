#pragma once

typedef UINT (CALLBACK* PFNGRADIENTFILL)(HDC, CONST PTRIVERTEX, DWORD, CONST PVOID, DWORD, DWORD);

class CGradientStatic : public CStatic
{
public:
	CGradientStatic();
	virtual ~CGradientStatic();

public:
	bool SetIcon(WORD wIconID);
	void SetFont(LOGFONT& logFont);
	void SetText(LPCTSTR lpszString);

	void SetGradient(COLORREF rgbNewStart, COLORREF rgbNewEnd, bool gradReverse);
	void SetTextColor(COLORREF rgbText) { _rgbText = rgbText; }
	void SetAlignement(BYTE align) { _align = align; }
	void EnableBorder(bool hasBorder) { _hasBorder = hasBorder; }
	void SetBorderColor(COLORREF rgbBorder) { _rgbBorder = rgbBorder; }

protected:
	void DrawBorder(HDC hDC, RECT rcBox);

	HICON _hIcon;
	HFONT _hFont;
	CString _caption;
	COLORREF _rgbStart;
	COLORREF _rgbEnd;
	HINSTANCE _hMSIMG32;
	PFNGRADIENTFILL _pfnGradientFill;
	COLORREF _rgbText;
	BYTE _align;
	COLORREF _rgbBorder;
	bool _hasBorder;
	short _gradReverse;

	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
};
