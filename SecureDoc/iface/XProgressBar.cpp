#include "stdafx.h"
#include "XProgressBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CXProgressBar::CXProgressBar()
: _TextColor(RGB(255, 255, 0))
, _hFont(NULL)
{
}

CXProgressBar::~CXProgressBar()
{
	if (_hFont) 
		::DeleteObject(_hFont);
}


BEGIN_MESSAGE_MAP(CXProgressBar, CProgressCtrl)
	ON_WM_PAINT()
END_MESSAGE_MAP()


void CXProgressBar::OnPaint() 
{
	CPaintDC dc(this); 

	RECT rcSurface = {0,0,0,0};
	GetClientRect(&rcSurface);

	LONG lWidth = rcSurface.right - rcSurface.left;
	LONG lHeight = rcSurface.bottom - rcSurface.top;

	HDC hWndDC = dc.GetSafeHdc();
	HDC hMemDC = ::CreateCompatibleDC(hWndDC);
	if (!hMemDC) 
		return;

	HBITMAP hMemBM = ::CreateCompatibleBitmap(hWndDC, lWidth, lHeight);
	::SelectObject (hMemDC, hMemBM);

	int nLow = 0, nHigh = 0;
	GetRange(nLow, nHigh);
	int nPos = GetPos();

	float fWndPerc = (float)(nPos * lWidth) / (float)(nHigh - nLow);

	RECT rcNow = {rcSurface.left, rcSurface.top, static_cast<long>(fWndPerc), rcSurface.bottom};
	
	COLORREF clrBase = RGB(0, 0, 0);
	HBRUSH hNewBrush = NULL;

	for (int i=1; i<=lHeight; ++i)
	{
		RECT rcGradient = {rcNow.left, i-1, rcNow.right, i};

		(nPos == nHigh) ? clrBase = RGB(32, 224-8*i, 32) : clrBase = RGB(255-8*i, 0, 0);			

		hNewBrush = ::CreateSolidBrush(clrBase);
		::SelectObject(hMemDC, hNewBrush);
		::FillRect(hMemDC, &rcGradient, hNewBrush);
		if (hNewBrush) 
			::DeleteObject(hNewBrush);
	}

	::SetBkMode(hMemDC, TRANSPARENT);
	if (_hFont) 
		::SelectObject(hMemDC, _hFont);
	::SetTextColor(hMemDC, _TextColor);

	CString sFmt;
	sFmt.Format(L"%s%d%%", _strCaption.GetBuffer(_strCaption.GetLength()), (int)((float)(nPos*100)/(float)(nHigh-nLow)));
	::DrawText(hMemDC, sFmt, sFmt.GetLength(), &rcSurface, DT_SINGLELINE | DT_VCENTER | DT_CENTER);

	::BitBlt(hWndDC, 0, 0, lWidth, lHeight, hMemDC, 0, 0, SRCCOPY);

	if (hMemBM)
		::DeleteObject(hMemBM);
	if (hMemDC)
		::DeleteDC(hMemDC);
}

void CXProgressBar::SetTextColor(COLORREF TextColor)
{
	_TextColor = TextColor;
}

void CXProgressBar::SetWindowText(LPCTSTR lpszString)
{
	_strCaption = lpszString;
	Invalidate();
}

void CXProgressBar::SetFont(const LOGFONT& logFont)
{
	if (_hFont) 
		::DeleteObject (_hFont);

	_hFont = ::CreateFontIndirect (&logFont);
}
