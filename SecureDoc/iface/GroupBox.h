#pragma once

class CGroupBox : public CButton
{
public:
	CGroupBox();
	virtual ~CGroupBox();

	void SetWndTitle(LPWSTR sTitle);
	void SetGroupBoxPos(CWnd* pInsertAfter, int x, int y, int cx, int cy, UINT uFlags);
	void InvalidateGroupBox(BOOL bErase);
	void SetGroupBoxRegion();
	void ActivateGroupBox(BOOL bActivate);

	CString m_strText;
	COLORREF m_ActiveColor;
	COLORREF m_ActiveShadow;
	COLORREF m_ActiveTextColor;
	LONG m_TextStartPoint;
	LOGFONT m_LogFont;

private:
	static const long TEXT_MARGIN = 5;
	CFont TextFont;
	void SetDefaultFont();

protected:
	virtual void PreSubclassWindow();
	afx_msg void OnPaint();
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg LRESULT OnSetText(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
};