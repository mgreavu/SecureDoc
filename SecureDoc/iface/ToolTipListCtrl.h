#pragma once

class CToolTipListCtrl : public CMFCListCtrl
{
	DECLARE_DYNAMIC(CToolTipListCtrl)

public:
	CToolTipListCtrl() {}
	virtual ~CToolTipListCtrl() {}

protected:
	CString _tooltip;

	int GetAllItemsHeight();
	int GetRowWidth();
	int GetHeaderHeight();
	bool IsScrollBarVisible(DWORD scrollBar);

	virtual void PreSubclassWindow();

	virtual CString GetToolTipText(int nRow, int nCol);
	void CellHitTest(const CPoint& pt, int& nRow, int& nCol) const;
	afx_msg BOOL OnToolNeedText(UINT id, NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnWindowPosChanging(WINDOWPOS* lpwndpos);

	DECLARE_MESSAGE_MAP()
};
