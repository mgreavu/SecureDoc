#include "stdafx.h"
#include "SplitterControl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

HCURSOR CSplitterControl::SplitterControl_hCursorV = NULL;
HCURSOR CSplitterControl::SplitterControl_hCursorH = NULL;

CSplitterControl::CSplitterControl()
	: m_bIsPressed(FALSE)	// Mouse is pressed down or not ?
	, m_nMin(-1)
	, m_nMax(-1) // Min and Max range of the splitter.
	, _size(2, 2)
{
	WORD HatchBits[8] = { 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55 };
	CBitmap bm;
	bm.CreateBitmap(8, 8, 1, 1, HatchBits);
	_hbrBkg.CreatePatternBrush(&bm);
}

CSplitterControl::~CSplitterControl() 
{
	_hbrBkg.DeleteObject();
}

BEGIN_MESSAGE_MAP(CSplitterControl, CStatic)
    ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()

BOOL CSplitterControl::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
}

void CSplitterControl::OnPaint() 
{
	CPaintDC dc(this);
	CRect rc;
	GetClientRect(rc);
	dc.SetTextColor(GetSysColor(COLOR_BTNSHADOW));
	dc.FillRect(rc, &_hbrBkg);
	return;
}

void CSplitterControl::Create(DWORD dwStyle, const CRect &rect, CWnd *pParent, UINT nID)
{
	CRect rc = rect;
	dwStyle |= SS_NOTIFY;
	
	// Determine default type base on it's size.
	m_nType = (rc.Width() < rc.Height()) ? SPS_VERTICAL : SPS_HORIZONTAL;

	CStatic::Create(L"", dwStyle, rc, pParent, nID);

	if (!SplitterControl_hCursorV)
	{
		SplitterControl_hCursorV = AfxGetApp()->LoadStandardCursor(IDC_SIZEWE);
		SplitterControl_hCursorH = AfxGetApp()->LoadStandardCursor(IDC_SIZENS);
	}

	SetRange(0, 0, -1);	// force the splitter not to be split
}

// nStyle = SPS_VERTICAL or SPS_HORIZONTAL
int CSplitterControl::SetStyle(int nStyle)
{
	int m_nOldStyle = m_nType;
	m_nType = nStyle;
	return m_nOldStyle;
}

void CSplitterControl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CStatic::OnLButtonDown(nFlags, point);
	
	m_bIsPressed = TRUE;
	SetCapture();

	CRect rcWnd;
	GetWindowRect(rcWnd);
	
	if (m_nType == SPS_VERTICAL)
	{
		m_nX = rcWnd.left + rcWnd.Width() / 2;
		m_nSavePos = m_nX;
	}
	else 
	{
		m_nY = rcWnd.top  + rcWnd.Height() / 2;
		m_nSavePos = m_nY;
	}
	
	CWindowDC dc(NULL);
	DrawDragRect(&dc, m_nX, m_nY, true);
}

void CSplitterControl::OnMouseMove(UINT nFlags, CPoint point)
{
	if (m_bIsPressed)
	{
		CPoint pt = point;
		ClientToScreen(&pt);
		GetParent()->ScreenToClient(&pt);

		if (m_nType == SPS_VERTICAL)
		{
			if (pt.x < m_nMin)
				pt.x = m_nMin;
			if (pt.x > m_nMax)
				pt.x = m_nMax;
		}
		else
		{
			if (pt.y < m_nMin)
				pt.y = m_nMin;
			if (pt.y > m_nMax)
				pt.y = m_nMax;
		}

		GetParent()->ClientToScreen(&pt);
		m_nX = pt.x;
		m_nY = pt.y;
		CWindowDC dc(NULL);
		DrawDragRect(&dc, m_nX, m_nY, false);
	}
	CStatic::OnMouseMove(nFlags, point);
}

void CSplitterControl::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (m_bIsPressed)
	{
		ClientToScreen(&point);
		CWindowDC dc(NULL);

		m_bIsPressed = FALSE;

		int delta;
		(m_nType == SPS_VERTICAL) ? delta = m_nX - m_nSavePos : delta = m_nY - m_nSavePos;
		if (delta != 0)
		{
			DrawDragRect(&dc, m_nX, m_nY, false);

			CWnd *pOwner = GetOwner();
			if (pOwner && IsWindow(pOwner->m_hWnd))
			{
				CRect rc;
				CPoint pt(m_nX, m_nY);
				pOwner->GetClientRect(rc);
				pOwner->ScreenToClient(&pt);
				MoveWindowTo(pt);

				SPC_NMHDR nmsp;
				ZeroMemory(&nmsp, sizeof(SPC_NMHDR));
				nmsp.hdr.hwndFrom = m_hWnd;
				nmsp.hdr.idFrom = GetDlgCtrlID();
				nmsp.hdr.code = SPN_SIZED;
				nmsp.delta = delta;

				pOwner->SendMessage(WM_NOTIFY, nmsp.hdr.idFrom, (LPARAM)&nmsp);
			}
		}
		else
		{
			DrawDragRect(&dc, m_nX, m_nY, true);
		}
	}

	ReleaseCapture();
	CStatic::OnLButtonUp(nFlags, point);
}

void CSplitterControl::DrawDragRect(CDC* pDC, int x, int y, bool bInit)
{
	CRect rcWnd;
	GetWindowRect(rcWnd);

	if (m_nType == SPS_VERTICAL)
	{
		_rcDragCurr.left = m_nX - 2;
		_rcDragCurr.top = rcWnd.top;
		_rcDragCurr.right = m_nX + 2;
		_rcDragCurr.bottom = rcWnd.bottom;
	}
	else // SPS_HORIZONTAL
	{
		_rcDragCurr.left = rcWnd.left;
		_rcDragCurr.top = m_nY - 2;
		_rcDragCurr.right = rcWnd.right;
		_rcDragCurr.bottom = m_nY + 2;
	}

	if (bInit)
		pDC->DrawDragRect(_rcDragCurr, _size, NULL, _size);
	else
		pDC->DrawDragRect(_rcDragCurr, _size, _rcDragPrev, _size);

	_rcDragPrev = _rcDragCurr;
}

void CSplitterControl::MoveWindowTo(CPoint pt)
{
	CRect rc;
	GetWindowRect(rc);
	CWnd* pParent = GetParent();
	if (!pParent || !::IsWindow(pParent->m_hWnd))
		return;

	pParent->ScreenToClient(rc);
	if (m_nType == SPS_VERTICAL)
	{	
		int nMidX = (rc.left + rc.right) / 2;
		int dx = pt.x - nMidX;
		rc.OffsetRect(dx, 0);
	}
	else
	{	
		int nMidY = (rc.top + rc.bottom) / 2;
		int dy = pt.y - nMidY;
		rc.OffsetRect(0, dy);
	}
	MoveWindow(rc);
}

void CSplitterControl::ChangeWidth(CWnd *pWnd, int dx, DWORD dwFlag)
{
	CWnd* pParent = pWnd->GetParent();
	if (pParent && ::IsWindow(pParent->m_hWnd))
	{
		CRect rcWnd;
		pWnd->GetWindowRect(rcWnd);
		pParent->ScreenToClient(rcWnd);

		if (dwFlag == CW_LEFTALIGN)
			rcWnd.right += dx;
		else if (dwFlag == CW_RIGHTALIGN)
			rcWnd.left -= dx;

		pWnd->MoveWindow(rcWnd);
	}
}

void CSplitterControl::ChangeHeight(CWnd *pWnd, int dy, DWORD dwFlag)
{
	CWnd* pParent = pWnd->GetParent();
	if (pParent && ::IsWindow(pParent->m_hWnd))
	{
		CRect rcWnd;
		pWnd->GetWindowRect(rcWnd);
		pParent->ScreenToClient(rcWnd);
		if (dwFlag == CW_TOPALIGN)
			rcWnd.bottom += dy;
		else if (dwFlag == CW_BOTTOMALIGN)
			rcWnd.top -= dy;

		pWnd->MoveWindow(rcWnd);
	}
}

void CSplitterControl::ChangePos(CWnd* pWnd, int dx, int dy)
{
	CWnd* pParent = pWnd->GetParent();
	if (pParent && ::IsWindow(pParent->m_hWnd))
	{
		CRect rcWnd;
		pWnd->GetWindowRect(rcWnd);
		pParent->ScreenToClient(rcWnd);
		rcWnd.OffsetRect(-dx, dy);
		pWnd->MoveWindow(rcWnd);
	}	
}

void CSplitterControl::SetRange(int nMin, int nMax)
{
	m_nMin = nMin;
	m_nMax = nMax;
}

// Set splitter range from (nRoot - nSubtraction) to (nRoot + nAddition)
// If (nRoot < 0) nRoot =  <current position of the splitter>
void CSplitterControl::SetRange(int nSubtraction, int nAddition, int nRoot)
{
	if (nRoot < 0)
	{
		CRect rcWnd;
		GetWindowRect(rcWnd);
		(m_nType == SPS_VERTICAL) ? nRoot = rcWnd.left + rcWnd.Width() / 2 : nRoot = rcWnd.top + rcWnd.Height() / 2;
	}
	m_nMin = nRoot - nSubtraction;
	m_nMax = nRoot + nAddition;
}

BOOL CSplitterControl::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	if (nHitTest == HTCLIENT)
	{
		(m_nType == SPS_VERTICAL) ? (::SetCursor(SplitterControl_hCursorV)) : (::SetCursor(SplitterControl_hCursorH));
		return 0;
	}
	return CStatic::OnSetCursor(pWnd, nHitTest, message);
}
