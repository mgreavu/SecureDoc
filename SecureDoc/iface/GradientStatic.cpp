#include "stdafx.h"
#include "GradientStatic.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#pragma warning (disable:4244)

CGradientStatic::CGradientStatic()
	: _hIcon(NULL)
	, _hFont(NULL)
	, _caption(_T(""))
	, _align(0)
	, _rgbText(RGB(0, 0, 0))
	, _hasBorder(false)
	, _rgbBorder(RGB(0, 0, 0))
	, _gradReverse(1)
	, _rgbStart(RGB(0, 0, 255))
	, _rgbEnd(RGB (0, 0, 0))
	, _hMSIMG32(NULL)
	, _pfnGradientFill(NULL)
{
	if (_hMSIMG32 = LoadLibrary(_T("msimg32.dll")))
	{
		_pfnGradientFill = (PFNGRADIENTFILL)::GetProcAddress(_hMSIMG32, "GradientFill");
		if (!_pfnGradientFill) 
		{
			::FreeLibrary(_hMSIMG32); 
			_hMSIMG32 = NULL;
		}
	}
}

CGradientStatic::~CGradientStatic()
{
	if (_hIcon) 
		::DestroyIcon(_hIcon);
	if (_hFont) 
		::DeleteObject(_hFont);
	if (_hMSIMG32) 	
		::FreeLibrary(_hMSIMG32);
}


BEGIN_MESSAGE_MAP(CGradientStatic, CStatic)
	ON_WM_PAINT()
	ON_WM_SIZE()
END_MESSAGE_MAP()


void CGradientStatic::OnPaint() 
{
	CPaintDC dc(this);

	RECT rcSurface;
	GetClientRect(&rcSurface);

	LONG lWidth = rcSurface.right - rcSurface.left;
	LONG lHeight = rcSurface.bottom - rcSurface.top;

	HDC hWndDC = dc.GetSafeHdc();
	HDC hMemDC = ::CreateCompatibleDC (hWndDC);
	if (!hMemDC) 
		return;

	HBITMAP hMemBM = ::CreateCompatibleBitmap(hWndDC, lWidth, lHeight);
	::SelectObject(hMemDC, hMemBM);

	if (_pfnGradientFill != NULL)
	{
		TRIVERTEX rcVertex[2];
		
		rcVertex[0].x = rcSurface.left;
		rcVertex[0].y = rcSurface.top;
		rcVertex[0].Red = GetRValue(_rgbStart) << 8;	// color values from 0x0000 to 0xff00 !!!!
		rcVertex[0].Green = GetGValue(_rgbStart) << 8;
		rcVertex[0].Blue = GetBValue(_rgbStart) << 8;
		rcVertex[0].Alpha = 0;

		rcVertex[1].x = rcSurface.right; 
		rcVertex[1].y = rcSurface.bottom;
		rcVertex[1].Red = GetRValue(_rgbEnd) << 8;
		rcVertex[1].Green = GetGValue(_rgbEnd) << 8;
		rcVertex[1].Blue = GetBValue(_rgbEnd) << 8;
		rcVertex[1].Alpha = 0;
		
		GRADIENT_RECT rect = {0, 0};
		rect.LowerRight = 1;
		
		_pfnGradientFill(hMemDC, rcVertex, 2, &rect, 1, GRADIENT_FILL_RECT_H);
	}

	if (_hIcon) 
		::DrawIconEx(hMemDC, 5, ((lHeight / 2) - 8), _hIcon, 0, 0, 0, NULL, DI_NORMAL);
	if (_hasBorder) 
		DrawBorder(hMemDC, rcSurface);
	::SetBkMode(hMemDC, TRANSPARENT);
	if (_hFont) 
		::SelectObject(hMemDC, _hFont);
	::SetTextColor(hMemDC, _rgbText);
	switch (_align)
	{
		case 0: 
			{
				rcSurface.left += 25;
				::DrawText(hMemDC, _caption, -1, &rcSurface, DT_SINGLELINE | DT_VCENTER | DT_LEFT); 
				break;
			}
		case 1: 
			{
				::DrawText(hMemDC, _caption, -1, &rcSurface, DT_SINGLELINE | DT_VCENTER | DT_CENTER); 
				break;
			}
		case 2: 
			{
				rcSurface.right -= 5;
				::DrawText(hMemDC, _caption, -1, &rcSurface, DT_SINGLELINE | DT_VCENTER | DT_RIGHT); 
				break;
			}
	}

	::BitBlt(hWndDC, 0, 0, lWidth, lHeight, hMemDC, 0, 0, SRCCOPY);

	::DeleteObject(hMemBM);
	::DeleteDC(hMemDC);
}

bool CGradientStatic::SetIcon(WORD wIconID)
{
	if (_hIcon) 
		::DestroyIcon(_hIcon);
	if (!(_hIcon = (HICON)::LoadImage(::GetModuleHandle(NULL), MAKEINTRESOURCE(wIconID), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR))) 
		return false;
	return true;
}

void CGradientStatic::SetFont(LOGFONT& logFont)
{
	_hFont = ::CreateFontIndirect(&logFont);
}

void CGradientStatic::SetText(LPCTSTR lpszString)
{
	_caption = lpszString;
	Invalidate();
}

void CGradientStatic::SetGradient(COLORREF rgbNewStart, COLORREF rgbNewEnd, bool gradReverse)
{
	_rgbStart = rgbNewStart;
	_rgbEnd = rgbNewEnd;
	(gradReverse) ? _gradReverse = -1 : _gradReverse = 1;
}

void CGradientStatic::DrawBorder(HDC hDC, RECT rcBox)
{
	HPEN hPen = ::CreatePen(PS_SOLID, 0, _rgbBorder);
	HPEN hOldPen = (HPEN)::SelectObject(hDC, hPen);

	::MoveToEx(hDC, 0,0, NULL);
	::LineTo(hDC, rcBox.right-1, rcBox.top);
	::LineTo(hDC, rcBox.right-1, rcBox.bottom-1);
	::LineTo(hDC, rcBox.left, rcBox.bottom-1);
	::LineTo(hDC, rcBox.left, rcBox.top);

	if (hOldPen)
		::SelectObject(hDC, hOldPen);
	::DeleteObject(hPen);
}

void CGradientStatic::OnSize(UINT nType, int cx, int cy) 
{
	CStatic::OnSize(nType, cx, cy);
	Invalidate();
}
