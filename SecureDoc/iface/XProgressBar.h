#pragma once

class CXProgressBar : public CProgressCtrl
{
public:
	CXProgressBar();
	~CXProgressBar();

	void SetFont(const LOGFONT& logFont);
	void SetWindowText(LPCTSTR lpszString);
	void SetTextColor(COLORREF TextColor);

protected:
	HFONT _hFont;
	CString _strCaption;
	COLORREF _TextColor;

	afx_msg void OnPaint();

	DECLARE_MESSAGE_MAP()
};
