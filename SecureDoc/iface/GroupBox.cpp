#include "stdafx.h"
#include "GroupBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CGroupBox::CGroupBox()
	: m_ActiveColor(RGB(224, 224, 224))
	, m_ActiveShadow(RGB(255, 255, 255))
	, m_ActiveTextColor(RGB(255, 255, 255))
	, m_TextStartPoint(20)
{
	memset(&m_LogFont, 0x00, sizeof(LOGFONT));
}

CGroupBox::~CGroupBox()
{
}


BEGIN_MESSAGE_MAP(CGroupBox, CButton)
	ON_WM_PAINT()
	ON_WM_CTLCOLOR_REFLECT()
	ON_MESSAGE(WM_SETTEXT, OnSetText)
END_MESSAGE_MAP()


void CGroupBox::OnPaint() 
{
	CPaintDC dc(this);
	int nSavedDC = dc.SaveDC();

	HRGN hRgn = ::CreateRectRgn(0, 0, 0, 0);
	GetWindowRgn(hRgn);
	::SelectClipRgn(dc.GetSafeHdc(), hRgn);

	CBrush* oldBrush = NULL;
	CPen* oldPen = NULL;

	CSize TitleSize;
	COLORREF crLineColor, crTextColor;

	TextFont.CreateFontIndirect(&m_LogFont);

	CFont* oldFont = dc.SelectObject(&TextFont);
	
	if (!m_strText.IsEmpty()) 
		TitleSize = dc.GetTextExtent(m_strText);
	else 
		TitleSize = CSize(0, 2);
	
	//Set the color for the box
	crLineColor = m_ActiveColor;
	crTextColor  = m_ActiveTextColor; 

	RECT rect;
	GetClientRect(&rect);

	HBRUSH hbr = (HBRUSH)::GetStockObject(HOLLOW_BRUSH);
	dc.SelectObject(CBrush::FromHandle(hbr));

	RECT BoxRect;
	POINT point = {TitleSize.cx, TitleSize.cy};

	//Draw the box outter lines
	BoxRect.left   = rect.left;
	BoxRect.top    = rect.top + (point.y/2) - 1;
	BoxRect.right  = rect.right - 1;
	BoxRect.bottom = rect.bottom - 1;	

	CPen OutterPen;
	OutterPen.CreatePen(PS_SOLID, 1, crLineColor);
	dc.SelectObject(&OutterPen);
	dc.MoveTo(BoxRect.left, BoxRect.bottom);
	dc.LineTo(BoxRect.right, BoxRect.bottom);
	dc.MoveTo(BoxRect.left, BoxRect.top);
	dc.LineTo(BoxRect.left, BoxRect.bottom);
	dc.MoveTo(BoxRect.right, BoxRect.top);
	dc.LineTo(BoxRect.right, BoxRect.bottom);

	dc.MoveTo(BoxRect.left, BoxRect.top);
	dc.LineTo(BoxRect.left + m_TextStartPoint - TEXT_MARGIN, BoxRect.top);

	int nRightOffset = BoxRect.right - 20;
	if (TitleSize.cx < ((BoxRect.right - BoxRect.left) - (m_TextStartPoint + TEXT_MARGIN + TEXT_MARGIN)))
		nRightOffset = m_TextStartPoint + TEXT_MARGIN + TitleSize.cx;

	RECT rcText = {rect.left + m_TextStartPoint, rect.top, nRightOffset - TEXT_MARGIN, rect.top + TitleSize.cy};

	dc.MoveTo(BoxRect.right, BoxRect.top);
	dc.LineTo(nRightOffset, BoxRect.top);

	dc.SelectObject(oldPen);
	OutterPen.DeleteObject();

	//Draw text
	dc.SetBkMode(TRANSPARENT);
	dc.SetTextColor(crTextColor);
	dc.ExtTextOut(rect.left + m_TextStartPoint, rect.top, ETO_CLIPPED, &rcText, m_strText, NULL);

	::DeleteObject(hbr);

	dc.SelectObject(oldBrush);

	dc.SelectObject(oldFont);
	TextFont.DeleteObject();

	::DeleteObject(hRgn);

	dc.RestoreDC(nSavedDC);
}

void CGroupBox::SetDefaultFont()
{
	m_LogFont.lfCharSet = ANSI_CHARSET;
	m_LogFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	m_LogFont.lfEscapement = 0;
	wcscpy_s(m_LogFont.lfFaceName, _countof(m_LogFont.lfFaceName), L"Arial");
	m_LogFont.lfHeight = 16;
	m_LogFont.lfItalic = FALSE;
	m_LogFont.lfOrientation = 0;
	m_LogFont.lfOutPrecision = OUT_DEFAULT_PRECIS;
	m_LogFont.lfPitchAndFamily = DEFAULT_PITCH | FF_MODERN;
	m_LogFont.lfQuality = PROOF_QUALITY;
	m_LogFont.lfStrikeOut = FALSE;
	m_LogFont.lfUnderline = FALSE;
	m_LogFont.lfWeight = FW_NORMAL;
	m_LogFont.lfWidth = 0;
}

void CGroupBox::PreSubclassWindow() 
{
	SetButtonStyle(BS_GROUPBOX, FALSE);

	GetWindowText(m_strText);
	SetDefaultFont();

	SetGroupBoxRegion();
	
	CButton::PreSubclassWindow();
}

void CGroupBox::SetGroupBoxRegion()
{
	CString WndTitle;
	CSize Size;
	RECT rect;
	
	GetClientRect(&rect);

	if (m_strText.IsEmpty()) 
	{
		Size = CSize(0, 2);
	}
	else
	{
		CDC* pDC = GetDC();

		CFont TextFontLocal;		
		TextFontLocal.CreateFontIndirect(&m_LogFont);
		CFont* oldFont = pDC->SelectObject(&TextFontLocal);		
		Size = pDC->GetTextExtent(m_strText);
		
		pDC->SelectObject(oldFont);
		TextFontLocal.DeleteObject();

		ReleaseDC(pDC);
	}
	
	CRgn UpRgn;
	CRgn LeftRgn;
	CRgn RightRgn;
	CRgn BottomRgn;
	
	UpRgn.CreateRectRgn(rect.left, rect.top, rect.right, rect.top+Size.cy);
	LeftRgn.CreateRectRgn(rect.left, rect.top, rect.left+2, rect.bottom-2);
	RightRgn.CreateRectRgn(rect.right-2, rect.top, rect.right, rect.bottom-2);
	BottomRgn.CreateRectRgn(rect.left, rect.bottom-2, rect.right, rect.bottom);
	
	CRgn ResRgn;
	
	ResRgn.CreateRectRgn(0, 0, 0, 0);
	
	ResRgn.CombineRgn(&UpRgn, NULL, RGN_COPY);
	ResRgn.CombineRgn(&ResRgn, &LeftRgn, RGN_OR);
	ResRgn.CombineRgn(&ResRgn, &RightRgn, RGN_OR);
	ResRgn.CombineRgn(&ResRgn, &BottomRgn, RGN_OR);
	
	SetWindowRgn((HRGN) ResRgn, TRUE);
}

void CGroupBox::InvalidateGroupBox(BOOL bErase)
{
	SetGroupBoxRegion();
	Invalidate(bErase);
	UpdateWindow();
}

void CGroupBox::SetGroupBoxPos(CWnd* pInsertAfter, int x, int y, int cx, int cy, UINT uFlags)
{
	SetWindowPos(pInsertAfter, x, y, cx, cy, uFlags);
	InvalidateGroupBox(TRUE);
}

HBRUSH CGroupBox::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	Invalidate();
	
	return NULL;
}

void CGroupBox::SetWndTitle(LPWSTR sTitle)
{
	SetWindowText(sTitle);
	SetGroupBoxRegion();
	Invalidate();
	UpdateWindow();
}

LRESULT CGroupBox::OnSetText(WPARAM wParam, LPARAM lParam)
{
	m_strText = (const _TCHAR*)lParam;
	SetGroupBoxRegion();
	Invalidate();
	UpdateWindow();
	
	return 0;
}
