#include "stdafx.h"
#include "ColoredDialog.h"

BEGIN_MESSAGE_MAP(CColoredDialog, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

HBRUSH CColoredDialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	switch (nCtlColor)
	{
		case CTLCOLOR_STATIC:
		{
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(_clrForeGnd);
			return (HBRUSH)GetStockObject(NULL_BRUSH);
		}
	}

	return hbr;
}

void CColoredDialog::CreateBackgroundBitmap()
{
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);

	int r2 = 112, g2 = 112, b2 = 156;
	int r1 = 32, g1 = 32, b1 = 64;

	int x1 = 0, y1 = 0;
	int x2 = 0, y2 = 0;

	CDC dc2;
	dc2.CreateCompatibleDC(&dc);

	if(_bkgBitmap.m_hObject)
		_bkgBitmap.DeleteObject();

	_bkgBitmap.CreateCompatibleBitmap(&dc, rect.Width(), rect.Height());
	CBitmap *pOldBmp = dc2.SelectObject(&_bkgBitmap);

	while(x1 < rect.Width() && y1 < rect.Height())
	{
		(y1 < rect.Height() - 1) ? ++y1 : ++x1;
		(x2 < rect.Width() - 1) ? ++x2 : ++y2;

		int i = x1 + y1;
		int r = r1 + (i * (r2 - r1) / (rect.Width() + rect.Height()));
		int g = g1 + (i * (g2 - g1) / (rect.Width() + rect.Height()));
		int b = b1 + (i * (b2 - b1) / (rect.Width() + rect.Height()));

		CPen p(PS_SOLID, 1, RGB(r, g, b));
		CPen *pOldPen = dc2.SelectObject(&p); 

		dc2.MoveTo(x1,y1);
		dc2.LineTo(x2,y2);

		dc2.SelectObject(pOldPen);
	} 

	dc2.SelectObject(pOldBmp);
}

BOOL CColoredDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	CreateBackgroundBitmap();
	return TRUE;
}

BOOL CColoredDialog::OnEraseBkgnd(CDC* pDC)
{
	CRect rect;
	GetClientRect(&rect);

	CDC dc2;
	dc2.CreateCompatibleDC(pDC);
	CBitmap *oldbmap = dc2.SelectObject(&_bkgBitmap);
	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &dc2, 0, 0, SRCCOPY);
	dc2.SelectObject(oldbmap);
	return TRUE;
}
