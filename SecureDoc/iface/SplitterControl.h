#pragma once

#define SPN_SIZED WM_APP + 1281
#define CW_LEFTALIGN 1
#define CW_RIGHTALIGN 2
#define CW_TOPALIGN 3
#define CW_BOTTOMALIGN 4
#define SPS_VERTICAL 1
#define SPS_HORIZONTAL 2

typedef struct SPC_NMHDR
{
	NMHDR hdr;
	int delta;
} SPC_NMHDR;

class CSplitterControl : public CStatic
{
public:
	CSplitterControl();
	virtual	~CSplitterControl();

	void SetRange(int nMin, int nMax);
	void SetRange(int nSubtraction, int nAddition, int nRoot);

	int	GetStyle() const { return m_nType; }
	int	SetStyle(int nStyle = SPS_VERTICAL);
	void Create(DWORD dwStyle, const CRect& rect, CWnd* pParent, UINT nID);

	static void ChangePos(CWnd* pWnd, int dx, int dy);
	static void ChangeWidth(CWnd* pWnd, int dx, DWORD dwFlag = CW_LEFTALIGN);
	static void ChangeHeight(CWnd* pWnd, int dy, DWORD dwFlag = CW_TOPALIGN);

protected:
	BOOL m_bIsPressed;
	int	 m_nType;
	int	 m_nX, m_nY;
	int	 m_nMin, m_nMax;
	int	 m_nSavePos;		// Save point on the lbutton down message
	CBrush _hbrBkg;

	CRect _rcDragCurr;
	CRect _rcDragPrev;
	const CSize _size;		// used to inflate the drag rect

	static HCURSOR SplitterControl_hCursorV;
	static HCURSOR SplitterControl_hCursorH;

	void DrawDragRect(CDC* pDC, int x, int y, bool bInit);
	void MoveWindowTo(CPoint pt);

	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);

	DECLARE_MESSAGE_MAP()
};
