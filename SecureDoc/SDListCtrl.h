#pragma once
#include "iface/ToolTipListCtrl.h"
#include <memory>
#include <string>
#include <vector>

using std::auto_ptr;
using std::wstring;
using std::vector;

class SDFile;
class SDFileContainer;
class CSDDropTarget;
class CKeyContainer;
class CTask;
class CConfig;

class CSDListCtrl : public CToolTipListCtrl
{
	DECLARE_DYNAMIC(CSDListCtrl)

public:
	CSDListCtrl(CKeyContainer* pKeyContainer, CConfig* pConfig);
	virtual ~CSDListCtrl();

	void Load(const wstring& folderPath);
	void OnDropCopy(const wstring& srcFolder, const vector<SDFile*>& veFiles);

	virtual int OnCompareItems(LPARAM lParam1, LPARAM lParam2, int iColumn);

	void ResizeCols(int newWidth = -1);

	static const UINT UWM_FILESCHANGED = WM_APP + 1836;

protected:
	CRect _rcInit;
	int _lastWidth;
	float _colRatio[4];
	CImageList _imgList;
	auto_ptr<CSDDropTarget> _pDropTarget;
	auto_ptr<SDFileContainer> _pSDFileContainer;
	CKeyContainer* _pKeyContainer;
	vector<CTask*> _veTasks;
	unsigned int _TaskIdCounter;
	unsigned int _totalFiles;
	unsigned int _encryptedFiles;
	CConfig* _pConfig;

	void Reload();
	void AddItem(int nIndex, const SDFile* pFile);
	size_t GetSelectedItems(vector<wstring>& vePaths);
	bool DoOps();
	CString GetToolTipText(int nRow, int nCol);

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnRightClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL OnDblClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL OnEndLabelEdit(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMenuEncrypt();
	afx_msg void OnMenuDecrypt();
	afx_msg void OnMenuDelete();
	DECLARE_MESSAGE_MAP()
};
