function InstallDepExe(depName, depParams: String) : Boolean;
var
	resCode: integer;
begin
	Result := false;
	ExtractTemporaryFile(depName);
	Result := Exec(ExpandConstant('{tmp}\') + depName, depParams, '', SW_SHOW, ewWaitUntilTerminated, resCode);
end;

function InstallDepMsi(depName, depParams: String) : Boolean;
var
	resCode: integer;
begin
	Result := false;
	ExtractTemporaryFile(depName);
	Result := Exec('msiexec.exe', '/i ' + ExpandConstant('{tmp}\') + depName + ' ' + depParams, '', SW_SHOW, ewWaitUntilTerminated, resCode);
end;
