
function IsCRT9Installed(platformType, wantedCrtVer: String) : Boolean;
var
	existingCRTVer, SearchPath, strAnalyzedFile, strScanFolder: string;
	FindRec: TFindRec;
begin
	Result := False;

	if platformType = 'x86' then begin
		SearchPath := ExpandConstant('{win}') + '\WinSxS\x86_Microsoft.VC90.CRT_*';
	end else if platformType = 'x64' then begin
		SearchPath := ExpandConstant('{win}') + '\WinSxS\amd64_Microsoft.VC90.CRT_*';
	end else begin
		exit;
	end;

	if FindFirst(SearchPath, FindRec) then begin
		try
			repeat
				if FindRec.Attributes and FILE_ATTRIBUTE_DIRECTORY = FILE_ATTRIBUTE_DIRECTORY then begin { it's a folder }
					strScanFolder := ExpandConstant('{win}') + '\WinSxS\' + FindRec.Name;
					strAnalyzedFile := FileSearch('msvcr90.dll', strScanFolder);
					if strAnalyzedFile <> '' then begin
						if GetVersionNumbersString(strAnalyzedFile, existingCRTVer) = True then begin
							if CompareVersion(existingCRTVer, wantedCrtVer) = 0 then begin
								Result := True;
								break;
							end;
						end;
					end;
				end;
			until not FindNext(FindRec);
		finally
		  FindClose(FindRec);
		end;
	end;
end;

function IsCRT10Installed(wantedCrtVer: String) : Boolean;
var
	existingCRTVer, strAnalyzedFile, strScanFolder: string;
begin
	Result := False;
	strScanFolder := ExpandConstant('{sys}');
	strAnalyzedFile := FileSearch('msvcr100.dll', strScanFolder);
	if strAnalyzedFile <> '' then begin
		if GetVersionNumbersString(strAnalyzedFile, existingCRTVer) = True then begin
			if CompareVersion(existingCRTVer, wantedCrtVer) >= 0 then begin
				Result := True;
			end;
		end;
	end;
end;
