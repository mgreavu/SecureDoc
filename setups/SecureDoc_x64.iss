;	Encryptor installer
;	by Mihai Greavu

#define private baseFolder ".."
#define private verApp GetFileVersion("..\bin\x64\SecureDoc.exe")
#define private PrereqFolder "D:\Prereqs"
#define private depVCRedist "vcredist_x64_10.0.30319.1.exe"

[Setup]
AppName=SecureDoc
AppVerName=SecureDoc {#verApp}
AppPublisher=Security32
AppPublisherURL=www.security32.com
AppSupportURL=www.security32.com
AppUpdatesURL=www.security32.com
AppCopyright=Copyright � 2012 Security32
VersionInfoVersion={#verApp}
DefaultDirName={pf}\Security32\SecureDoc
DefaultGroupName=SecureDoc
OutputDir={#baseFolder}\setups\bin
OutputBaseFilename=SecureDoc_x64_{#verApp}
SetupIconFile={#baseFolder}\setups\Install.ico
Compression=lzma/Max
SolidCompression=true
UsePreviousAppDir=false
AppVersion={#verApp}
UninstallDisplayName=SecureDoc {#verApp}
UninstallFilesDir={app}\uninst
SetupLogging=true
AllowUNCPath=false
MinVersion=0,5.2.3790
PrivilegesRequired=admin
WizardImageFile={#baseFolder}\setups\SetupFront.bmp
ChangesAssociations=true
ArchitecturesInstallIn64BitMode=x64
ArchitecturesAllowed=x64

[Languages]
Name: en; MessagesFile: compiler:Default.isl
Name: it; MessagesFile: compiler:Languages\Italian.isl

[CustomMessages]
en.CheckDependenciesProgressPageTitle1=Prerequisites
en.CheckDependenciesProgressPageTitle2=Checking SecureDoc Dependencies Status
en.CheckDependenciesPrereqListText=Prerequisites list
en.CheckDependenciesPrereqNotFound=Not found
en.CheckDependenciesPrereqTooOld=Too old
en.CheckDependenciesPrereqFound=found
en.CheckDependenciesPrereqNeedAtLeast=need at least
en.CheckDependenciesPrereqInit=Initializing...
en.CheckDependenciesPrereqWinSxS=Scanning WinSxS...
en.CheckDependenciesPrereqScanOk=Prerequisites scan completed. All Ok.
en.CheckDependenciesPrereqScanFail=Prerequisites scan completed. You must fix dependencies first.

it.CheckDependenciesProgressPageTitle1=Prerequisiti
it.CheckDependenciesProgressPageTitle2=Verifica dipendenze SecureDoc in corso
it.CheckDependenciesPrereqListText=Elenco prerequisiti
it.CheckDependenciesPrereqNotFound=Non trovato
it.CheckDependenciesPrereqTooOld=Troppo vecchio
it.CheckDependenciesPrereqFound=trovato
it.CheckDependenciesPrereqNeedAtLeast=necessaria almeno
it.CheckDependenciesPrereqInit=Inizializzazione...
it.CheckDependenciesPrereqWinSxS=Scansione WinSxS...
it.CheckDependenciesPrereqScanOk=Scansione dei prerequisiti completata. Prerequisiti installati.
it.CheckDependenciesPrereqScanFail=Scansione dei prerequisiti completata. E' necessario verificare le dipendenze.

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked
Name: quicklaunchicon; Description: {cm:CreateQuickLaunchIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked

[Dirs]
Name: {app}\lang

[Files]
Source: {#PrereqFolder}\vcredist\x64\{#depVCRedist}; Flags: nocompression dontcopy
Source: {#baseFolder}\bin\x64\SecureDoc.exe; DestDir: {app}; Flags: ignoreversion
Source: {#baseFolder}\bin\x64\lang\SecureDocROM.dll; DestDir: {app}\lang; Flags: ignoreversion
Source: {#baseFolder}\bin\x64\libeay32.dll; DestDir: {app}; Flags: ignoreversion
Source: {#baseFolder}\bin\x64\sqlite3.dll; DestDir: {app}; Flags: ignoreversion
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: {group}\SecureDoc; Filename: {app}\SecureDoc.exe
Name: {group}\{cm:UninstallProgram,SecureDoc}; Filename: {uninstallexe}
Name: {commondesktop}\SecureDoc; Filename: {app}\SecureDoc.exe; Tasks: desktopicon
Name: {userappdata}\Microsoft\Internet Explorer\Quick Launch\SecureDoc; Filename: {app}\SecureDoc.exe; Tasks: quicklaunchicon

[Run]
Filename: {app}\SecureDoc.exe; Description: {cm:LaunchProgram,SecureDoc}; Flags: nowait postinstall skipifsilent unchecked

[Code]
#include "inc\common\utils.iss"
#include "inc\common\deps.iss"
#include "inc\crt.iss"

const
	CRT_VERSION 		= '10.0.30319.1';
	PREREQ_INSTA_CRT	=  $0001;

var
	PrerequisitesPage: TWizardPage;
	PrereqProgBar: TNewProgressBar;
	PrereqList: TListBox;
	PrereqProgBarText: TNewStaticText;
	dwInstaPrereq: dword;

procedure InitializeWizard;
var
	PrereqListText: TNewStaticText;

begin
	PrerequisitesPage := CreateCustomPage (wpWelcome, ExpandConstant('{cm:CheckDependenciesProgressPageTitle1}'), ExpandConstant('{cm:CheckDependenciesProgressPageTitle2}'));

	PrereqProgBarText := TNewStaticText.Create(PrerequisitesPage);
	PrereqProgBarText.Caption := ExpandConstant('{cm:CheckDependenciesPrereqInit}');
	PrereqProgBarText.AutoSize := True;
	PrereqProgBarText.Parent := PrerequisitesPage.Surface;

	PrereqProgBar := TNewProgressBar .Create(PrerequisitesPage);
	PrereqProgBar.Top := PrereqProgBarText.Top + PrereqProgBarText.Height + ScaleY(8);
	PrereqProgBar.Width := PrerequisitesPage.SurfaceWidth;
	PrereqProgBar.Parent := PrerequisitesPage.Surface;
	PrereqProgBar.Min := 0;
	PrereqProgBar.Max := 10;
	PrereqProgBar.Position := 0;

	PrereqListText := TNewStaticText.Create(PrerequisitesPage);
	PrereqListText.Top := PrereqProgBar.Top + PrereqProgBar.Height + ScaleY(8);
	PrereqListText.Caption := ExpandConstant('{cm:CheckDependenciesPrereqListText}');
	PrereqListText.AutoSize := True;
	PrereqListText.Parent := PrerequisitesPage.Surface;

	PrereqList := TListBox.Create(PrerequisitesPage);
	PrereqList.Top := PrereqListText.Top + PrereqListText.Height + ScaleY(8);
	PrereqList.Width := PrerequisitesPage.SurfaceWidth;
	PrereqList.Height := ScaleY(97);
	PrereqList.Parent := PrerequisitesPage.Surface;
	PrereqList.MultiSelect := False;
end;

procedure CurPageChanged(CurPageID: Integer);
begin
	if CurPageID = PrerequisitesPage.ID then
	begin
		PrereqList.Items.Clear();

		PrereqProgBar.Position := 5;
		Sleep (20);
		PrereqProgBarText.Caption := ExpandConstant('{cm:CheckDependenciesPrereqWinSxS}');

		if IsCrt10Installed(CRT_VERSION) = True then begin
			dwInstaPrereq := dwInstaPrereq or PREREQ_INSTA_CRT;
		end;

		if dwInstaPrereq and PREREQ_INSTA_CRT = PREREQ_INSTA_CRT then
		begin
			PrereqList.Items.Add('Microsoft CRT ver ' + CRT_VERSION + '...' + '[Ok]');
		end else
		begin
			PrereqList.Items.Add('Microsoft CRT ver ' + CRT_VERSION + '...[' + ExpandConstant('{cm:CheckDependenciesPrereqNotFound}') + ']');
		end;

		PrereqProgBar.Position := 10;
		Sleep (20);

		if dwInstaPrereq = PREREQ_INSTA_CRT then
		begin
			PrereqProgBarText.Caption := ExpandConstant('{cm:CheckDependenciesPrereqScanOk}');
		end else
			PrereqProgBarText.Caption := ExpandConstant('{cm:CheckDependenciesPrereqScanFail}');
	end;
end;

procedure CurStepChanged(CurStep: TSetupStep);
begin
	if CurStep = ssInstall then begin
		if (dwInstaPrereq and PREREQ_INSTA_CRT) = 0 then begin
			if InstallDepExe('{#depVCRedist}', '/passive') = false then begin
				MsgBox(ExpandConstant('Error installing dependency file {#depVCRedist}'), mbError, MB_OK);
				Abort;
			end;
		end;
	end;
end;
